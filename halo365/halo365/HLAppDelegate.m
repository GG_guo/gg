//
//  AppDelegate.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/3/28.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//
//ggggg
#import "HLAppDelegate.h"
//#import "HLLoginViewController.h"
#import "HLNewfeatureViewController.h"
//#import "HLHaloTool.h"
//#import "HLAPPFrameTabBarController.h"
#import "HLLoginManage.h"

@interface HLAppDelegate ()

@end

@implementation HLAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    NSString *key = @"CFBundleVersion";
    
    // 取出沙盒中存储的上次使用软件的版本号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults stringForKey:key];
    [self setupNavBar];
    
    // 获得当前软件的版本号
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    if ([currentVersion isEqualToString:lastVersion]) {
        // 显示状态栏
        application.statusBarHidden = NO;
        
        if (_tabBarController!=nil) {
            [_tabBarController.view removeFromSuperview];
            _tabBarController = nil;
        }
        _tabBarController = [[HLBaseAPPTabBarController alloc]init];
        
        self.window.rootViewController = _tabBarController;
        
        [self.window makeKeyAndVisible];
        
        
        
        //        self.window.rootViewController = [[HLLoginViewController alloc] init];
        
    } else { // 新版本
        self.window.rootViewController = [[HLNewfeatureViewController alloc] init];
        // 存储新版本
        [defaults setObject:currentVersion forKey:key];
        [defaults synchronize];
    }
    [self.window makeKeyAndVisible];
    
    
    return YES;
    
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    // 停止下载所有图片
    [[SDWebImageManager sharedManager] cancelAll];
    // 清除内存中的图片
    [[SDWebImageManager sharedManager].imageCache clearMemory];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    //关键语句
    [[HLLoginManage sharedManager]autoLogin];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)setupNavBar
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    UINavigationBar *bar = [UINavigationBar appearance];
    CGFloat rgb = 206/255;
    bar.barTintColor = [UIColor colorWithRed:135/255 green:rgb  blue:1 alpha:0.9];
    bar.tintColor = [UIColor whiteColor];
    bar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
}

-(UIViewController *)getNowViewController{
    
    UIViewController *reController = nil;
    if (_tabBarController !=nil) {
        id tempViewController = _tabBarController;
        //        while ([(UIViewController *)tempViewController presentationController] !=nil) {
        //            tempViewController = [(UIViewController *)tempViewController presentationController];
        //        }
        if (tempViewController!=nil)
        {
            if ([tempViewController isKindOfClass:[UINavigationController class]])
            {
                reController = [(UINavigationController *)tempViewController topViewController];
            }
            else
            {
                reController = tempViewController;
            }
        }
        else
        {
            reController = _tabBarController;
        }
    }
    return reController;
    
}

@end
