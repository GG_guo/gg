//
//  Define.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/5.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//


//登录
#define Login_InternalAPI @"http://192.168.31.2:9002/v0/signin"
#define Login_ExternalAPI @"http://api.halo365.cn/v0/signin"

//短信发送
#define Verification_InternalAPI @"http://192.168.31.2:9002/v0/sms"
#define Verification_ExternalAPI @"http://api.halo365.cn/v0/sms"


//忘记密码
#define Forgetpwd_InternalAPI @"http://192.168.31.2:9002/v0/forgetpwd"
#define Forgetpwd_ExternalAPI @"http://api.halo365.cn/v0/forgetpwd"

//注册密码
#define Registered_InternalAPI @"http://192.168.31.2:9002/v0/signup"
#define Registered_ExternalAPI @"http://api.halo365.cn/v0/signup"

//我的
#define My_InternalAPI @"http://192.168.31.2:9002/v0/profile/"
#define My_ExternalAPI @"http://api.halo365.cn/v0/profile/"

//Tool基本地址
#define BESTTOOL @"http://static.halo365.cn/tools/"

#define User_Agreement @"http://static.halo365.cn/privacy/"



//获得RGB颜色
#define ZLColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

#define bgVCcolor [UIColor colorWithRed:235.f/255.f green:235.f/255.f blue:244.f/255.f alpha:1];
