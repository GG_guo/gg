//
//  HLAttentionViewController.m
//  FlipTableView
//
//  Created by fujin on 15/7/9.
//  Copyright (c) 2015年 fujin. All rights reserved.
//

#import "HLAttentionViewController.h"
#import "HLShowCustomCell.h"
#import "HLShowSetFrame.h"
#import "HLStatusToolbar.h"

@interface HLAttentionViewController ()<UITableViewDataSource,UITableViewDelegate,HLStatusToolbarDelegate>
@property(nonatomic,strong)UITableView *tableView;
@end

@implementation HLAttentionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTableView];
    self.arrayData = [NSMutableArray array];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"fuckme" object:nil];
    
    [self hideExcessLine:_tableView];
    
}

-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}

- (void)updateData:(NSNotification *)not
{
    NSDictionary *dic = not.userInfo;
    HLShowSetFrame *model = dic[@"gg"];
    [self.arrayData addObject:model];
    [self.tableView reloadData];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
    NSLog(@"%@",self.arrayData);
}
- (void)setTableView{
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.backgroundColor = ZLColor(226, 226, 226);
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    //    self.tableView.backgroundColor = [UIColor blackColor];
    [self.view addSubview: self.tableView];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLShowCustomCell *cell = [HLShowCustomCell cellWithTableView:tableView index:indexPath ];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.tag = indexPath.row;
    cell.statusToolbar.delegate = self;
    [cell setSetFrame:self.arrayData[indexPath.row] andVC:self];
    return  cell;
    
}
#pragma mark - 实现代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 取出这行对应的frame模型
    HLShowSetFrame *frame = self.arrayData[indexPath.row];
    return frame.cellHeight;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
