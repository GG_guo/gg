//
//  ImagBroserVC.h
//  halo365
//
//  Created by GG on 16/3/24.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagBroserVC : UIViewController
@property (nonatomic, weak) UIView *imageParten;
@property (nonatomic, assign) NSInteger imageCount;

@property (nonatomic, assign) int currentImageIndex;
@property (nonatomic, assign) NSMutableArray *array;



@end
