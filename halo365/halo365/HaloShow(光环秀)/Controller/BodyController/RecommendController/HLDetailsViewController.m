//
//  HLDetailsViewController.m
//  halo365
//
//  Created by 李鹏辉 on 16/5/17.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLDetailsViewController.h"
#import "HLBaseShowViewController.h"
#import "HLShowModel.h"
#import "HLShowSetFrame.h"
#import "HLShowCustomCell.h"
#import "HLStatusToolbar.h"
#import "HLComeOnCell.h"
#import "HLCommentOnCell.h"
#import "HLSectionHeader.h"

@interface HLDetailsViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayDatas;

@end

@implementation HLDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];

    
    [self setTableView];
    [self setTextField];
}

//退出
- (void)exit
{
//    
//    HLBaseShowViewController *AS = [[HLBaseShowViewController alloc] init];
//    AS.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    
//    [self presentViewController:AS animated:YES completion:nil];
//    
//    
    [self dismissViewControllerAnimated:nil completion:nil];

    
//    [self]
}

- (void)setTextField
{
    UITextField *textFile = [[UITextField alloc]init];
    textFile.backgroundColor = [UIColor whiteColor];
    textFile.frame = CGRectMake(5, 10+CGRectGetMaxY(self.tableView.frame), self.view.frame.size.width-72, 44);
    textFile.placeholder = @"说点什么吧...";
    textFile.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:textFile];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(CGRectGetMaxX(textFile.frame)+2, 10+CGRectGetMaxY(self.tableView.frame), 60, 44);
    btn.backgroundColor = [UIColor whiteColor];
    btn.layer.cornerRadius = 5;
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    
    [btn setTitle:@"发送" forState:UIControlStateNormal];
    [self.view addSubview:btn];
}

- (void)setTableView
{
    self.tableView = [[UITableView alloc]init];
    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 128);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    self.tableViewr.frame = self.view.frame;
//    self.tableView.backgroundColor  = [UIColor greenColor];
    [self.view addSubview: self.tableView];
    
    _arrayDatas = [NSMutableArray array];
    [_arrayDatas addObject:self.detailModel];
//    HLShowSetFrame *setframe = [[HLShowSetFrame alloc]init];
//    HLShowModel *modelData = [[HLShowModel alloc]init];
//    modelData.moment = @"sdfksklajdflkjaskldfjklasjdflksajdfl";
//    modelData.dateline = @"1459318346";
//    modelData.avatar = @"http://img.halo365.cn/avatar/00000000/0000000013.jpg";
//    //            modelData.piclist = dic[@"piclist"];
//    modelData.nickname = @"kjnjknkjnsadf";
//    setframe.model = modelData;
//    [_arrayDatas addObject:setframe];

    _detailArray = [NSMutableArray arrayWithArray:_arrayDatas];

    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64);
    topView.backgroundColor = ZLColor(0, 122, 255);
    UIButton *exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exitButton.frame = CGRectMake(15, 32, 40, 20);
    [exitButton setTitle:@"退出" forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [exitButton addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:exitButton];
    [self.view addSubview:topView];
    
//    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 40)];
//    lable.text = [NSString stringWithFormat:@"点击的是第%ld行",(long)self.index];
//    [self.view addSubview:lable];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 2)
    {
        return 3;
    }
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
        case 0:
        {
            HLShowCustomCell *cell = [HLShowCustomCell cellWithTableView:tableView index:indexPath ];
            cell.tag = indexPath.row;
//            cell.statusToolbar.delegate = self;
            [cell setSetFrame:self.detailModel andVC:self];
            return cell;
        }
            break;
        case 1:
        {
            static NSString *cellid = @"ggcell";
            HLComeOnCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
            cell.backgroundColor = [UIColor clearColor];
            
            if(!cell)
            {
                cell = [[HLComeOnCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
                
            }
            return cell;
        }
            break;
        case 2:
        {
            static NSString *cellid = @"ggcell";
            HLCommentOnCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
            cell.backgroundColor = [UIColor clearColor];
            if(!cell)
            {
                cell = [[HLCommentOnCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
                
            }
            return cell;
        }
            break;
            
        default:
            break;
    }
    
    return  nil;
    
}

#pragma mark - 实现代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        HLShowSetFrame *frame = self.detailModel;
        return frame.cellHeight;
    }
    if(indexPath.section == 2)
    {
        return 80;
    }
    return 44;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 2)
    {
        return 40;
    }
    return 0;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section ==2)
    {
        return @"精彩评论";
    }
    return nil;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section ==2)
    {
        HLSectionHeader *view = [[HLSectionHeader alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
        return view;
    }
    return nil;
}

@end
