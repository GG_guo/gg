//
//  HLRecommendViewController.m
//  FlipTableView
//
//  Created by fujin on 15/7/9.
//  Copyright (c) 2015年 fujin. All rights reserved.
//

#import "HLRecommendViewController.h"
#import "HomeHeaderView.h"
#import "HLShowModel.h"
#import "HLShowSetFrame.h"
#import "HLShowCustomCell.h"
#import "HLStatusToolbar.h"
#import "HLDetailsViewController.h"
#import "HLComeOnCell.h"
#import "HLCommentOnCell.h"
#import "detailViewController2.h"
@interface HLRecommendViewController ()<UITableViewDataSource,UITableViewDelegate,FFScrollViewDelegate,HLStatusToolbarDelegate,UIActionSheetDelegate>

{
    BOOL header;
    NSInteger maxID;
    NSInteger minID;
    UIScrollView *view;
//    CLShareManager *shareView;
}

@property (nonatomic, strong) UITableView *tableView;

@property (strong, nonatomic) HomeHeaderView *homeHeaderView;

@property(nonatomic,strong)NSMutableArray *arrayNumer;

@property (nonatomic, strong) NSMutableArray *arrayDatas;

@end

@implementation HLRecommendViewController

//- (void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBarHidden = YES;
//    
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setupUserData];
    //分享
//    shareView = [[CLShareManager alloc] init];

}

//轮播图点击方法
- (void)scrollViewDidClickedAtPage:(NSInteger)pageNumber {
    
    NSLog(@"%ld",pageNumber);
    
}

//获取用户信息
- (void)setupUserData
{
    _arrayDatas = [NSMutableArray array];
    
    NSString *urlString = @"http://api.halo365.cn/v0/moment/max/0/min/0/uuid/0/";
    [HLAFNworkingHeader requestURL:urlString httpMethod:@"GET" token:nil params:nil file:nil success:^(id data) {
        //解析 装在model里面
        NSArray *array = data[@"msg"];
        for (NSDictionary *dic in array)
        {
            
            HLShowSetFrame *setframe = [[HLShowSetFrame alloc]init];
            HLShowModel *modelData = [[HLShowModel alloc]init];
            modelData.moment = dic[@"moment"];
            modelData.dateline = dic[@"dateline"];
            modelData.avatar = dic[@"avatar"];
            modelData.piclist = dic[@"piclist"];
            modelData.nickname = dic[@"nickname"];
            modelData.momentid = dic[@"momentid"];
            modelData.like_num = dic[@"like_num"];
            modelData.comment_num = dic[@"comment_num"];
            setframe.model = modelData;
            
            NSLog(@"like_num == %@",dic[@"like_num"]);
            
            [_arrayDatas addObject:setframe];
        }
        
        //        NSLog(@"arrayDatas = %@", arrayDatas);
//        NSLog(@"data == %@",data);
        self.view.userInteractionEnabled = YES;
        
        

        
        [self.tableView reloadData];
        [self setTableView];
        
    } fail:^(NSError *error) {
        NSLog(@"错误本身:%@ 错误原因:%@ 错误描述%@",
              error,
              error.localizedRecoverySuggestion,
              error.localizedDescription);
    }];
    
}

-(void)ClikedZLStatusToolbarBtn:(UIButton *)btn
{
 
    switch (btn.tag) {
        case 103:
        {

//            [shareView setShareVC:self content:@"测试分享" image:[UIImage imageNamed:@"test"] url:@"https://github.com/ClaudeLi/CLShare.git"];
//            [shareView show];

        
        }
         
            break;

        case 104:
           
            [self reloadAlert];
            
            break;
            
        default:
            break;
    }
}

- (void)reloadAlert
{
    //初始化提示框；
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //点击按钮的响应事件；
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }]];

    
    //弹出提示框；
    [self presentViewController:alert animated:true completion:nil];

}

- (void)setTableView
{
    _arrayNumer = [NSMutableArray array];
    self.tableView = [[UITableView alloc]init];
    self.tableView.backgroundColor = ZLColor(226, 226, 226);
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height );
    
    [self.view addSubview: self.tableView];
    
    self.homeHeaderView = [[HomeHeaderView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, SCREEN_HEIGHT/5)];
    
    self.tableView.tableHeaderView = self.homeHeaderView;
    self.homeHeaderView .picScrollView.pageViewDelegate = self;
    
    NSArray *adsArr = @[@"1.jpg",@"2.jpg",@"3.jpg",@"4.jpg"];
    [_homeHeaderView.picScrollView initWithImgs:adsArr];
    
    //刷新
    [self configureRefresh];
    //拿出 momentid
    [self findMaxAndMin];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addImageView:) name:@"clickImage" object:nil];
    
}

- (void)addImageView:(NSNotification *)notif
{
    //显示图片浏览器
    
    NSNumber *num = notif.userInfo[@"nums"];
    NSInteger num1 = num.integerValue;
    
    NSNumber *index = notif.userInfo[@"indexs"];
    NSInteger index1 = index.integerValue;
    
    HLShowSetFrame *models = self.arrayDatas[num1];
    
    view = [[UIScrollView alloc]initWithFrame:self.view.frame];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiddenView)];
    [view addGestureRecognizer:tap];
    
    view.backgroundColor = [UIColor blackColor];
    
    view.pagingEnabled = YES;
    view.contentSize = CGSizeMake(models.model.piclist.count *SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view insertSubview:view aboveSubview:self.tableView];
    
    for(int i = 0;i<models.model.piclist.count;i ++)
    {
        NSString *url = models.model.piclist[i];
        
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.frame = CGRectMake(i*SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:url]];
        
        [view addSubview:imageView];
        [view setContentOffset:CGPointMake(index1*SCREEN_WIDTH, 0) animated:NO];
        
    }
    
}

- (void)hiddenView
{
    if(view)
    {
        [view removeFromSuperview];
    }
}

- (void)findMaxAndMin
{
    for(HLShowSetFrame *data in self.arrayDatas)
    {
        NSString *ID = data.model.momentid;
        [_arrayNumer addObject:ID];
    }
    
    maxID = [[_arrayNumer valueForKeyPath:@"@max.intValue"] integerValue];
    minID = [[_arrayNumer valueForKeyPath:@"@min.intValue"] integerValue];
}

//刷新
- (void)configureRefresh
{
    //设置下拉刷新
    MJRefreshNormalHeader *header1 = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        //此处数据接口请求 列表刷新判断 建议封装的afn方法 保持代码的简洁性
        
        NSString *URL = [NSString stringWithFormat:@"http://api.halo365.cn/v0/moment/max/0/min/%ld",(long)maxID];
        header = YES;
        [self addataDic:URL];
        
        [self.tableView.header beginRefreshing];
        [self.tableView.header endRefreshing];
        
        
    }];
    self.tableView.header = header1;
    
    //设置上拉加载更多
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        NSString *URL = [NSString stringWithFormat:@"http://api.halo365.cn/v0/moment/max/%ld/min/0",(long)minID];
        header = NO;
        
        [self.tableView.footer beginRefreshing];
        [self.tableView.footer endRefreshing];
        [self addataDic:URL];
        
    }];
    self.tableView.footer = footer;
    
}

- (void)addataDic:(NSString *)url
{
    [HLAFNworkingHeader requestURL:url httpMethod:@"GET" token:nil params:nil file:nil success:^(id data) {
        
//        NSLog(@"data == %@",data);
        
        NSArray *array = data[@"msg"];
        
        for (NSDictionary *dic in array)
        {
            HLShowSetFrame *setframe = [[HLShowSetFrame alloc]init];
            HLShowModel *modelData = [[HLShowModel alloc]init];
            modelData.moment = dic[@"moment"];
            modelData.dateline = dic[@"dateline"];
            modelData.avatar = dic[@"avatar"];
            modelData.piclist = dic[@"piclist"];
            modelData.nickname = dic[@"nickname"];
            modelData.momentid = dic[@"momentid"];
            modelData.like_num = dic[@"like_num"];
            modelData.comment_num = dic[@"comment_num"];
            
            setframe.model = modelData;
            if(header)
            {
                [self.arrayDatas insertObject:setframe atIndex:0];
            }else{
                if ([dic[@"momentid"] isEqual:@"1"]) {
                    [self.tableView.footer endRefreshingWithNoMoreData];
                    
                }
                [self.arrayDatas addObject:setframe];
            }
        }
        
//        NSLog(@"data == %@",data);

        
        [self findMaxAndMin];
        
        [self.tableView reloadData];
    } fail:^(NSError *error) {
        NSLog(@"请求失败---%@",error);
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayDatas.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLShowCustomCell *cell = [HLShowCustomCell cellWithTableView:tableView index:indexPath ];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    __block UIViewController *VC = self;
    cell.touchBlock = ^(NSInteger num){
        
        detailViewController2 *vc = [[detailViewController2 alloc]init];
        vc.detailModel = self.arrayDatas[indexPath.row];
        [VC presentViewController:vc animated:YES completion:nil];
    };
    cell.tag = indexPath.row;
    cell.statusToolbar.delegate = self;
    [cell setSetFrame:self.arrayDatas[indexPath.row] andVC:self];
    return  cell;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLDetailsViewController *detailsVC = [[HLDetailsViewController alloc] init];
    detailsVC.index = indexPath.row;
    
    detailsVC.detailModel = self.arrayDatas[indexPath.row];
    
    [self presentViewController:detailsVC animated:nil completion:nil];
//    [self.navigationController pushViewController:detailsVC animated:YES];

//    NSLog(@"%ld",(long)indexPath.row);
}


#pragma mark - 实现代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 取出这行对应的frame模型
    HLShowSetFrame *frame = self.arrayDatas[indexPath.row];
    return frame.cellHeight;
}

@end
