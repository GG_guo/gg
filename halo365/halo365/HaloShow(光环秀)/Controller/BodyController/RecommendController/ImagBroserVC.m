//
//  ImagBroserVC.m
//  halo365
//
//  Created by GG on 16/3/24.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "ImagBroserVC.h"
#import "BroserView.h"


@interface ImagBroserVC ()<UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *scrollView;

@end

@implementation ImagBroserVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];

    [self addScrollView];

}

- (void)addScrollView
{
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.frame = self.view.bounds;
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;

    _scrollView.contentSize = CGSizeMake(self.array.count*SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view addSubview:_scrollView];
    _scrollView.hidden = YES;
    for(int i = 0;i<self.array.count;i++)
    {
        BroserView *view = [[BroserView alloc]initWithFrame:CGRectMake(i*SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_scrollView addSubview:view];
//        view.backgroundColor = [UIColor blueColor];
        [view setData:self.array index:i];
        
        //处理单击
        __weak __typeof(self)weakSelf = self;
        view.singleTapBlock = ^(UITapGestureRecognizer *recognizer){
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf hidePhotoBrowser:recognizer];
        };
        //长按处理
        view.longPressBlock = ^(UILongPressGestureRecognizer *recognizer)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf saveImage:recognizer];
        };
        
    }
    
[_scrollView setContentOffset:CGPointMake(self.currentImageIndex*SCREEN_WIDTH, 0) animated:NO];\
    
}
- (void)saveImage:(UILongPressGestureRecognizer *)recognizer
{
//    NSInteger offset = _scrollView.contentOffset.x/SCREEN_WIDTH;
    BroserView *view = (BroserView *)recognizer.view;
    UIImageView *imageView = view.imageview;
    UIImageWriteToSavedPhotosAlbum(imageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo

{
    
    
    
    if (error != NULL) {
        
        UIAlertView *photoSave = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [photoSave show];
        
    }else {
        
        UIAlertView *photoSave = [[UIAlertView alloc] initWithTitle:@"保存成功" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [photoSave show];
    
    }
    
}
- (void)hidePhotoBrowser:(UITapGestureRecognizer *)recognizer
{
    NSInteger offset = _scrollView.contentOffset.x/SCREEN_WIDTH;
    BroserView *view = (BroserView *)recognizer.view;
    UIImageView *currentImageView = view.imageview;
    
    UIView *sourceView = self.imageParten.subviews[offset];
    
    UIView *parentView = [self getParsentView:sourceView];
    CGRect targetTemp = [sourceView.superview convertRect:sourceView.frame toView:parentView];
    
    
    UIView *covre = [[UIView alloc]initWithFrame:targetTemp];
    covre.backgroundColor = [UIColor whiteColor];
    [parentView addSubview:covre];
    
    CGFloat appWidth = SCREEN_WIDTH;
    CGFloat appHeight = SCREEN_HEIGHT;
    
    UIImageView *tempImageView = [[UIImageView alloc] init];
    tempImageView.image = currentImageView.image;
    if (tempImageView.image) {
        CGFloat tempImageSizeH = tempImageView.image.size.height;
        CGFloat tempImageSizeW = tempImageView.image.size.width;
        CGFloat tempImageViewH = (tempImageSizeH * appWidth)/tempImageSizeW;
        if (tempImageViewH < appHeight) {
            tempImageView.frame = CGRectMake(0, (appHeight - tempImageViewH)*0.5, appWidth, tempImageViewH);
        } else {
            tempImageView.frame = CGRectMake(0, 0, appWidth, tempImageViewH);
        }
    } else {
        tempImageView.backgroundColor = [UIColor whiteColor];
        tempImageView.frame = CGRectMake(0, (appHeight - appWidth)*0.5, appWidth, appWidth);
    }
    [self.view.window addSubview:tempImageView];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    [UIView animateWithDuration:0.5 animations:^{
        tempImageView.frame = targetTemp;
        
    } completion:^(BOOL finished) {
        [tempImageView removeFromSuperview];
        [covre removeFromSuperview];
    }];
}
- (void)dismissVC
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showImageBroser];
}
- (void)setArray:(NSMutableArray *)array
{
    if(!_array)
    {
        _array  = array;
    }
}
- (void)showImageBroser
{


    UIView *sourceView = self.imageParten.subviews[self.currentImageIndex];

    UIView *parentView = [self getParsentView:sourceView];
    CGRect rect = [sourceView.superview convertRect:sourceView.frame toView:parentView];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = rect;
    imageView.contentMode = UIViewContentModeScaleAspectFit;

    
//    imageView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:imageView];
    [imageView sd_setImageWithURL:[NSURL URLWithString:self.array[self.currentImageIndex]]];
    
    
    CGFloat placeImageSizeW = imageView.image.size.width;
    CGFloat placeImageSizeH = imageView.image.size.height;
    CGRect targetTemp;
    CGFloat placeHolderH = (placeImageSizeH * SCREEN_WIDTH)/placeImageSizeW;
    if (placeHolderH <= SCREEN_HEIGHT) {
        targetTemp = CGRectMake(0, (SCREEN_HEIGHT - placeHolderH) * 0.5 , SCREEN_WIDTH, placeHolderH);
    } else {
        targetTemp = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }

    [UIView animateWithDuration:0.5 animations:^{
        imageView.frame = targetTemp;
    } completion:^(BOOL finished) {
    
        [imageView removeFromSuperview];
        _scrollView.hidden = NO;

    }];
    
    
}

#pragma mark 获取控制器的view
- (UIView *)getParsentView:(UIView *)view{
    if ([[view nextResponder] isKindOfClass:[UIViewController class]] || view == nil) {
        return view;
    }
    return [self getParsentView:view.superview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
