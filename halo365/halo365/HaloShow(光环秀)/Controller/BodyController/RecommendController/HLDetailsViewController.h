//
//  HLDetailsViewController.h
//  halo365
//
//  Created by 李鹏辉 on 16/5/17.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLShowSetFrame.h"

@interface HLDetailsViewController : UIViewController

@property(nonatomic,assign) NSInteger index;
@property(nonatomic, strong) NSString *str;
@property(nonatomic,strong)NSMutableArray *detailArray;

@property(nonatomic,strong)HLShowSetFrame *detailModel;

@end
