//
//  UIRightViewController.m
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/25.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import "HLRightViewController.h"
#import "HLTextView.h"
#import "HLComposeToolbar.h"
#import "HLComposePhotosView.h"
#define HLNotificationCenter [NSNotificationCenter defaultCenter]

@interface HLRightViewController ()<UITextViewDelegate, HLComposeToolbarDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>


@property (nonatomic, weak) HLTextView *textView;
@property (nonatomic, weak) HLComposeToolbar *toolbar;
@property (nonatomic, weak) HLComposePhotosView *photosView;

@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *sendButton;

@end

@implementation HLRightViewController

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;

}
- (void)viewDidLoad {
    [super viewDidLoad];

    // 设置导航栏属性
    [self setupNavBar];
    
    // 添加textView
    [self setupTextView];
    
    // 添加toolbar
    [self setupToolbar];
    
    // 添加photosView
    [self setupPhotosView];

}

// 设置导航栏属性
- (void)setupNavBar
{
//    self.view.backgroundColor = [UIColor lightGrayColor];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(cancelButton)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleDone target:self action:@selector(send)];
//    self.navigationItem.rightBarButtonItem.enabled = NO;
//    self.title = @"发状态";
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_cancelButton];
    [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(32);
        make.left.offset(15);
        make.width.offset(40);
        make.height.offset(20);
    }];
    
    _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [_sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    _sendButton.enabled = NO;
//    if (_sendButton.enabled == (self.textView.text.length != 0)) {
//        [_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//
//    }
    [_sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    [self.view addSubview:_sendButton];
    [_sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(32);
        make.right.offset(-15);
        make.width.offset(40);
        make.height.offset(20);
    }];
    
    
}

- (void)setupTextView
{
    // 1.添加
    HLTextView *textView = [[HLTextView alloc] init];
    textView.font = [UIFont systemFontOfSize:15];
//    textView.frame = self.view.bounds;
    textView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT -64);
    // 垂直方向上永远可以拖拽
    textView.alwaysBounceVertical = YES;
    textView.delegate = self;
    textView.placeholder = @"分享新鲜事...";
    [self.view addSubview:textView];
    self.textView = textView;
    
    // 2.监听textView文字改变的通知
    [HLNotificationCenter addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:textView];
    // 3.监听键盘的通知
    [HLNotificationCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [HLNotificationCenter addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)setupToolbar
{
    HLComposeToolbar *toolbar = [[HLComposeToolbar alloc] init];
    toolbar.delegate = self;
    CGFloat toolbarH = 44;
    CGFloat toolbarW = self.view.frame.size.width;
    CGFloat toolbarX = 0;
    CGFloat toolbarY = self.view.frame.size.height - toolbarH;
    toolbar.frame = CGRectMake(toolbarX, toolbarY, toolbarW, toolbarH);
    [self.view addSubview:toolbar];
    self.toolbar = toolbar;
}

#pragma mark - toolbar的代理方法
- (void)composeToolbar:(HLComposeToolbar *)toolbar didClickedButton:(IWComposeToolbarButtonType)buttonType
{
    switch (buttonType) {
        case IWComposeToolbarButtonTypeCamera: // 相机
            [self openCamera];
            break;
            
        case IWComposeToolbarButtonTypePicture: // 相册
            [self openPhotoLibrary];
            break;
            
        default:
            break;
    }
}

//打开相机
- (void)openCamera
{
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.delegate = self;
    [self presentViewController:ipc animated:YES completion:nil];
}

//打开相册
- (void)openPhotoLibrary
{
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.delegate = self;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - 图片选择控制器的代理
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // 1.销毁picker控制器
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // 2.去的图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self.photosView addImage:image];
    
    NSLog(@"%@", info);
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.textView becomeFirstResponder];
}

// 监听文字改变
- (void)textDidChange
{
//    self.navigationItem.rightBarButtonItem.enabled = (self.textView.text.length != 0);
    _sendButton.enabled = (self.textView.text.length != 0);
    [_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

}

- (void)dealloc
{
    [HLNotificationCenter removeObserver:self];
}

//键盘即将显示的时候调用
- (void)keyboardWillShow:(NSNotification *)note
{
    // 1.取出键盘的frame
    CGRect keyboardF = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    // 2.取出键盘弹出的时间
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // 3.执行动画
    [UIView animateWithDuration:duration animations:^{
        self.toolbar.transform = CGAffineTransformMakeTranslation(0, -keyboardF.size.height);
    }];
}

//键盘即将退出的时候调用
- (void)keyboardWillHide:(NSNotification *)note
{
    // 1.取出键盘弹出的时间
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // 2.执行动画
    [UIView animateWithDuration:duration animations:^{
        self.toolbar.transform = CGAffineTransformIdentity;
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}


//取消
- (void)clickCancelButton
{
    [self dismissViewControllerAnimated:nil completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

//发微博
- (void)send
{
    [self dismissViewControllerAnimated:nil completion:nil];

    if (self.photosView.totalImages.count) { // 有图片
        [self sendWithImage];
    } else { // 没有图片
        [self sendWithoutImage];
    }
    
    // 关闭控制器
    [self dismissViewControllerAnimated:YES completion:nil];
}


/**
 *  发有图片的微博
 */
- (void)sendWithImage
{
    // 1.封装请求参数
    //    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //    params[@"status"] = self.textView.text;
    //    params[@"access_token"] = [IWAccountTool account].access_token;
    //
    //    // 2.封装文件参数
    //    NSMutableArray *formDataArray = [NSMutableArray array];
    //    NSArray *images = [self.photosView totalImages];
    //    for (UIImage *image in images) {
    //        IWFormData *formData = [[IWFormData alloc] init];
    //        formData.data = UIImageJPEGRepresentation(image, 0.000001);
    //        formData.name = @"pic";
    //        formData.mimeType = @"image/jpeg";
    //        formData.filename = @"";
    //        [formDataArray addObject:formData];
    //    }
    //
    //    // 3.发送请求
    //    [IWHttpTool postWithURL:@"https://upload.api.weibo.com/2/statuses/upload.json" params:params formDataArray:formDataArray success:^(id json) {
    //        [MBProgressHUD showSuccess:@"发送成功"];
    //    } failure:^(NSError *error) {
    //        [MBProgressHUD showError:@"发送失败"];
    //    }];
}

/**
 *  发没有图片的微博
 */
- (void)sendWithoutImage
{
//    // 1.封装请求参数
//    IWSendStatusParam *param = [IWSendStatusParam param];
//    param.status = self.textView.text;
//    
//    // 2.发送请求
//    [IWStatusTool sendStatusWithParam:param success:^(IWSendStatusResult *result) {
//        [MBProgressHUD showSuccess:@"发送成功"];
//    } failure:^(NSError *error) {
//        [MBProgressHUD showError:@"发送失败"];
//    }];
}


//添加photosView
- (void)setupPhotosView
{
    HLComposePhotosView *photosView = [[HLComposePhotosView alloc] init];
    CGFloat photosW = self.textView.frame.size.width;
    CGFloat photosH = self.textView.frame.size.height;
    CGFloat photosY = 80;
    photosView.frame = CGRectMake(0, photosY, photosW, photosH);
    [self.textView addSubview:photosView];
    self.photosView = photosView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
