//
//  UILeftViewController.m
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/25.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import "HLLeftViewController.h"
#import "Masonry.h"

@interface HLLeftViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) NSArray *cellName;
@property(nonatomic, strong) UITableView *tableView;

@end

@implementation HLLeftViewController

//- (void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBarHidden = NO;
//    
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self setupNavBar1];

    [self setView];
}

- (void)setupNavBar1
{
    
//   UIButton *_cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
//    [_cancelButton addTarget:self action:@selector(cancelButton) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_cancelButton];
//    [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.offset(32);
//        make.left.offset(15);
//        make.width.offset(40);
//        make.height.offset(20);
//    }];

}

- (void)setView
{
    _cellName = @[@"评论",@"@",@"赞",@"粉丝"];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_tableView];
}

- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

////取消按钮
//- (void)cancelButton
//{
//    [self.navigationController popViewControllerAnimated:YES];
////    [self dismissViewControllerAnimated:nil completion:nil];
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( section  == 0) {
        return _cellName.count;

    }
    return 1;
}

//指定标题的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section ==0) {
        return 0;
    }
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.section == 0) {
        return 50;
    }
    
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrderListCell";
    
    //自定义cell类有图片
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 12.5, 150, 25)];
        cellLabel.text = _cellName[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        [cell addSubview:cellLabel];

    }
    if (indexPath.section ==1) {
       
//        UILabel *cellLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(45, 12.5, 150, 25)];
//        cellLabel1.text = @"ahjdfjkhaksjhdjkfhaskjdhfjkhasjkdhfjkashdkjfhasjkdfhjkashdfjkhasjkdfhjkashdfkjhaksjdfhkjashdkjfjkahsdjkfhakjsdhfj";
//        [cell addSubview:cellLabel1];
        
        UIImageView *image = [[UIImageView alloc] init];
        image.image = [UIImage imageNamed:@"图标"];
        image.layer.masksToBounds = YES;
        image.layer.cornerRadius = 15;
        [cell addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(12.5);
            make.left.offset(15);
            make.width.offset(30);
            make.height.offset(30);
            
        }];
        
        UILabel *nameLabel = [[UILabel alloc] init];
        nameLabel.text = @"halo365";
//        nameLabel.backgroundColor = [UIColor redColor];
        [cell addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(image.mas_top);
            make.left.equalTo(image.mas_right).with.offset(10);
            make.width.offset(100);
            make.height.equalTo(image.mas_height);
            
        }];
        
        
        UILabel *timeLabel = [[UILabel alloc] init];
        timeLabel.text = @"2016/05/21";
        [cell addSubview:timeLabel];
        [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(image.mas_top);
            make.right.offset(-15);
            make.width.offset(100);
            make.height.equalTo(image.mas_height);
        }];
        
        UILabel *detailLabel = [[UILabel alloc] init];
//        detailLabel.backgroundColor = [UIColor redColor];
        detailLabel.numberOfLines = -1;
        detailLabel.font = [UIFont systemFontOfSize:13];
        detailLabel.text = @"从牛肉中摄取蛋白质是极好的，但是全牛中含有大量的脂肪和胆固醇。你可以点击查询牛肉分离蛋白。你会得知，牛肉分离蛋白为你提供优质蛋白的同时也隐藏摄入了不健康的营养素，即便在精瘦肉中。牛肉蛋白是从美国农业部（USDA）中摘录的，其中的分解蛋白经加热后可去除。胆固醇和脂肪将不复存在。最后去除水分。这些被提纯的蛋白粉末被简易包装成一个个独立且等份的增肌补剂。";

        CGSize detailSize = [self sizeWithText:detailLabel.text font:detailLabel.font maxSize:CGSizeMake(SCREEN_WIDTH - 45, MAXFLOAT)];
        
        [cell addSubview:detailLabel];
        [detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(image.mas_bottom).with.offset(0);
            make.left.equalTo(image.mas_right).with.offset(10);
            make.right.equalTo(timeLabel.mas_right);
            make.width.offset(detailSize.width);
            make.height.offset(detailSize.height);
        }];
        
        UIImageView *detailImage = [[UIImageView alloc] init];
        detailImage.image = [UIImage imageNamed:@"1"];
        [cell addSubview:detailImage];
        [detailImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(detailLabel.mas_bottom);
            make.left.equalTo(image.mas_right).with.offset(10);
            make.right.equalTo(timeLabel.mas_right);
            make.width.offset(SCREEN_WIDTH - 45);
            make.height.offset(150);
        }];
        
        }
   
    }
    
    return cell;
}

@end
