//
//  ViewController.m
//  FlipTableView
//
//  Created by fujin on 15/7/9.
//  Copyright (c) 2015年 fujin. All rights reserved.
//

#import "HLBaseShowViewController.h"
#import "SegmentTapView.h"
#import "FlipTableView.h"

#import "HLRecommendViewController.h"
#import "HLAttentionViewController.h"
#import "HLLatestViewController.h"

#import "HLLeftViewController.h"
#import "HLRightViewController.h"

#define ScreeFrame [UIScreen mainScreen].bounds
@interface HLBaseShowViewController ()<SegmentTapViewDelegate,FlipTableViewDelegate,UINavigationControllerDelegate>
{
    HLRecommendViewController *v1;
    HLAttentionViewController *v2;
    HLLatestViewController *v3;
}
@property (nonatomic, strong)SegmentTapView *segment;
@property (nonatomic, strong)FlipTableView *flipView;
@property (strong, nonatomic) NSMutableArray *controllsArray;
@end

@implementation HLBaseShowViewController

//- (void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBarHidden = YES;
    
//}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"光环秀";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initSegment];
    [self initFlipTableView];
    [self initNavigationItem];
}

- (void)initNavigationItem
{
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"navigationbar_addbuddy" highIcon:@"navigationbar_addbuddyHL" target:self action:@selector(clickLeftItem)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"navigationbar_ content" highIcon:@"navigationbar_ contentHL" target:self action:@selector(clickRightItem)];
    
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = [UIImage imageNamed:@"halo365"];
    iconView.frame = CGRectMake((SCREEN_WIDTH - 101)/2, 9, 101, 53/2);
    [self.navigationController.navigationBar addSubview:iconView];
    

}

- (void)clickLeftItem
{
    HLLeftViewController *leftVC = [[HLLeftViewController alloc] init];
//    leftVC.view.backgroundColor = [UIColor purpleColor];
    [self.navigationController pushViewController:leftVC animated:YES];
//    [self presentViewController:leftVC animated:nil completion:nil];
}

- (void)clickRightItem
{
    HLRightViewController *rightVC = [[HLRightViewController alloc] init];
    rightVC.view.backgroundColor = [UIColor darkGrayColor];
    [self presentViewController:rightVC animated:YES completion:nil];
}

-(void)initSegment{
    self.segment = [[SegmentTapView alloc] initWithFrame:CGRectMake(0, 64, ScreeFrame.size.width, 44) withDataArray:[NSArray arrayWithObjects:@"推荐",@"关注",@"最新",nil] withFont:15];
    self.segment.delegate = self;
//    self.segment.backgroundColor  = ZLColor(238, 238, 238);
    [self.view addSubview:self.segment];
    
}

-(void)initFlipTableView{
    if (!self.controllsArray) {
        self.controllsArray = [[NSMutableArray alloc] init];
    }
    
    v1 = [[HLRecommendViewController alloc] init];
//    v1.view.backgroundColor = [UIColor blueColor];
    v2 = [[HLAttentionViewController alloc] init];
    v2.view.backgroundColor = [UIColor yellowColor];
    
    v3 = [[HLLatestViewController alloc] init];
    v3.view.backgroundColor = [UIColor lightGrayColor];

    [self.controllsArray addObject:v1];
    [self.controllsArray addObject:v2];
    [self.controllsArray addObject:v3];
//    [self.controllsArray addObject:v4];
    
    self.flipView = [[FlipTableView alloc] initWithFrame:CGRectMake(0, 108, ScreeFrame.size.width, self.view.frame.size.height - 108) withArray:_controllsArray];
    self.flipView.delegate = self;
    [self.view addSubview:self.flipView];
}

#pragma mark -------- select Index
-(void)selectedIndex:(NSInteger)index
{
    NSLog(@"aa%ld",index);
    [self.flipView selectIndex:index];
    
}
-(void)scrollChangeToIndex:(NSInteger)index
{
    NSLog(@"bb%ld",index);
    [self.segment selectIndex:index];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
