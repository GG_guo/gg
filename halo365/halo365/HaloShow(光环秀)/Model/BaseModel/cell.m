//
//  cell.m
//  halo_demo
//
//  Created by GG on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "cell.h"

@implementation cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"cell";
    cell *cells = [tableView dequeueReusableCellWithIdentifier:ID];
    
    
    if (cells == nil) {
        cells = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:nil options:nil][0];
    }
    cells.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cells;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
