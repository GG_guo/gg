//
//  detailViewController2.h
//  halo_demo
//
//  Created by GG on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLShowSetFrame.h"

@interface detailViewController2 : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tableView;
}
@property(nonatomic,assign) NSInteger index;
@property(nonatomic, strong) NSString *str;
@property(nonatomic,strong)NSMutableArray *detailArray;

@property(nonatomic,strong)HLShowSetFrame *detailModel;


@end
