//
//  fouckVC.m
//  halo_demo
//
//  Created by GG on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "fouckVC.h"
#import "fouckCell.h"
@interface fouckVC ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation fouckVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64);
    topView.backgroundColor = ZLColor(0, 122, 255);
    UIButton *exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exitButton.frame = CGRectMake(15, 32, 40, 20);
    [exitButton setTitle:@"退出" forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [exitButton addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:exitButton];
    [self.view addSubview:topView];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    fouckCell *cell = [fouckCell cellWithTableView:tableView];
    return cell;
}
- (void)exit
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
