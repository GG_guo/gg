//
//  detailViewController2.m
//  halo_demo
//
//  Created by GG on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "detailViewController2.h"
#import "detailViewContriller.h"
#import "cell.h"
#import "fouckVC.h"
#import "imgeVC.h"
#import "HLBaseShowViewController.h"

@interface detailViewController2 ()

@end

@implementation detailViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initContentView];
    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64);
    topView.backgroundColor = ZLColor(0, 122, 255);
    UIButton *exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exitButton.frame = CGRectMake(15, 32, 40, 20);
    [exitButton setTitle:@"退出" forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [exitButton addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:exitButton];
    [self.view addSubview:topView];
    
    
}
- (void)exit{

    [self dismissViewControllerAnimated:YES completion:nil];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 4;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    cell *cells = [cell cellWithTableView:tableView];
    return cells;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 134;
}
- (void)initContentView{
    
    detailViewContriller *view = [[detailViewContriller alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 200)];
    __block UIViewController *viewcontroler = self;

    view.TouchBlock = ^(NSInteger num){

        switch (num) {
            case 0:
                [viewcontroler presentViewController:[[imgeVC alloc]init] animated:YES completion:nil];
                break;
            case 1:
                
                break;
            case 2:
                [viewcontroler presentViewController:[[fouckVC alloc]init] animated:YES completion:nil];
                break;
            case 3:
            {
                NSLog(@"%@",self.detailModel);
                NSDictionary *dic = @{@"gg":self.detailModel};
//                [array addObject:self.detailModel];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"fuckme" object:nil userInfo:dic];
            }
                break;
            
                
            default:
                break;
        }
    };
//    __block UIViewController *viewcontroler = self;
//    NSLog(@"%d",self.retainCount);
//    button.block = ^(BlockButton *btn){
//        //block将当前对象self， retain了一下
//        [viewcontroler dismissModalViewControllerAnimated:YES];
//        
//        
//        //如果使用self的属性或者方法，block会将self retain一下, 正是这个retain导致了循环引用
//        //        [self dismissModalViewControllerAnimated:YES];
//        NSLog(@"%d",viewcontroler.retainCount);
//    };
    
//    [self.view addSubview:view];
    tableView = [[UITableView alloc]init];
    tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.tableHeaderView = view;
    
    [self.view addSubview:tableView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
