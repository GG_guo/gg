//
//  SetFrame.m
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//


#import "HLShowSetFrame.h"
#import "HLShowModel.h"


//设备的宽高
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width

@implementation HLShowSetFrame

//计算文字尺寸

- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (void)setModel:(HLShowModel *)model
{
    _model = model;
    //自空间之间的间距
    
    CGFloat padding = 10;
    //1.头像
    
    CGFloat iconX = padding;
    CGFloat iconY = padding;
    CGFloat iconW = 30;
    CGFloat iconH = 30;
    _iconF = CGRectMake(iconX, iconY, iconW, iconH);
    
    // 2.昵称
    // 文字的字体
    CGSize nameSize = [self sizeWithText:self.model.nickname font:GGNameFont maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    CGFloat nameX = CGRectGetMaxX(_iconF) + padding;
    CGFloat nameY = iconY + (iconH - nameSize.height) *0.5;
    _nicknameF = CGRectMake(nameX, nameY, nameSize.width, nameSize.height);
    
    //3.时间
    CGFloat timeX = CGRectGetMaxX(_nicknameF) +padding;
    CGFloat timeY = nameY;
    CGSize timeSize = [self sizeWithText:self.model.dateline font:GGNameFont maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    _datalineF = CGRectMake(timeX, timeY, kDeviceWidth - CGRectGetMaxX(_nicknameF)-20, timeSize.height);
    
    //4.正文
    CGFloat contentX = iconX;
    CGFloat contentY = CGRectGetMaxX(_iconF) +padding;
    CGSize contentSize = [self sizeWithText:self.model.moment font:GGTextFont maxSize:CGSizeMake(kDeviceWidth - 20, MAXFLOAT)];
    _contentF = CGRectMake(contentX, contentY, contentSize.width, contentSize.height);
    
    //5.发布的图片
    NSLog(@"%@",self.model.piclist);
    
    if([self.model.piclist isKindOfClass:[NSArray class]]&&self.model.piclist.count)
    {
        CGFloat pictureX = contentX;
        CGFloat pictureY = CGRectGetMaxY(_contentF) + padding;
        CGFloat pictureW = kDeviceWidth - 20;
        CGFloat pictureH = pictureW;
        _pictureF = CGRectMake(pictureX, pictureY, pictureW, pictureH);
        
        //6.工具条
        
        CGFloat statusToolbarX = iconX;
        CGFloat statusToolbarY = CGRectGetMaxY(_pictureF)+ padding;
        CGFloat statusToolbarW = [UIScreen mainScreen].bounds.size.width;
        CGFloat statusToolbarH = 35;
        
        _statusToolbarF = CGRectMake(statusToolbarX, statusToolbarY, statusToolbarW, statusToolbarH);
        
        _cellHeight = CGRectGetMaxY(_statusToolbarF) + padding;
        
    } else {
        CGFloat statusToolbarX = 0;
        CGFloat statusToolbarY = CGRectGetMaxY(_contentF)+ padding;
        CGFloat statusToolbarW = [UIScreen mainScreen].bounds.size.width - padding;
        CGFloat statusToolbarH = 35;
        
        _statusToolbarF = CGRectMake(statusToolbarX, statusToolbarY, statusToolbarW, statusToolbarH);
        _cellHeight = CGRectGetMaxY(_statusToolbarF) + padding;
    }
    
}

@end

