//
//  SetFrame.h
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class HLShowModel;

@interface HLShowSetFrame : NSObject

//头像的frame
@property (nonatomic, assign, readonly) CGRect iconF;

//昵称的frame
@property (nonatomic, assign, readonly) CGRect nicknameF;

//发布时间
@property (nonatomic, assign, readonly) CGRect datalineF;

//正文的frame
@property (nonatomic, assign, readonly) CGRect contentF;

//配图的frame
@property (nonatomic, assign, readonly) CGRect pictureF;

//cell的高度
@property (nonatomic, assign, readonly) CGFloat cellHeight;

//halo的工具条
@property (nonatomic, assign, readonly) CGRect statusToolbarF;

@property (nonatomic, strong) HLShowModel *model;

@end
