//
//  ModelData.h
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLShowModel : NSObject

@property (nonatomic, strong) NSString *moment;//moment 内容 文字
@property (nonatomic, strong) NSString *dateline;//dateline 显示时间
@property (nonatomic, strong) NSString *avatar;//avatar 头像
@property (nonatomic, strong) NSArray  *piclist;//pickiest 发布图片
@property (nonatomic, strong) NSString *nickname;//nickname 昵称
@property (nonatomic, strong) NSString *momentid;//id 昵称
@property (nonatomic, strong) NSString *comment_num;//评论数
@property (nonatomic, strong) NSString *like_num;//赞数

@end
