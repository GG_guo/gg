//
//  HLComposePhotosView.h
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/25.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLComposePhotosView : UIView

//添加一张新的图片
- (void)addImage:(UIImage *)image;

//返回内部所有的图片
- (NSArray *)totalImages;

@end
