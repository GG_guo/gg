//
//  HLComposeToolbar.h
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/25.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HLComposeToolbar;

typedef enum {
    IWComposeToolbarButtonTypeCamera,
    IWComposeToolbarButtonTypePicture,
    IWComposeToolbarButtonTypeMention,
    IWComposeToolbarButtonTypeTrend,
    IWComposeToolbarButtonTypeEmotion
} IWComposeToolbarButtonType;

@protocol HLComposeToolbarDelegate <NSObject>
@optional
- (void)composeToolbar:(HLComposeToolbar *)toolbar didClickedButton:(IWComposeToolbarButtonType)buttonType;
@end


@interface HLComposeToolbar : UIView

@property (weak, nonatomic) id<HLComposeToolbarDelegate> delegate;

@end
