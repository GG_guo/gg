//
//  HLTextView.h
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/25.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLTextView : UITextView

@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;

@end
