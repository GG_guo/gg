//
//  CustomView.h
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLShowCustomView : UIView

{
    NSInteger num;
    UIViewController *VC;
}

@property(nonatomic,strong)NSMutableArray *imageDatas;
@property(nonatomic,strong)UIScrollView *scroView;


- (void)setFrames:(CGRect )frame with:(NSArray *)array and:(UIViewController*)vc ;

@end
