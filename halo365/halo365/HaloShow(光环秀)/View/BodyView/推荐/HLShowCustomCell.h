//
//  CustomCell.h
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HLShowSetFrame,HLStatusToolbar;
@interface HLShowCustomCell : UITableViewCell

@property (nonatomic, strong) HLShowSetFrame *setFrame;

@property (nonatomic, weak) HLStatusToolbar *statusToolbar;
@property(nonatomic,copy) void(^touchBlock)(NSInteger);

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSIndexPath*)indexPath;
- (void)setSetFrame:(HLShowSetFrame *)setFrame andVC:(UIViewController *)vc;

@end
