//
//  HomeHeaderView.m
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/23.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import "HomeHeaderView.h"

@implementation HomeHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
//        [self setButton];
        [self setPicScrollView];
    }
    return self;
}

//- (void)setButton
//{
//    UIButton *headerButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    headerButton.frame = CGRectMake(0,0, self.frame.size.width, self.frame.size.height);
//    [headerButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:headerButton];
//    
//}

- (void)setPicScrollView
{
    _picScrollView = [[FFScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    [self addSubview:_picScrollView];
}

//- (void)btnClick:(UIButton *)sender
//{
//    
//}

@end
