//
//  HomeHeaderView.h
//  FlipTableView
//
//  Created by 李鹏辉 on 16/5/23.
//  Copyright © 2016年 fujin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFScrollView.h"

@interface HomeHeaderView : UIView

@property (nonatomic, strong)  FFScrollView *picScrollView;

@end
