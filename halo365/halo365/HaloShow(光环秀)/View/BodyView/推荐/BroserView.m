//
//  BroserView.m
//  halo365
//
//  Created by GG on 16/3/25.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "BroserView.h"
#import "FGTProgressHUD.h"

@interface BroserView()<SDWebImageManagerDelegate>
{
    SDWebImageManager *manager;
}
@property (nonatomic,strong) UIScrollView *scrollview;
@property (nonatomic,strong) UITapGestureRecognizer *doubleTap;
@property (nonatomic,strong) UITapGestureRecognizer *singleTap;
@property (nonatomic,strong) UILongPressGestureRecognizer *longPress;


@end
@implementation BroserView

- (id)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        
        [self addSubview: self.scrollview];
        [self addGestureRecognizer:self.doubleTap];
        [self addGestureRecognizer:self.singleTap];
        [self addGestureRecognizer:self.longPress];
        
        
    }
    return self;
}

- (UILongPressGestureRecognizer *)longPress
{
    if(!_longPress)
    {
        _longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressAction:)];
    }
    return _longPress;
}
- (UIScrollView *)scrollview
{
    if(!_scrollview)
    {
        _scrollview = [[UIScrollView alloc] init];
        _scrollview.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        _scrollview.delegate = self;
        _scrollview.clipsToBounds = YES;
        [_scrollview addSubview:self.imageview];
        self.scrollview.minimumZoomScale = 0.5;
        self.scrollview.maximumZoomScale = 2;

    }
    return _scrollview;
}

- (UIImageView *)imageview
{
    if (!_imageview) {
        _imageview = [[UIImageView alloc] init];
        _imageview.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        _imageview.contentMode =  UIViewContentModeScaleAspectFit;

        _imageview.userInteractionEnabled = YES;
    }
    return _imageview;
}

- (UITapGestureRecognizer *)doubleTap
{
    if (!_doubleTap) {
        _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        _doubleTap.numberOfTapsRequired = 2;
        _doubleTap.numberOfTouchesRequired  =1;
    }
    return _doubleTap;
}
- (UITapGestureRecognizer *)singleTap
{
    if (!_singleTap) {
        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        _singleTap.numberOfTapsRequired = 1;
        _singleTap.numberOfTouchesRequired = 1;
        //只能有一个手势存在
        [_singleTap requireGestureRecognizerToFail:self.doubleTap];
        
    }
    return _singleTap;

}
- (void)longPressAction:(UILongPressGestureRecognizer *)recognizer
{
    if(self.longPressBlock)
    {
        self.longPressBlock(recognizer);
    }
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{

    if (self.singleTapBlock) {
        self.singleTapBlock(recognizer);
    }
}
- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self];
    if (self.scrollview.zoomScale <= 1.0) {
        
        CGFloat scaleX = touchPoint.x + self.scrollview.contentOffset.x;//需要放大的图片的X点
        CGFloat sacleY = touchPoint.y + self.scrollview.contentOffset.y;//需要放大的图片的Y点
        [self.scrollview zoomToRect:CGRectMake(scaleX, sacleY, 10, 10) animated:YES];
        
    } else {
        [self.scrollview setZoomScale:1.0 animated:YES]; //还原
    }
}
- (void)setData:(NSMutableArray *)array index:(NSInteger )index
{

    [FGTProgressHUD showHUDAddedTo:self animated:YES];
    [self.imageview sd_setImageWithURL:array[index] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    
        [FGTProgressHUD hideHUDForView:self animated:YES];

    }];

}



- (CGPoint)centerOfScrollViewContent:(UIScrollView *)scrollView
{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    CGPoint actualCenter = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                       scrollView.contentSize.height * 0.5 + offsetY);
    return actualCenter;
}

#pragma mark UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageview;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    self.imageview.center = [self centerOfScrollViewContent:scrollView];
}
@end
