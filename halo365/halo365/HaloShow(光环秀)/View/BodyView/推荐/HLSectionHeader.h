//
//  HLSectionHeader.h
//  halo365
//
//  Created by GG on 16/5/27.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLSectionHeader : UIView
- (id)initWithFrame:(CGRect)frame;

@end
