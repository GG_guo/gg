//
//  HLStatusToolbar.m
//  halo365
//
//  Created by 李鹏辉 on 16/5/17.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLStatusToolbar.h"
#import "HLShowModel.h"

@interface HLStatusToolbar()

@property (nonatomic, strong) NSMutableArray *btnArray;
//@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *dividers;
@property (nonatomic, weak) UIButton *attitudeBtn;//赞
@property (nonatomic, weak) UIButton *commentBtn;//评论
@property (nonatomic, weak) UIButton *collectionBtn;//收藏
@property (nonatomic, weak) UIButton *shareBtn;//分享
@property (nonatomic, weak) UIButton *reportBtn;//举报

@end

@implementation HLStatusToolbar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 1.设置图片
        self.userInteractionEnabled = YES;
//        self.image = [UIImage resizedImageWithName:@"timeline_card_bottom_background"];
//        self.highlightedImage = [UIImage resizedImageWithName:@"timeline_card_bottom_background_highlighted"];
        
        // 2.添加按钮
        self.attitudeBtn = [self setupBtnWithTitle:@"0" image:@"attitude" bgImage:nil tag:100];
        self.commentBtn = [self setupBtnWithTitle:@"0" image:@"comment" bgImage:nil tag:101];
        self.shareBtn = [self setupBtnWithTitle:nil image:@"share" bgImage:nil tag:102];
        self.collectionBtn = [self setupBtnWithTitle:nil image:@"collection" bgImage:nil tag:103];
        self.reportBtn = [self setupBtnWithTitle:nil image:@"report" bgImage:nil tag:104];
        
        
        // 3.添加分割线
        [self setupDivider];
        
    }
    
    return self;
}

//初始化分割线
- (void)setupDivider
{
    UIImageView *divider = [[UIImageView alloc] init];
    divider.image = [UIImage imageNamed:@"timeline_card_bottom_line_os7@2x"];
    [self addSubview:divider];
    [self.dividers addObject:divider];
    UIImageView *divider1 = [[UIImageView alloc] init];
    divider1.image = [UIImage imageNamed:@"timeline_card_bottom_line_os7@2x"];
    [self addSubview:divider1];
    [self.dividers addObject:divider1];
}

- (UIButton *)setupBtnWithTitle:(NSString *)title image:(NSString *)image bgImage:(NSString *)bgImage tag:(NSInteger)tag
{
    UIButton *btn = [[UIButton alloc] init];
    btn.tag=tag;
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btn.adjustsImageWhenHighlighted = NO;
    [btn setBackgroundImage:[UIImage imageNamed:bgImage] forState:UIControlStateHighlighted];
    [btn  addTarget:self action:@selector(clicedBtns:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    // 添加按钮到数组
    [self.btnArray addObject:btn];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = title;
    titleLabel.font = [UIFont systemFontOfSize:13];
    titleLabel.textColor = [UIColor grayColor];
    [self addSubview:titleLabel];
    
//    [self.titleArray addObject:titleLabel];
    
    return btn;
}

-(void)clicedBtns :(UIButton *)btn
{
    
//    [self.delegate ClikedZLStatusToolbarBtn:btn];
    if ([self.delegate respondsToSelector:@selector(ClikedZLStatusToolbarBtn:)]) {
        [self.delegate ClikedZLStatusToolbarBtn:btn];
        
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 1.设置按钮的frame
    NSInteger dividerCount = self.dividers.count; // 分割线的个数
    CGFloat dividerW = 2; // 分割线的宽度
    NSInteger btnCount = self.btnArray.count;
//    CGFloat btnW = (self.frame.size.width - dividerCount * dividerW) / btnCount;
    CGFloat btnW = 60;
    CGFloat btnH = 20;
    CGFloat btnY = 8;
    for (int i = 0; i<btnCount; i++) {
        UIButton *btn = self.btnArray[i];
//        UILabel *title = self.titleArray[i];
        if (i <3) {
            // 设置frame
            CGFloat btnX = i * (btnW + dividerW );

            btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
            
//            CGFloat titleX = CGRectGetMaxX(btn.frame) +10;

//            title.frame = CGRectMake(titleX, btnY, 40, btnW);

        }
        if (i >=3) {
            CGFloat btnX = i * (btnW + dividerW ) +30;
            btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        }
        
    }
    
    
    
    // 2.设置分割线的frame
    CGFloat dividerH = btnH;
    CGFloat dividerY = 0;
    for (int j = 0; j<dividerCount; j++) {
        UIImageView *divider = self.dividers[j];
        
        // 设置frame
        UIButton *btn = self.btnArray[j];
        CGFloat dividerX = CGRectGetMaxX(btn.frame);
        divider.frame = CGRectMake(dividerX, dividerY, dividerW, dividerH);
    }
}

- (void)setModel:(HLShowModel *)model
{
    _model = model;
    [self setupBtn:self.attitudeBtn originalTitle:@"0" count:model.like_num];
    [self setupBtn:self.commentBtn originalTitle:@"0" count:model.comment_num];

//    NSLog(@"like_count == %@",model.like_num);
}

- (void)setupBtn:(UIButton *)btn originalTitle:(NSString *)originalTitle count:count
{
//    NSLog(@"count == %@",count);
    if (count) { // 个数不为0
        NSString *title = nil;
        if ((int)count < 10000) { // 小于1W
            title = [NSString stringWithFormat:@"%@", count];
        } else { // >= 1W
            // 42342 / 1000 * 0.1 = 42 * 0.1 = 4.2
            // 10742 / 1000 * 0.1 = 10 * 0.1 = 1.0
            // double countDouble = count / 1000 * 0.1;
            
            // 42342 / 10000.0 = 4.2342
            // 10742 / 10000.0 = 1.0742
            double countDouble = (int)count / 10000.0;
            title = [NSString stringWithFormat:@"%.1f万", countDouble];
            
            // title == 4.2万 4.0万 1.0万 1.1万
            title = [title stringByReplacingOccurrencesOfString:@".0" withString:@""];
        }
        [btn setTitle:title forState:UIControlStateNormal];
    } else {
        [btn setTitle:originalTitle forState:UIControlStateNormal];
    }

}

- (NSMutableArray *)btnArray
{
    if (_btnArray == nil) {
        _btnArray = [NSMutableArray array];
    }
    return _btnArray;
}

- (NSMutableArray *)dividers
{
    if (_dividers == nil) {
        _dividers = [NSMutableArray array];
    }
    return _dividers;
}

//- (NSMutableArray *)titleArray
//{
//    if (_titleArray == nil) {
//        _titleArray = [NSMutableArray array];
//    }
//    
//    return _titleArray;
//}
@end
