//
//  HLSectionHeader.m
//  halo365
//
//  Created by GG on 16/5/27.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLSectionHeader.h"

@implementation HLSectionHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self addContentView:frame];
    }
    return self;
}
- (void)addContentView:(CGRect )frame
{
    UIView *view = [[UIView alloc]init];
    view.frame = frame;

    [self addSubview:view];

    UILabel *lable = [[UILabel alloc]init];
    lable.frame = CGRectMake(5, 5, frame.size.width-10, frame.size.height-10);
    lable.text = @"精彩评论";
    lable.font = [UIFont systemFontOfSize:13];
    lable.backgroundColor = [UIColor whiteColor];
    [view addSubview:lable];
    
}
@end
