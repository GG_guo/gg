//
//  HLStatusToolbar.h
//  halo365
//
//  Created by 李鹏辉 on 16/5/17.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HLStatusToolbar,HLShowModel;

@protocol  HLStatusToolbarDelegate<NSObject>


-(void)ClikedZLStatusToolbarBtn :(UIButton *)btn;

@end

@interface HLStatusToolbar : UIImageView

@property (nonatomic, strong) HLShowModel *model;

@property (nonatomic, weak) id <HLStatusToolbarDelegate> delegate;

@end
