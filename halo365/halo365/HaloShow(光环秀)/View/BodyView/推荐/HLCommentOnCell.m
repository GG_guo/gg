//
//  HLCommentOnCell.m
//  halo365
//
//  Created by GG on 16/5/27.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCommentOnCell.h"

@implementation HLCommentOnCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
        [self addContectView];
        
    }
    return self;
}
- (void)addContectView
{
    UIView *view = [[UIView alloc]init];
//    view.layer.cornerRadius = 5;
    view.frame = CGRectMake(5, 0, SCREEN_WIDTH-10, 80);
    view.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:view];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(5, 5, 25, 25);
    imageView.layer.cornerRadius = 12.5;
    imageView.image = [UIImage imageNamed:@"图标.png"];
    [view addSubview:imageView];
    UILabel *lable = [[UILabel alloc]init];
    lable.frame = CGRectMake(CGRectGetMaxX(imageView.frame)+5
                             , 0, 100, 30);
    lable.text = @"GGGG哥";
//    lable.backgroundColor = [UIColor whiteColor];

    [view addSubview:lable];
    UILabel *datelable = [[UILabel alloc]init];
    datelable.frame = CGRectMake(CGRectGetMaxX(imageView.frame)+5, CGRectGetMaxY(lable.frame), 100, 20);
//    datelable.backgroundColor = [UIColor grayColor];

    datelable.font = [UIFont systemFontOfSize:13];
    datelable.text = @"1小时前";
    [view addSubview:datelable];
    UILabel *textLable = [[UILabel alloc]init];
    textLable.frame = CGRectMake(CGRectGetMaxX(imageView.frame)+5, CGRectGetMaxY(datelable.frame), view.frame.size.width-(CGRectGetMaxX(imageView.frame)+5)*2, 80 -(CGRectGetMaxY(datelable.frame)));
    textLable.text = @"GGGGGGGGGGGGGGGGG";
//    textLable.backgroundColor = [UIColor brownColor];

    [view addSubview:textLable];
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
