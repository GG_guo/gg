//
//  BroserView.h
//  halo365
//
//  Created by GG on 16/3/25.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BroserView : UIView<UIScrollViewDelegate>

@property (nonatomic, strong) void (^singleTapBlock)(UITapGestureRecognizer *recognizer);
@property (nonatomic, strong) void (^longPressBlock)(UILongPressGestureRecognizer *recognizer);
@property (nonatomic,strong) UIImageView *imageview;

- (void)setData:(NSMutableArray *)array index:(NSInteger )index;
@end
