//
//  HLComeOnCell.m
//  halo365
//
//  Created by GG on 16/5/27.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLComeOnCell.h"

@implementation HLComeOnCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        [self addLableAndImageView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)addLableAndImageView
{
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(5, 0, SCREEN_WIDTH-10, self.frame.size.height);
    view.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:view];
    UILabel *lable = [[UILabel alloc]init];
    lable.frame = CGRectMake(5, 0,70 , self.frame.size.height);
    lable.backgroundColor = [UIColor clearColor];
    lable.font = [UIFont systemFontOfSize:15];
    lable.text = [NSString stringWithFormat:@"%d加油",567];
    [view addSubview:lable];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(CGRectGetMaxX(lable.frame), 5, 34, 34);
    imageView.image = [UIImage imageNamed:@"图标.png"];
    imageView.layer.cornerRadius = 17;
    imageView.backgroundColor = [UIColor clearColor];

    [view addSubview:imageView];
}

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
