//
//  CustomCell.m
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLShowCustomCell.h"
#import "HLShowSetFrame.h"
#import "HLShowModel.h"
#import "HLShowCustomView.h"
#import "HLStatusToolbar.h"

// 昵称的字体
//#define GGNameFont [UIFont systemFontOfSize:13]
// 正文的字体
#define GGTextFont [UIFont systemFontOfSize:15]

@interface HLShowCustomCell()

//头像
@property (nonatomic, strong) UIImageView *iconView;

//昵称
@property (nonatomic, strong) UILabel *nameView;

//时间
@property (nonatomic, strong) UILabel *timeStr;

//正文
@property (nonatomic, strong) UILabel *textView;

//图的父视图
@property (strong, nonatomic)HLShowCustomView *VIEW;
@end

@implementation HLShowCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        [self setupOriginalSubviews];
        [self setupStatusToolBar];
        
    }
    
    return  self;
}

//添加原创halo内部的子控件
- (void)setupOriginalSubviews
{
    //头像
    UIImageView *iconView = [[UIImageView alloc]init];
    iconView.layer.cornerRadius = 5;
    iconView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchUp)];
    [iconView addGestureRecognizer:tap];
    [self.contentView addSubview:iconView];
    self.iconView = iconView;
    self.iconView.backgroundColor = [UIColor clearColor];
    //昵称
    UILabel *nameView = [[UILabel alloc]init];
    nameView.font = GGNameFont;
    [self.contentView addSubview:nameView];
    self.nameView = nameView;
    
    //时间
    UILabel *timeStr = [[UILabel alloc]init];
    timeStr.font = GGNameFont;
    [self.contentView addSubview:timeStr];
    self.timeStr = timeStr;
    self.timeStr.backgroundColor = [UIColor clearColor];
    
    //正文
    UILabel *content = [[UILabel alloc]init];
    content.numberOfLines = 0;
    content.font = GGTextFont;
    [self.contentView addSubview:content];
    self.textView = content;
    self.textView.backgroundColor = [UIColor clearColor];
    
    //图片
    
    self.VIEW = [[HLShowCustomView alloc]init];
    [self.contentView addSubview:self.VIEW];
}

- (void)touchUp{
    self.touchBlock(0);
}
//添加halo的工具条
- (void)setupStatusToolBar
{
    // 6.halo的工具条
    HLStatusToolbar *statusToolbar = [[HLStatusToolbar alloc] init];
    [self.contentView addSubview:statusToolbar];
    self.statusToolbar = statusToolbar;
}


//每个section的item个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([self.setFrame.model.piclist isKindOfClass:[NSArray class]]&&self.setFrame.model.piclist.count>0)
    {
        return self.setFrame.model.piclist.count;
    }
    return 0;
}

//拦截frame的设置
- (void)setFrame:(CGRect)frame
{
    frame.origin.y += 5;
    frame.origin.x = 5;
    frame.size.width -= 2 * 5;
    frame.size.height -=2 * 5;
    [super setFrame:frame];
}

- (void)setSetFrame:(HLShowSetFrame *)setFrame andVC:(UIViewController *)vc
{
    _setFrame = setFrame;
    //设置数据
    [self settingData];
    [self setttingFrame:vc];
    
}

//获取时间
- (NSString *)getTime:(NSString *)time
{
    NSString * timeStampString = time;
    NSTimeInterval _interval=[[timeStampString substringToIndex:10] doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss "];
    //NSDate转NSString
    NSString *currentDateString = [dateFormatter stringFromDate:date];
    
    return currentDateString;
}

- (void)settingData
{
    HLShowModel *model = self.setFrame.model;
    //头像
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:model.avatar]];
    
    //昵称
    self.nameView.text = model.nickname;
    
    //时间
    self.timeStr.text = [self getTime:model.dateline];
    self.timeStr.textAlignment = NSTextAlignmentRight;
    
    //正文
    self.textView.text = model.moment;
    
    //图片
    
    if([model.piclist isKindOfClass:[NSArray class]]&&model.piclist.count>0)
    {
        self.VIEW.hidden = NO;
        
    }else{
        self.VIEW.hidden = YES;
    }
    
    self.statusToolbar.model = self.setFrame.model;
}

- (void)setttingFrame:(UIViewController *)VC
{
    
    self.iconView.frame = self.setFrame.iconF;
    self.nameView.frame = self.setFrame.nicknameF;
    self.timeStr.frame = self.setFrame.datalineF;
    self.textView.frame = self.setFrame.contentF;
    
    if([self.setFrame.model.piclist isKindOfClass:[NSArray class]]&&self.setFrame.model.piclist.count>0)
    {
        
        //        NSInteger num = self.setFrame.model.piclist.count;
        [self.VIEW setFrames:CGRectMake(self.setFrame.pictureF.origin.x, self.setFrame.pictureF.origin.y, self.setFrame.pictureF.size.width, self.setFrame.pictureF.size.width) with:self.setFrame.model.piclist and:VC];
    }
    
    
    self.statusToolbar.frame = self.setFrame.statusToolbarF;
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView index:(NSIndexPath*)indexPath
{
    static NSString *ID = @"status";
    //    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)indexPath.section, (long)indexPath.row];
    HLShowCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //防止数据重复
    //    CustomCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[HLShowCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }else{
        
    }
    return cell;
}

@end
