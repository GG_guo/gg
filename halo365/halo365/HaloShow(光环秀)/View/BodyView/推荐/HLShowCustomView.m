//
//  CustomView.m
//  Halo365
//
//  Created by GG on 16/3/15.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLShowCustomView.h"
#import "ImagBroserVC.h"
//#import "customBtn.h"
@implementation HLShowCustomView

- (void)setFrames:(CGRect )frame with:(NSMutableArray *)array and:(UIViewController *)vc
{
    VC = vc;
    self.frame = frame;
    self.imageDatas = array;
    [self addImageView:array];
}
- (void)addImageView:(NSArray *)array
{
    NSInteger count = array.count;
    for(UIView *view in self.subviews)
    {
        if(view != nil)
        {
            [view removeFromSuperview];
        }
    }
    
    CGFloat appvieww=SCREEN_WIDTH-30;
    //    CGFloat appviewh=SCREEN_WIDTH-30;
    _scroView = [[UIScrollView alloc]init];
    _scroView.frame = CGRectMake(0, 0, SCREEN_WIDTH-30, SCREEN_WIDTH-30);
    _scroView.backgroundColor = [UIColor blueColor];
    _scroView.delegate = self;
    _scroView.scrollEnabled = YES;
    _scroView.pagingEnabled = YES;
    _scroView.panGestureRecognizer.delaysTouchesBegan = YES;
    
    _scroView.contentSize = CGSizeMake((SCREEN_WIDTH-30) *count, SCREEN_WIDTH-30);
    [self addSubview:_scroView];
    
    
    for (int i=0; i<count; i++) {
        
        UIButton *cover = [UIButton buttonWithType:UIButtonTypeCustom];
        cover.frame = CGRectMake(appvieww *i, 0, SCREEN_WIDTH-30, SCREEN_WIDTH-30);
        cover.tag = i;
        cover.backgroundColor = [UIColor grayColor];
        [cover addTarget:self action:@selector(addScroView:) forControlEvents:UIControlEventTouchUpInside];
        [_scroView addSubview:cover];
        
        UIImageView * gag = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-30, SCREEN_WIDTH-30)];
        gag.contentMode = UIViewContentModeScaleAspectFill;
        gag.clipsToBounds = YES;
        NSString *smallURL = [array[i] stringByAppendingString:@".block.jpg"];
        [gag sd_setImageWithURL:[NSURL URLWithString:smallURL]];
        [cover addSubview:gag];
        
        
        
    }
    
}
//- (void)tap:(UIGestureRecognizer *)gesture
//{
//
//    NSNumber *index = [NSNumber numberWithInteger:gesture.view.tag];
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    NSLog(@"%@",gesture.view.superview.superview.superview.superview);
//
//    num = gesture.view.superview.superview.superview.superview.tag;
//    NSNumber *nums = [NSNumber numberWithInteger:num];
//    [dic setValue:index forKey:@"indexs"];
//    [dic setValue:nums forKey:@"nums"];
//
//    ImagBroserVC *vc = [[ImagBroserVC alloc]init];
//    vc.imageParten = self;
//    vc.imageCount = self.imageDatas.count;
//    vc.currentImageIndex = (int)gesture.view.tag;
//    vc.array = self.imageDatas;
//    [VC presentViewController:vc animated:NO completion:nil];
//}
- (void)addScroView:(UIButton *)btn
{
    NSNumber *index = [NSNumber numberWithInteger:btn.tag];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    num = btn.superview.superview.superview.tag;
    NSNumber *nums = [NSNumber numberWithInteger:num];
    [dic setValue:index forKey:@"indexs"];
    [dic setValue:nums forKey:@"nums"];
    
    ImagBroserVC *vc = [[ImagBroserVC alloc]init];
    vc.imageParten = btn.superview;
    vc.imageCount = self.imageDatas.count;
    vc.currentImageIndex = (int)btn.tag;
    vc.array = self.imageDatas;
    [VC presentViewController:vc animated:NO completion:nil];
    
    
}



@end
