//
//  HLHaloTool.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLHaloTool.h"
#import "HLLoginViewController.h"
#import "HLNewfeatureViewController.h"

@implementation HLHaloTool

+(void)chooseRootController
{
    NSString *key = @"CFBundleVersion";
    
    // 取出沙盒中存储的上次使用软件的版本号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults stringForKey:key];
    
    // 获得当前软件的版本号
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    if ([currentVersion isEqualToString:lastVersion]) {
        // 显示状态栏
        [UIApplication sharedApplication].statusBarHidden = NO;
        
         [UIApplication sharedApplication].keyWindow.rootViewController = [[HLLoginViewController alloc] init];
        
    } else { // 新版本
       [UIApplication sharedApplication].keyWindow.rootViewController = [[HLNewfeatureViewController alloc] init];
        // 存储新版本
        [defaults setObject:currentVersion forKey:key];
        [defaults synchronize];
    }
}

@end
