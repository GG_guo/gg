//
//  HLHaloTool.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLHaloTool : NSObject

//选择根控制器(新特性界面 或者 halo主界面)
+ (void)chooseRootController;
@end
