//
//  HLBaseNavigationController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLBaseNavigationController.h"

@interface HLBaseNavigationController ()

@end

@implementation HLBaseNavigationController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    //改变 状态栏的颜色为白色
    return UIStatusBarStyleLightContent;
    //改变状态栏的颜色为黑色
//    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationBar.barTintColor=ZLColor(22, 40, 77);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
