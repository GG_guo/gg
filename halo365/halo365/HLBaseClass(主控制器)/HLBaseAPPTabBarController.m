//
//  HLAPPFrameTabBarController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLBaseAPPTabBarController.h"
#import "HLBaseNavigationController.h"

#import "HLBasePlanViewController.h"
#import "HLBaseShowViewController.h"
#import "HLBaseMeViewController.h"

#define kClassKey   @"rootVCClassString"
#define kTitleKey   @"title"
#define kImgKey     @"imageName"
#define kSelImgKey  @"selectedImageName"

#define Global_tintColor [UIColor colorWithRed:0 green:(190 / 255.0) blue:(12 / 255.0) alpha:1]

@interface HLBaseAPPTabBarController ()

@end

@implementation HLBaseAPPTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *childItemsArray = @[
                                 @{kClassKey  : @"HLBasePlanViewController",
                                   kTitleKey  : @"光环计划",
                                   kImgKey    : @"tabbar_plan",
                                   kSelImgKey : @"tabbar_planHL"},
                                 
                                 
                                 @{kClassKey  : @"HLBaseShowViewController",
                                   kTitleKey  : @"光环秀",
                                   kImgKey    : @"tabbar_show",
                                   kSelImgKey : @"tabbar_showHL"},
  
                                 @{kClassKey  : @"HLBaseMeViewController",
                                   kTitleKey  : @"我",
                                   kImgKey    : @"tabbar_me",
                                   kSelImgKey : @"tabbar_meHL"} ];

    [childItemsArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        UIViewController *vc = [NSClassFromString(dict[kClassKey]) new];
//        vc.title = dict[kTitleKey];
        HLBaseNavigationController *nav = [[HLBaseNavigationController alloc] initWithRootViewController:vc];
        
        UITabBarItem *item = nav.tabBarItem;
        item.title = dict[kTitleKey];
        item.image = [UIImage imageNamed:dict[kImgKey]];
        item.selectedImage = [[UIImage imageNamed:dict[kSelImgKey]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [item setTitleTextAttributes:@{NSForegroundColorAttributeName : Global_tintColor} forState:UIControlStateSelected];
        [self addChildViewController:nav];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
