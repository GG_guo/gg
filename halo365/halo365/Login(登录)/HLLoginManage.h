//
//  HLLoginManage.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/7/4.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HLAppDelegate.h"

@interface HLLoginManage : NSObject

@property (nonatomic , assign)NSInteger  loginState;

+(instancetype)sharedManager;

//判断登录状态，已登录返回yes，未登录则弹出登录页面
//已登录返回yes，未登录返回no
-(BOOL)judgeLoginState;

//自动登录，登录失败则弹出登录页面

-(void)autoLogin;

@end
