//
//  HLLoginViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/3/28.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLLoginViewController.h"
#import "HLRegisteredViewController.h"
#import "HLForgetViewController.h"
#import "HLBaseAPPTabBarController.h"
#import "HLLoginManage.h"
//#import "WeiboSDK.h"
//#import <TencentOpenAPI/TencentOAuth.h>
//#import "sdkCall.h"
//#import "WXApi.h"
//#import "HLAppDelegate.h"

@interface HLLoginViewController ()<UITabBarControllerDelegate,UITextFieldDelegate>

{
    UIImageView *_backgroundImage;
    UITextField *_accountField;
    UITextField *_passwordField;
    
}
//@property (nonatomic, retain)TencentOAuth *oauth;

//@property(nonatomic,strong)HLAPPFrameTabBarController *tabBC;
@end

@implementation HLLoginViewController

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    //获取账号
    NSDictionary *accountDic = [[NSUserDefaults standardUserDefaults]objectForKey:AUTOLOGIN];
    NSString *account = [accountDic objectForKey:ACCOUNT];
    NSString *password = [accountDic objectForKey:PASSWORD];
    if (account != nil && account.length >0) {
        _accountField.text = account;
    }
    if (password != nil && password.length >0) {
        _passwordField.text = password;
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self infrastructure];
    [self setButton];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushVC) name:@"LOGINSUC" object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushWechat) name:@"wechat" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushWeibo) name:@"weibo" object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessed) name:kLoginSuccessed object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginFailed) name:kLoginFailed object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginCancelled) name:kLoginCancelled object:nil];

}

- (void)pushWechat
{
    
    NSString *nickname =  [[NSUserDefaults standardUserDefaults] objectForKey:@"nickname"];
    NSString *openid =  [[NSUserDefaults standardUserDefaults] objectForKey:@"openid"];
    NSString *sex =  [[NSUserDefaults standardUserDefaults] objectForKey:@"sex"];
    NSString *imageStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"headimgurl"];
    NSLog(@"imageStr==%@",imageStr);
    
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"type"] = @"wechat";
    parmars[@"identifier"]= @"";
    parmars[@"openid"] = openid;
    parmars[@"nickname"] = nickname;
    parmars[@"sex"] = sex;
    parmars[@"headimgurl"] = imageStr;
    
    [HLAFNworkingHeader requestURL:Login_ExternalAPI httpMethod:@"POST" token:nil params:parmars  file:nil success:^(id data) {
        //            NSLog(@"请求成功---%@",data);
        HideLoading;
        self.view.userInteractionEnabled = YES;
        NSString *token = [[data[@"msg"] objectForKey:@"info"] objectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
        
        [self pushVC];
        
        //登录成功
        [HLLoginManage sharedManager].loginState = 1;
        //保存账号，不保存密码
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys: _accountField.text,ACCOUNT,_passwordField.text,PASSWORD ,nil];
        [[NSUserDefaults standardUserDefaults] setObject:dic forKey:AUTOLOGIN];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    } fail:^(NSError *error) {
    }];
    
}

- (void)pushWeibo
{
//    HLAPPFrameTabBarController *tabBC = [[HLAPPFrameTabBarController alloc] init];
//    tabBC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:tabBC animated:YES completion:nil];
    
    NSString *openid =  [[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    NSString *screen_name =  [[NSUserDefaults standardUserDefaults] objectForKey:@"screen_name"];
    NSString *gender =  [[NSUserDefaults standardUserDefaults] objectForKey:@"gender"];
    NSString *avatar_large = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar_large"];
    NSLog(@"imageStr==%@",avatar_large);
    
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"type"] = @"weibo";
    parmars[@"identifier"]= @"";
    parmars[@"id"] = openid;
    parmars[@"screen_name"] = screen_name;
    parmars[@"gender"] = gender;
    parmars[@"avatar_large"] = avatar_large;
    
    [HLAFNworkingHeader requestURL:Login_ExternalAPI httpMethod:@"POST" token:nil params:parmars  file:nil success:^(id data) {
        //            NSLog(@"请求成功---%@",data);
        HideLoading;
        self.view.userInteractionEnabled = YES;
        NSString *token = [[data[@"msg"] objectForKey:@"info"] objectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
        
        [self pushVC];
        
        //登录成功
        [HLLoginManage sharedManager].loginState = 1;
        //保存账号，不保存密码
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys: _accountField.text,ACCOUNT,_passwordField.text,PASSWORD ,nil];
        [[NSUserDefaults standardUserDefaults] setObject:dic forKey:AUTOLOGIN];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    } fail:^(NSError *error) {
    }];

}

- (void)pushVC
{
    HLBaseAPPTabBarController *tabBC = [[HLBaseAPPTabBarController alloc] init];
    tabBC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:tabBC animated:YES completion:nil];
}

//基础设置
- (void)infrastructure
{
    _backgroundImage = [[UIImageView alloc] init];
    _backgroundImage.image = [UIImage imageNamed:@"底图1"];
    [self.view addSubview:_backgroundImage];
    [_backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
//        make.width.offset(SCREEN_WIDTH);
//        make.height.offset(SCREEN_HEIGHT);
    }];
    
    UIImageView *logoImage = [[UIImageView alloc] init];
    logoImage.image = [UIImage imageNamed:@"logo"];
    [self.view addSubview:logoImage];
    [logoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(119*Proportion_HEIGHT);
        make.left.offset((SCREEN_WIDTH- 181*Proportion_WIDTH)/2);
        make.width.offset (181*Proportion_WIDTH);
//        make.right.offset((363 - SCREEN_WIDTH)/2);
        make.height.offset(48*Proportion_HEIGHT);
    }];
    
    _accountField= [[UITextField alloc]init];
    _accountField.delegate = self;
    _accountField.placeholder = @"手机号";
    [_accountField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_accountField setValue:[UIColor colorWithWhite:1 alpha:0.3] forKeyPath:@"_placeholderLabel.textColor"];
    [_accountField setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    _accountField.font = [UIFont systemFontOfSize:16];
    _accountField.textColor = [UIColor whiteColor];
    _accountField.borderStyle = UITextBorderStyleNone;
    _accountField.background = [UIImage imageNamed:@"输入底图2"];
    _accountField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _accountField.keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:_accountField];
    
    [_accountField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(logoImage.mas_bottom).with.offset(47*Proportion_HEIGHT);
        make.left.offset(30*Proportion_WIDTH);
        make.right.offset(-30*Proportion_WIDTH);
        make.height.offset(49*Proportion_HEIGHT);
    }];
    
    _passwordField = [[UITextField alloc] init];
    _passwordField.delegate = self;
    _passwordField.placeholder = @"请输入6~20位字母或数字";
    [_passwordField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_passwordField setValue:[UIColor colorWithWhite:1 alpha:0.3] forKeyPath:@"_placeholderLabel.textColor"];
    [_passwordField setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    _passwordField.font = [UIFont systemFontOfSize:16];
    _passwordField.textColor = [UIColor whiteColor];
    _passwordField.borderStyle = UITextBorderStyleNone;
    _passwordField.background = [UIImage imageNamed:@"输入底图2"];
    _passwordField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _passwordField.secureTextEntry = YES;
    [_passwordField addTarget:self action:@selector(cancelKeyboard) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:_passwordField];
    
    [_passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_accountField.mas_bottom).with.offset(10);
        make.left.equalTo(_accountField.mas_left);
        make.right.equalTo(_accountField.mas_right);
        make.height.equalTo(_accountField.mas_height);
    }];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame=CGRectMake(0, -50, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

//键盘弹下
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)cancelKeyboard
{
    return;
}

//设置按钮
- (void)setButton
{
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginButton setTitle:@"登录" forState:UIControlStateNormal];
//    loginButton.contentEdgeInsets = UIEdgeInsetsMake(-15, 0, 0, 0);
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginButton setBackgroundImage:[UIImage imageNamed:@"登录按钮"] forState:UIControlStateNormal];
    [self.view addSubview:loginButton];
    [loginButton addTarget:self action:@selector(clickLoginButton) forControlEvents:UIControlEventTouchUpInside];
    [loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_passwordField.mas_bottom).with.offset(35);
        make.left.equalTo(_passwordField.mas_left);
        make.right.equalTo(_passwordField.mas_right);
        make.height.offset(50*Proportion_HEIGHT);
    }];
    

    UIButton *registeredButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [registeredButton setTitle:@"立即注册" forState:UIControlStateNormal];
    [registeredButton setTitleColor:ZLColor(165, 165, 165) forState:UIControlStateNormal];
    [registeredButton setTitleColor:[UIColor colorWithWhite:1 alpha:0.6] forState:UIControlStateHighlighted];
//    [registeredButton setBackgroundImage:[UIImage imageNamed:@"4.pic_hd"] forState:UIControlStateNormal];
    registeredButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [registeredButton addTarget:self action:@selector(clickRegistered) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registeredButton];
    [registeredButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(loginButton.mas_bottom).with.offset(20);
        make.right.equalTo(loginButton.mas_right).with.offset(5);
        make.width.offset(84);
        make.height.offset(20);

    }];
    
    UIButton *forgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetButton setTitle:@"忘记密码?" forState:UIControlStateNormal];
    [forgetButton setTitleColor:ZLColor(165, 165, 165) forState:UIControlStateNormal];
    [forgetButton setTitleColor:[UIColor colorWithWhite:1 alpha:0.6] forState:UIControlStateHighlighted];
    forgetButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:forgetButton];
    [forgetButton addTarget:self action:@selector(clickForget) forControlEvents:UIControlEventTouchUpInside];
    [forgetButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(loginButton.mas_bottom).with.offset(20);
        make.left.equalTo(loginButton.mas_left).with.offset(5);
        make.width.offset(84);
        make.height.offset(20);

        
    }];
    
    UIImageView *dividerImage = [[UIImageView alloc] init];
    dividerImage.image = [UIImage imageNamed:@"其他方式登录"];
    [self.view addSubview:dividerImage];
    NSLog(@"mmm===%f",Proportion_HEIGHT);
    [dividerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(loginButton.mas_bottom).with.offset(164*Proportion_HEIGHT);
        make.left.equalTo(loginButton.mas_left);
        make.width.offset(315*Proportion_WIDTH);
        make.height.offset(12);
        
    }];
    
    NSArray *imageArray = @[@"微博",@"qq",@"微信"];
    for (int i = 0;  i <imageArray.count; i++) {
        
        UIButton *thirdButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [thirdButton setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [thirdButton addTarget:self action:@selector(clickThirdButton:) forControlEvents:UIControlEventTouchUpInside];
        thirdButton.tag = i+1;
        [self.view addSubview:thirdButton];
        [thirdButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(dividerImage.mas_bottom).with.offset(29*Proportion_HEIGHT);
            make.left.offset((SCREEN_WIDTH-180)/4*(i+1) +60*i);
            make.width.offset(32*Proportion_WIDTH);
            make.height.offset(32*Proportion_HEIGHT);
        }];

    }
    
}

- (void)clickThirdButton:(UIButton *)sender
{
//    if (sender.tag == 1) {
//        NSLog(@"点击微博");
//        WBAuthorizeRequest *request = [WBAuthorizeRequest request];
//        //    http://sns.whalecloud.com/sina2/callback
//        request.redirectURI = @"http://www.sina.com";
//        //    request.redirectURI = @"http://sns.whalecloud.com/sina2/callback";
//        
//        request.scope = @"all";
//        request.userInfo = @{@"SSO_From": @"HLLoginViewController",
//                             @"Other_Info_1": [NSNumber numberWithInt:123],
//                             @"Other_Info_2": @[@"obj1", @"obj2"],
//                             @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
//        [WeiboSDK sendRequest:request];
//
//        
//        
//    }else if(sender.tag == 2){
//        NSLog(@"点击QQ");
//        //跳转到微信appstore
////        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://weixin.qq.com/r/o3W_sRvEMSVOhwrSnyCH"]];
//        //直接跳转到微信界面
//        
//        NSArray* permissions = [NSArray arrayWithObjects:
//                                kOPEN_PERMISSION_GET_USER_INFO,
//                                kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
//                                kOPEN_PERMISSION_ADD_ALBUM,
//                                kOPEN_PERMISSION_ADD_ONE_BLOG,
//                                kOPEN_PERMISSION_ADD_SHARE,
//                                kOPEN_PERMISSION_ADD_TOPIC,
//                                kOPEN_PERMISSION_CHECK_PAGE_FANS,
//                                kOPEN_PERMISSION_GET_INFO,
//                                kOPEN_PERMISSION_GET_OTHER_INFO,
//                                kOPEN_PERMISSION_LIST_ALBUM,
//                                kOPEN_PERMISSION_UPLOAD_PIC,
//                                kOPEN_PERMISSION_GET_VIP_INFO,
//                                kOPEN_PERMISSION_GET_VIP_RICH_INFO,nil];
//        
//        [[[sdkCall getinstance] oauth] authorize:permissions inSafari:NO];
//
//        
//    }else
//    {
//        NSLog(@"点击微信");
//        //打开微信
////        NSString *str =@"weixin://qr/JnXv90fE6hqVrQOU9yA0";
////        
////        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
//        
//        if ([WXApi isWXAppInstalled]) {
//            SendAuthReq *req = [[SendAuthReq alloc]init];
//            req.scope = @"snsapi_userinfo";
//            req.openID = @"wx83c9eb746a9c6ae6";
//            req.state = @"1245";
//            [WXApi sendReq:req];  
//        }
//    }

}


- (void)loginSuccessed
{
    NSLog(@"成功登录");
    [self pushVC];
}
- (void)loginFailed
{
    NSLog(@"网路有问题");
}
- (void)loginCancelled
{
    NSLog(@"登录失败");
}
//点击登录
- (void)clickLoginButton
{
    
    if (_accountField.text.length !=11) {

        ShowAlert(@"请输入正确手机号");
        return;
    }
    
    if (_passwordField.text.length < 6||_passwordField.text.length>20) {
        ShowAlert(@"密码格式不正确");
        return;
    }
    
    [self loginButton];

}

- (void)loginButton
{
    ShowLoading(@"请稍后");
    [self.view endEditing:YES];
    self.view.userInteractionEnabled = NO;
    
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"type"] = @"mobile";
    parmars[@"identifier"]= _accountField.text;
    parmars[@"credential"] = [MTAuthCode authEncode:_passwordField.text authKey:@"ios32D44@&&ddks" expiryPeriod:0];
    
    [HLAFNworkingHeader requestURL:Login_ExternalAPI httpMethod:@"POST" token:nil params:parmars  file:nil success:^(id data) {
        //        NSLog(@"请求成功---%@",data);
        
        HideLoading;
        self.view.userInteractionEnabled = YES;
        NSString *token = [[data[@"msg"] objectForKey:@"info"] objectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
        
        [self pushVC];
        
        //登录成功
        [HLLoginManage sharedManager].loginState = 1;
        //保存账号，不保存密码
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys: _accountField.text,ACCOUNT,_passwordField.text,PASSWORD ,nil];
        [[NSUserDefaults standardUserDefaults] setObject:dic forKey:AUTOLOGIN];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    } fail:^(NSError *error) {
        HideLoading;
        self.view.userInteractionEnabled = YES;
        
        ShowAlert(@"输入账号或密码有误");
        
        NSLog(@"请求失败---%@",error);
    }];
    
}

//注册
- (void)clickRegistered
{
    HLRegisteredViewController *registeredVC = [[HLRegisteredViewController alloc] init];
    registeredVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:registeredVC animated:YES completion:nil];
}

//忘记密码
- (void)clickForget
{
    HLForgetViewController *forgetVC = [[HLForgetViewController alloc] init];
    forgetVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:forgetVC animated:YES completion:nil];
    
}

@end
