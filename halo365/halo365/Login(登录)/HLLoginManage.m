//
//  HLLoginManage.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/7/4.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLLoginManage.h"
#import "HLLoginViewController.h"

@implementation HLLoginManage

+(instancetype)sharedManager{
    
    static HLLoginManage *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        _sharedManager = [[HLLoginManage alloc]init];
    });
    return _sharedManager;
}

//判断登录状态，已登录返回yes，未登录则弹出登录页面
//已登录返回yes，未登录返回no
-(BOOL)judgeLoginState{
    
    BOOL flag = NO;
    if (self.loginState ==1) {
        flag = YES;
    }
    else{
        [self showLoginView];
    }
    return flag;
}

//自动登录，登录失败则弹出登录页面

-(void)autoLogin{
    
    if (self.loginState !=1) {
        [self login];
    }
}

//弹出登录界面
-(void)showLoginView{
    
    
    UIViewController *currentVC = [HLSharedAppDelegate getNowViewController];
    HLLoginViewController *login = [[HLLoginViewController alloc]init];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:login];
    [currentVC presentViewController:navController animated:NO completion:nil];
    
}
//登录
-(void)login{
    //获取账号信息
    NSDictionary *accountDic = [[NSUserDefaults standardUserDefaults]objectForKey:AUTOLOGIN];
    NSString *account = [accountDic objectForKey:ACCOUNT];
    NSString *password = [accountDic objectForKey:PASSWORD];
    if (account == nil || account.length == 0) {
        [self showLoginView];
        return;
    }
    if (password == nil || password.length == 0) {
        [self showLoginView];
        return;
    }
    
    //此处为网络请求部分
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"type"] = @"mobile";
    parmars[@"identifier"]= account;
    parmars[@"credential"] = [MTAuthCode authEncode:password authKey:@"ios32D44@&&ddks" expiryPeriod:0];
    
    [HLAFNworkingHeader requestURL:Login_ExternalAPI httpMethod:@"POST" token:nil params:parmars  file:nil success:^(id data) {
//        NSLog(@"请求成功---%@",data);
        
        //        HideLoading;
        //        self.view.userInteractionEnabled = YES;
        NSString *token = [[data[@"msg"] objectForKey:@"info"] objectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
        
        //        HLAPPFrameTabBarController *tabBC = [[HLAPPFrameTabBarController alloc] init];
        //        //        tabBC.token = token;
        //        tabBC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        //        [self presentViewController:tabBC animated:YES completion:nil];
        
        //登录成功
        [HLLoginManage sharedManager].loginState = 1;
        
        //保存账号，不保存密码
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:account,ACCOUNT,password,PASSWORD ,nil];
        [[NSUserDefaults standardUserDefaults] setObject:dic forKey:AUTOLOGIN];
        [[NSUserDefaults standardUserDefaults]synchronize];
    } fail:^(NSError *error) {
        //        HideLoading;
        [self showLoginView];
        
    }];
    
}



@end
