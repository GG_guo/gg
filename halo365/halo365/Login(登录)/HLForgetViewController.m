//
//  HLForgetViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/3/31.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLForgetViewController.h"
#import "HLLoginViewController.h"
//#import "UIButton+CountDown.h"

@interface HLForgetViewController ()<UITextFieldDelegate>

{
    UIView *topView;
    UITextField *accountField;
    UITextField *passwordField;
    UITextField *validationField;
    UIButton *validationButton ;
    
    NSInteger _count;
    
}

@end

@implementation HLForgetViewController


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *backImageView = [[UIImageView alloc] init];
    backImageView.image = [UIImage imageNamed:@"底图1"];
    [self.view addSubview:backImageView];
    [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        make.width.offset(SCREEN_WIDTH);
        make.height.offset(SCREEN_HEIGHT);
        
    }];
    
    [self navigationView];
    [self contentView];
}

- (void)navigationView
{
    NSLog(@"55== %f",64*Proportion_HEIGHT);
    topView = [[UIView alloc] init];
    //    topView.backgroundColor = [UIColor redColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
    }];
    
    UIImageView *topImageView = [[UIImageView alloc] init];
    topImageView.image = [UIImage imageNamed:@"注册-菜单栏"];
    
    [topView addSubview:topImageView];
    [topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.bottom.offset(0);
        
    }];
    
    UILabel *themeLabel = [[UILabel alloc] init];
    themeLabel.text = @"忘记密码";
    themeLabel.textColor = [UIColor whiteColor];
    themeLabel.font = [UIFont systemFontOfSize:18];
    [topImageView addSubview:themeLabel];
    
    [themeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(34*Proportion_HEIGHT);
        make.centerX.equalTo(topImageView);
        //        make.bottom.offset(-14);
        make.width.offset(72);
        make.height.offset(18);
    }];
    //
    UIButton *returnButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [returnButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    //    [returnButton setBackgroundImage:[UIImage imageNamed:@"20x36返回按钮选中"] forState:UIControlStateHighlighted];
    [returnButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:returnButton];
    [returnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        //        make.bottom.offset(-13);
        make.width.offset(11);
        make.height.offset(18);
    }];
}

//返回按钮
- (void)clickReturn
{
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (void)contentView
{
    //账号输入框
    accountField = [[UITextField alloc] init];
    accountField.delegate = self;
    accountField.placeholder = @"请输入手机号";
    [accountField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [accountField setValue:[UIColor colorWithWhite:1 alpha:0.3] forKeyPath:@"_placeholderLabel.textColor"];
    [accountField setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    accountField.font = [UIFont systemFontOfSize:16];
    accountField.textColor = [UIColor whiteColor];
    accountField.borderStyle = UITextBorderStyleNone;
    accountField.background = [UIImage imageNamed:@"输入底图2"];
    accountField.clearButtonMode = UITextFieldViewModeWhileEditing;
    accountField.keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:accountField];
    
    [accountField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).with.offset(48*Proportion_HEIGHT);
        make.left.offset(30*Proportion_WIDTH);
        make.right.offset(-30*Proportion_WIDTH);
        make.height.offset(49*Proportion_HEIGHT);
    }];
    
    //验证码输入框
    validationField = [[UITextField alloc] init];
    validationField.delegate = self;
    validationField.placeholder = @"请输入验证码";
    validationField.keyboardType = UIKeyboardTypeNumberPad;
    [validationField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [validationField setValue:[UIColor colorWithWhite:1 alpha:0.3] forKeyPath:@"_placeholderLabel.textColor"];
    [validationField setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    validationField.font = [UIFont systemFontOfSize:16];
    validationField.textColor = [UIColor whiteColor];
    validationField.borderStyle = UITextBorderStyleNone;
    validationField.background = [UIImage imageNamed:@"输入底图2"];
    validationField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.view addSubview:validationField];
    
    [validationField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountField.mas_bottom).with.offset(10);
        make.left.equalTo(accountField.mas_left);
        make.right.equalTo(accountField.mas_right);
        make.height.equalTo(accountField.mas_height);
    }];
    
    //验证码按钮
    validationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [validationButton setBackgroundImage:[UIImage imageNamed:@"注册-验证码矩形"] forState:UIControlStateNormal];
    [validationButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [validationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [validationButton setTitleColor:[UIColor colorWithWhite:1 alpha:0.6] forState:UIControlStateHighlighted];
    validationButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [validationButton addTarget:self action:@selector(clickValidation:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:validationButton];
    [validationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.left.equalTo(validationField.mas_right).with.offset(-2);
        make.right.offset(-30);
        make.top.equalTo(validationField.mas_top);
        make.bottom.equalTo(validationField.mas_bottom).offset(-10);
        make.width.offset(97);
    }];
    
    //密码输入框
    passwordField = [[UITextField alloc] init];
    passwordField.delegate = self;
    passwordField.placeholder = @"请输入密码";
    [passwordField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [passwordField setValue:[UIColor colorWithWhite:1 alpha:0.3] forKeyPath:@"_placeholderLabel.textColor"];
    [passwordField setValue:[UIFont systemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    passwordField.font = [UIFont systemFontOfSize:16];
    passwordField.textColor = [UIColor whiteColor];
    passwordField.borderStyle = UITextBorderStyleNone;
    passwordField.background = [UIImage imageNamed:@"输入底图2"];
    passwordField.clearButtonMode = UITextFieldViewModeWhileEditing;
    passwordField.secureTextEntry = YES;
    [passwordField addTarget:self action:@selector(cancelKeyboard) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:passwordField];
    
    [passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(validationField.mas_bottom).with.offset(10);
        make.left.equalTo(accountField.mas_left);
        make.right.equalTo(accountField.mas_right);
        make.height.equalTo(accountField.mas_height);
    }];
    
    
    //完成
    UIButton *completeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [completeButton setTitle:@"注册" forState:UIControlStateNormal];
    //    completeButton.contentEdgeInsets = UIEdgeInsetsMake(-15, 0, 0, 0);
    [completeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [completeButton setBackgroundImage:[UIImage imageNamed:@"登录按钮"] forState:UIControlStateNormal];
    [completeButton addTarget:self action:@selector(clickComplete) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:completeButton];
    
    [completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passwordField.mas_bottom).with.offset(35);
        make.left.equalTo(passwordField.mas_left);
        make.right.equalTo(passwordField.mas_right);
        make.height.offset(50*Proportion_HEIGHT);
        
    }];
 
}


//验证码
- (void)clickValidation:(UIButton *)button
{
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"type"] = @"forgetpwd";
    parmars[@"mobile"]= accountField.text;
    
    [HLAFNworkingHeader requestURL:Verification_ExternalAPI httpMethod:@"POST" token:nil params:parmars  file:nil success:^(id data) {
        
        NSLog(@"%@",data);
        NSDictionary *dict = data[@"msg"];
        NSLog(@"bbb==%@",dict);
        if ([dict isEqual:@"-10000"]) {
            ShowAlert(@"传入数据不完整");
        }else if ([dict isEqual:@"-10001"]) {
            ShowAlert(@"数据库操作错");
        }else if ([dict isEqual:@"-11001"]) {
            ShowAlert(@"手机号码格式错误");
        }else if ([dict isEqual:@"-11002"]) {
            ShowAlert(@"类型不正确");
        }else if ([dict isEqual:@"-11003"]) {
            ShowAlert(@"手机号未注册");
        }else if ([dict isEqual:@"-11004"]) {
            ShowAlert(@"手机号没变化");
        }else if ([dict isEqual:@"-11005"]) {
            ShowAlert(@"手机号已注册");
        }else if ([dict isEqual:@"-11006"]) {
            ShowAlert(@"短信已发送 60秒内不重发");
        }else if ([dict isEqual:@"-11007"]) {
            ShowAlert(@"短信发送失败，可能超过今天上限");
        }else {
            validationButton.enabled = NO;
            _count = 60;
            [button setTitle:@"60秒" forState:UIControlStateDisabled];
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        }
    } fail:^(NSError *error) {
        HideLoading;
        self.view.userInteractionEnabled = YES;
        
        ShowAlert(@"输入账号有误");
        
        NSLog(@"请求失败---%@",error);
    }];
    //    }
}

-(void)timerFired:(NSTimer *)timer
{
    if (_count !=1) {
        _count -=1;
        [validationButton setTitle:[NSString stringWithFormat:@"%ld秒",_count] forState:UIControlStateDisabled];
    }
    else
    {
        [timer invalidate];
        validationButton.enabled = YES;
        [validationButton setTitle:@"重新获取验证码" forState:UIControlStateNormal];
    }
}



//完成
- (void)clickComplete
{
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"authcode"] = validationField.text;
    parmars[@"mobile"]= accountField.text;
    parmars[@"password"] = [MTAuthCode authEncode:passwordField.text authKey:@"ios32D44@&&ddks" expiryPeriod:0];
    
    [HLAFNworkingHeader requestURL:Forgetpwd_ExternalAPI httpMethod:@"POST" token:nil params:parmars  file:nil success:^(id data) {
        
        NSDictionary *dict = data[@"msg"];
        if ([dict isEqual:@"-10000"]) {
            ShowAlert(@"传入数据不完整");
        }if ([dict isEqual:@"-10001"]) {
            ShowAlert(@"数据库操作错");
        }if ([dict isEqual:@"-11001"]) {
            ShowAlert(@"手机号码格式错误");
        }if ([dict isEqual:@"-11003"]) {
            ShowAlert(@"手机号未注册");
        }if ([dict isEqual:@"-11008"]) {
            ShowAlert(@"手机验证码不正确");
        }if ([dict isEqual:@"-11009"]) {
            ShowAlert(@"密码格式不正确");
        }if ([dict isEqual:@"-11010"]) {
            ShowAlert(@"验证码超时");
        }if ([dict isEqual:@"-11011"]) {
            ShowAlert(@"验证码不正确");
        }if ([dict isEqual:@"-11012"]) {
            ShowAlert(@"手机号不存在");
        }
        
        NSLog(@"%@",data);
        //        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } fail:^(NSError *error) {
        HideLoading;
        self.view.userInteractionEnabled = YES;
        
        ShowAlert(@"输入验证码有误");
        
        NSLog(@"error=请求失败---%@",error);
    }];
}


//键盘弹下
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)cancelKeyboard
{
    return;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
