//
//  main.m
//  halo365
//
//  Created by 李鹏辉 on 16/7/21.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HLAppDelegate class]));
    }
}
