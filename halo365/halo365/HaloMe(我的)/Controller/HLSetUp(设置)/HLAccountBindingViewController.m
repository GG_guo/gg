//
//  HLAccountBindingViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLAccountBindingViewController.h"

@interface HLAccountBindingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *labelArray;
}
@end

@implementation HLAccountBindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    labelArray = @[@"微信",@"微博",@"QQ",@"手机"];
    [self navigationBar];
    [self contentView];

}

//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"账号绑定";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];
    
}

-(void)contentView
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64*Proportion_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64*Proportion_HEIGHT) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.scrollEnabled = NO;
    tableView.backgroundColor = bgVCcolor;
    [self.view addSubview:tableView];
    [self hideExcessLine:tableView];
}

//去掉多余的线
-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrderListCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = labelArray[indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        
        UISwitch *switchView = [UISwitch new];
//        switchView.backgroundColor = [UIColor lightGrayColor];
        //onTintColor设置开启颜色
        switchView.onTintColor = [UIColor greenColor];
        
        //tintColor设置正常关闭颜色；
        switchView.tintColor = [UIColor whiteColor];
       
        //thumbTintColor设置圆形按钮颜色；
        switchView.thumbTintColor = [UIColor whiteColor];
        
        //设置YES或NO，是否使用animated动画效果：
        [switchView setOn:NO animated:YES];
        //添加动作事件
        [switchView addTarget:self action:@selector(clickSwitch:) forControlEvents:UIControlEventValueChanged];
        
        [cell addSubview:switchView];
        [switchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(8);
            make.right.offset(-15);
            
        }];

    }
    
    return cell;
}

- (void)clickSwitch:(id)sender
{
    UISwitch *switchView = (UISwitch *)sender;
    //获取对象的isOn属性，默认是关闭状态
    if (switchView.isOn) {
        NSLog(@"开启状态");
    }else
    {
        NSLog(@"关闭状态");
    }
}

//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
