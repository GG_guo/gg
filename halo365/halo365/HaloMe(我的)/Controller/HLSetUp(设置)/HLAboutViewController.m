//
//  HLAboutViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLAboutViewController.h"

@interface HLAboutViewController ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>

{
    UITableView *_tableView;
}
@property (nonatomic, strong) UIWebView *contentWebView;

@end

@implementation HLAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationBar];
    [self baseConfigure];
    [self prepareDatas];
}

//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"关于我们";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];
    
}


- (void)baseConfigure {
    
    self.navigationItem.title = @"关于我们";
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT -64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _tableView.backgroundColor = [UIColor whiteColor];
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/5)];
    headView.backgroundColor = [UIColor redColor];
    UIImageView *trademarkImage = [[UIImageView alloc] init];
    trademarkImage.image = [UIImage imageNamed:@"图标"];
    trademarkImage.layer.masksToBounds = YES;
    trademarkImage.layer.cornerRadius = 40;
    [headView addSubview:trademarkImage];
    [trademarkImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(headView.mas_top).with.offset(SCREEN_HEIGHT/10 -40);
        make.left.equalTo(headView.mas_left).with.offset(SCREEN_WIDTH/2 -40);
        make.width.offset(80);
        make.height.offset(80);
    }];
    
    UILabel *versionLabel = [[UILabel alloc] init];
    versionLabel.text = @"halo365 V1.0.0";
    versionLabel.textColor = [UIColor whiteColor];
    versionLabel.font = [UIFont systemFontOfSize:16];
    versionLabel.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:versionLabel];
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(trademarkImage.mas_bottom).with.offset(5);
        make.left.equalTo(headView.mas_left).with.offset(SCREEN_WIDTH/2 - 70);
        make.width.offset(140);
        make.height.offset(16);
    }];
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/3)];
    UILabel *companyLabel = [[UILabel alloc] init];
    companyLabel.text = @"北京正离子科技有限公司";
    companyLabel.textColor = [UIColor lightGrayColor];
    companyLabel.font = [UIFont systemFontOfSize:14];
    companyLabel.textAlignment = NSTextAlignmentCenter;
    [footView addSubview:companyLabel];
    [companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(footView.mas_bottom).with.offset(-20);
        make.left.equalTo(footView.mas_left).with.offset(SCREEN_WIDTH/2 - 100);
        make.width.offset(200);
        make.height.offset(14);
    }];
    UILabel *Copyright = [[UILabel alloc] init];
    Copyright.text = @"Copyright ©2016-2017";
    Copyright.textColor = [UIColor lightGrayColor];
    Copyright.font = [UIFont systemFontOfSize:14];
    Copyright.textAlignment = NSTextAlignmentCenter;
    [footView addSubview:Copyright];
    [Copyright mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(companyLabel.mas_top).with.offset(-10);
        make.left.equalTo(footView.mas_left).with.offset(SCREEN_WIDTH/2 - 100);
        make.width.offset(200);
        make.height.offset(14);
    }];


    
    
    headView.backgroundColor = [UIColor redColor];
    footView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollEnabled = NO;
    _tableView.tableHeaderView = headView;
    _tableView.tableFooterView = footView;
    
    
    _tableView.rowHeight = 1;
    
    _contentWebView = [[UIWebView alloc] init];
//    _contentWebView.backgroundColor = [UIColor redColor];
    _contentWebView.delegate = self;
    _contentWebView.scrollView.showsVerticalScrollIndicator = NO;
    _contentWebView.scrollView.scrollEnabled = NO;
    _contentWebView.dataDetectorTypes = UIDataDetectorTypeNone;
    
    [self.view addSubview:_tableView];
    
}

- (void)prepareDatas {
    
    NSString *htmlString = @"<p>halo365的用户可以将自己的健康状况以及运动方法分享给大家，共同完善一个科学、严谨、能融入自己生活节奏的运动饮食计划库。</p><p></p><p> 我们知道用户需要的是一个科学、严谨、能融入自己生活节奏的运动饮食计划，我们将把这些不同生活状态的计划用视频的形式展现在用户眼前，用户可以通过对视频的学习寻找到属于自己的健康生活方式。光环365有社交属性，用户可以将自己的健康状态以及运动方法分享给大家。</p>";
    
    NSString *jsString = [NSString stringWithFormat:@"<html> \n"
                          "<head> \n"
                          "<link href=\"about.css\" rel=\"stylesheet\"> \n"
                          "</style> \n"
                          "</head> \n"
                          "<body>%@</body> \n"
                          "</html>", htmlString];
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    [_contentWebView loadHTMLString:jsString baseURL:baseURL];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *indentifier = @"webView";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:indentifier];
        cell.backgroundColor = bgVCcolor;
        [cell.contentView addSubview:_contentWebView];
        [_contentWebView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(0);
        }];
    }
    return cell;
}

#pragma mark - web view delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    CGFloat contentHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue] +20;
    NSLog(@"contentHeight:%.2f", contentHeight);
    _tableView.rowHeight = contentHeight;
    [_tableView reloadData];
}



//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
