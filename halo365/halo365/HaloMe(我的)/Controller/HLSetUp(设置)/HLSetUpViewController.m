//
//  HLSetUpViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//设置

#import "HLSetUpViewController.h"
#import "HLAccountBindingViewController.h"
#import "HLPrivacySettingsViewController.h"
#import "HLLinksHealthViewController.h"
#import "HLBeingPushedViewController.h"
#import "HLFeedbackViewController.h"
#import "HLAboutViewController.h"
#import "HLLoginViewController.h"
#import "HLInformationShareView.h"

@interface HLSetUpViewController ()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,HLInformationShareViewDelegate>

{
    UITableView *_tableView;
    NSArray *setArray;
    UIView *_shareMaskView;
}
@property (nonatomic, strong)HLInformationShareView *shareView;

@end

@implementation HLSetUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = bgVCcolor;
    setArray = @[@"账号绑定",@"隐私设置",@"链接健康",@"清理缓存",@"消息推送",@"推荐给好友",@"意见反馈",@"用户协议",@"关于我们",@"退出"];
    [self navigationBar];
    [self tableview];

}

//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"设置";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];
    
}

//定义tableView
- (void)tableview
{
    _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 64*Proportion_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64*Proportion_HEIGHT) style:UITableViewStylePlain];
    
    _tableView.dataSource=self;
    _tableView.delegate=self;
    _tableView.backgroundColor=[UIColor whiteColor];
//    _tableView.scrollEnabled=NO;
    [self.view addSubview:_tableView];
    [self hideExcessLine:_tableView];
}

//去掉多余的线
-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return setArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"OrderListCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = setArray[indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController = nil;
    switch (indexPath.row) {
        case 0:
        {
            //账号绑定
            viewController = [HLAccountBindingViewController new];
            [self.navigationController pushViewController:viewController animated:YES];

        }
            break;
        case 1:
        {
            //隐私设置
            viewController = [HLPrivacySettingsViewController new];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
        case 2:
        {
            //链接健康
            
        }
            break;
        case 3:
        {
            //清理缓存
            float tmpSize = [[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
            NSString *clearCacheName = tmpSize >= 1 ? [NSString stringWithFormat:@"清理缓存(%.2fM)",tmpSize] : [NSString stringWithFormat:@"清理缓存(%.2fK)",tmpSize * 1024];
            [HLWarning  showMessage:clearCacheName];
            
            [[SDImageCache sharedImageCache] clearDisk];
            [[SDImageCache sharedImageCache] clearMemory];
            
        }
            break;
        case 4:
        {
            //消息推送
            viewController = [HLBeingPushedViewController new];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
        case 5:
        {
            //推荐给好友
            UIWindow *window = [UIApplication sharedApplication].keyWindow;
            
            _shareMaskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            _shareMaskView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
            
            UIView *action = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -200)];
            action.backgroundColor = [UIColor clearColor];
            
            UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickMaskView)];
            [action addGestureRecognizer:tapGR];
            [_shareMaskView addSubview:action];
            
            _shareView = [[HLInformationShareView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 200)];
            
            _shareView.delegate = self;

            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.25f];
            _shareView.frame = CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 200);
            
            [UIView commitAnimations];
            [_shareMaskView addSubview:_shareView];
            [window addSubview:_shareMaskView];
            
            
        }
            break;
        case 6:
        {
            //意见反馈
            viewController = [HLFeedbackViewController new];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
        case 7:
        {
            //用户协议
            WebVC *helpview=[[WebVC alloc]init];
            helpview.hidesBottomBarWhenPushed=YES;
//            NSString*url=[NSString stringWithFormat:@"http://static.halo365.cn/privacy/"];
            
            helpview.urlString = User_Agreement;
            [self.navigationController pushViewController:helpview animated:YES];
            
            
        }
            break;
        case 8:
        {
            // 关于我们
            viewController = [HLAboutViewController new];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
        case 9:
        {
            //退出
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确定要注销?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self logOut];
                
            }]];

            [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
                
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];

        }
            break;

        default:
            break;
    }
}


-(void)didClickCancleButtonInBottomView
{
    
    [self didClickMaskView];
    
}
- (void)didClickMaskView
{
    [UIView animateWithDuration:0.25f animations:^{
        
        _shareView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 200);
        
    } completion:^(BOOL finished) {
        
        [_shareMaskView removeFromSuperview];
        [_shareView removeFromSuperview];
    }];
}

//代理方法
- (void)shareAppWithTag:(NSInteger)tag{
    
    NSLog(@"%ld",(long)tag);
//    if (tag==6000) {
//        //微信分享
//        [self wechat];
//        
//    }else if(tag==6001){
//        //朋友圈分享
//        [self fridenchat];
//    }else if (tag==6002){
//        //QQ好友
////        [self qqfriden];
//    }else if (tag==6003){
//        //空间
//        [self qqhome];
//    }
    
    
}

//退出登录
- (void)logOut
{
    //此处为网络请求部分
    //模拟注销
    [HLLoginManage sharedManager].loginState = 0;
    [[HLLoginManage sharedManager] judgeLoginState];
    
    //获取账号信息
    NSDictionary *accountDic = [[NSUserDefaults standardUserDefaults] objectForKey:AUTOLOGIN];
    NSString *account = [accountDic objectForKey:ACCOUNT];
    if (account == nil) {
        account = @"";
    }
    
    //保存账号，不保存密码
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:account,ACCOUNT,@"",PASSWORD, nil];
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:AUTOLOGIN];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
    HLLoginViewController *loginVC = [HLLoginViewController new];
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:loginVC animated:YES completion:nil];

}


//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
