//
//  HLUserAgreementViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLUserAgreementViewController.h"

@interface HLUserAgreementViewController ()

@end

@implementation HLUserAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    [self navigationBar];
    
}

- (void)navigationBar
{
    
    //加一个导航条
    UINavigationBar* navcBar=[[UINavigationBar alloc]init];
    [navcBar setBackgroundImage:[UIImage imageNamed:@"bg_user_center"] forBarMetrics:UIBarMetricsDefault];
    [self.view addSubview:navcBar];
    [self.view bringSubviewToFront:navcBar];
    [navcBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"用户协议";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [navcBar addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(27);
        make.left.offset(SCREEN_WIDTH/2-60);
        make.width.offset(120);
        make.height.offset(30);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"return"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [navcBar addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(6);
        make.width.offset(30);
        make.height.offset(28);
        make.bottom.offset(-8);
    }];
}

//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
