//
//  HLMeTableViewController.h
//  halo365
//
//  Created by 李鹏辉 on 16/7/21.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLBaseMeViewController : UIViewController

@property(nonatomic,strong)NSString *token;

@end
