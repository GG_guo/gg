//
//  InformationSetController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/5.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLInformationSetController.h"
#import "HLDatePicker.h"
#import "HLDatatPicker.h"
#import "HLCityPicker.h"

#import "HLBaseMeViewController.h"

@interface HLInformationSetController ()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    UITableView *_tableView;
    UIImagePickerController *_imagePickerController;
    UIImageView *headImage;//头像
    UIButton *buttonImage;
    NSArray *firstLabelArray;
    NSArray *secondLabelArray;
    NSArray *thirdLabelArray;
    NSMutableArray *totalLabelArray;
    
    NSArray *purposeArray;
    NSArray *firstShowLabelArray;
    NSArray *secondShowLabelArray;
    NSArray *thirdShowLabelArray;
    
    UITableViewCell *cell;
    
    //    UIButton *birthButton;
    UITextField *nameTextField;
    UIButton *nameButton;
    //    UIButton *stateButton;//状态按钮
    UIButton *purposeButton;//目标按钮
    UIButton *genderButton;//性别按钮
    NSString *genderString;
    UIButton *birthButton;
    
    UIButton *heightButton;
    UIButton *weightButton;
    
    UIButton *bodyFatButton;
    UIButton *goalButton;
    UIButton *activityButton;
    UIButton *intensityButton;
    
    NSString *goalString;
    NSString *activityString;
    NSString *intensityString;
    
    NSString *purposeString;
    NSString *purposeString1;
    
    NSString *purposeString2;
    NSString *purposeString3;
    
    UILabel *genderLabel;
    UILabel *invitecodeLabel;
    UIButton *rightButton;

}
@property (strong, nonatomic)  UILabel *provinceLabel;
@property (strong, nonatomic)  UILabel *cityLabel;

@end

@implementation HLInformationSetController

-(void)viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = bgVCcolor;
    
    [self navigationBar];
    
    [self upData];

    
 }

//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];

    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"编辑个人资料";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        //        make.bottom.offset(-14);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];

    //保存按钮
    rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"保存" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    
//    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [rightButton addTarget:self action:@selector(clickSave:) forControlEvents:UIControlEventTouchUpInside];
    
    [barView addSubview:rightButton];
    [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15);
        make.width.offset(40);
        make.height.offset(18);
        make.top.offset(33*Proportion_HEIGHT);
    }];
}

- (void)upData
{
    self.token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSLog(@"token == %@",self.token);
    
    [HLAFNworkingHeader requestURL:My_ExternalAPI httpMethod:@"GET" token:self.token params:nil file:nil success:^(id data) {
        NSLog(@"AA==%@",data);
        [_tableView removeFromSuperview];
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64*Proportion_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT -64*Proportion_HEIGHT) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_tableView];
        
        firstLabelArray = @[@"头像",@"昵称",@"性别",@"出身年月",@"地区",@"邀请码"];
        secondLabelArray = @[@"身高",@"体重",@"体脂"];
        thirdLabelArray  = @[@"目标",@"日常活动量",@"强度"];
        
        totalLabelArray = [[NSMutableArray alloc] initWithObjects:firstLabelArray,secondLabelArray,thirdLabelArray, nil];
        
        NSDictionary *dict = data[@"msg"];
        _model = [[Model alloc] initWithDic:dict];
        [_tableView reloadData];
        
        
    } fail:^(NSError *error) {
        NSLog(@"a=错误本身:%@ a=错误原因:%@ a=错误描述%@",
              error,
              error.localizedRecoverySuggestion,
              error.localizedDescription);
    }];
}

//指定标题的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section ==0) {
        return 0;
    }
    return 20;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return totalLabelArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[totalLabelArray objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && indexPath.section == 0) {
        return 76;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrderListCell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [[cell textLabel] setText:[[totalLabelArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row]];
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    
                    [self ImageViewData];
                    
                    break;
                case 1:
                {
                    [self NameData];
                }
                    break;
                case 2:
                    [self GenderData];
                    
                    break;
                case 3:
                    
                    [self BirthData];
                    
                    break;
                case 4:
                    [self CityData];
                    break;
                case 5:
                    [self InvitecodeData];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    [self HeightData];
                    break;
                case 1:
                    [self WeightData];
                    break;
                case 2:
                    [self BodyFatData];
                    
                    break;
                    
                default:
                    break;
            }
            
            
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                    [self TargetData];
                    
                    break;
                case 1:
                    [self ActivityData];
                    
                    break;
                case 2:
                    [self IntensityData];
                    
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        default:
            break;
    }
    
    return cell;
}

//头像
- (void)ImageViewData
{
    buttonImage = [UIButton buttonWithType:UIButtonTypeCustom];

    buttonImage.backgroundColor = [UIColor clearColor];
    [buttonImage setImage:[UIImage imageNamed:@"default_avatar"] forState:UIControlStateNormal];

    [buttonImage addTarget:self action:@selector(clickButtonImage) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:buttonImage];
    [buttonImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15);
        make.top.offset(13);
        make.height.offset(50);
        make.width.offset(50);
        
    }];
    
    headImage = [[UIImageView alloc] init];
    headImage.layer.masksToBounds = YES;
    headImage.layer.cornerRadius = 25;
    
    [headImage sd_setImageWithURL:[NSURL URLWithString:_model.avatar] placeholderImage:[UIImage imageNamed:@"default_avatar"]];
//    headImage.image = [UIImage imageNamed:@"default_avatar"];
    [cell addSubview:headImage];
    
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15);
        make.top.offset(13);
        make.height.offset(50);
        make.width.offset(50);
        
    }];
    
}

- (void)clickButtonImage
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"选择照片" message:@"从相册选取照片或拍照发送" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self takeCamera];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self photoSelect];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

//拍照
- (void)takeCamera
{
    BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
    if(!isCamera)
    {
        ShowAlert(@"无法调用相机,请检查系统设置");
        return;
    }
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.sourceType= UIImagePickerControllerSourceTypeCamera;
    _imagePickerController.delegate=self;
    //编辑模式
    _imagePickerController.allowsEditing= YES;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

//从相册选择
- (void)photoSelect
{
    //从相册选择
    _imagePickerController= [[UIImagePickerController alloc] init];
    //设置picker类型
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imagePickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:_imagePickerController.sourceType];
    
    //设置代理
    _imagePickerController.delegate = self;
    //设置图片可以修改
    _imagePickerController.allowsEditing = YES;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
    
}

//选中图片之后执行的方法将图片上传到服务器，上传成功之后直接改变头像
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    headImage.image=image;
    [_imagePickerController dismissViewControllerAnimated:YES completion:nil];
}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [_imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

//昵称
- (void)NameData
{
    nameTextField = [[UITextField alloc] init];
    nameTextField.placeholder = @"请输入你的昵称";
    nameTextField.text = _model.nickname;
    nameTextField.delegate = self;
    [nameTextField addTarget:self action:@selector(cancelKeyboard) forControlEvents:UIControlEventEditingDidEndOnExit];
    [cell addSubview:nameTextField];
    [nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
    }];
    
}

//性别
- (void)GenderData
{
    genderLabel = [[UILabel alloc] init];
    genderString = [NSString stringWithFormat:@"%@",_model.gender];
    if ([genderString  isEqual: @"m"]) {
        genderLabel.text = @"男";
        
    }else
    {
        genderLabel.text = @"女";
    }
    genderLabel.textColor = [UIColor lightGrayColor];
    [cell addSubview:genderLabel];
    [genderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
    }];
    
}

//出生年月
- (void)BirthData
{
    
    birthButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [birthButton setTitle:_model.birth forState:UIControlStateNormal];
    [birthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [birthButton setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    birthButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    birthButton.backgroundColor = [UIColor lightGrayColor];
    [birthButton addTarget:self action:@selector(clickBirthButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:birthButton];
    [birthButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(90);
        make.top.offset(0);
        make.width.offset(120);
        make.height.offset(44);
        
    }];
}

//地区
- (void)CityData
{
    UIButton *cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    cityButton.frame = CGRectMake(10,100,  self.view.frame.size.width -20, 43);
    cityButton.backgroundColor = [UIColor grayColor];
    [cityButton addTarget:self action:@selector(clickCityButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell  addSubview:cityButton];
    [cityButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(46);
    }];
    
    _provinceLabel = [[UILabel alloc] init];
    _provinceLabel.textColor = [UIColor whiteColor];
    _provinceLabel.font = [UIFont systemFontOfSize:15];
    //    _provinceLabel.frame = CGRectMake(0, 0, 100, 43);
    [cityButton addSubview:_provinceLabel];
    [_provinceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cityButton.mas_left).with.offset(10);
        make.width.offset(100);
        make.height.equalTo(cityButton.mas_height);
    }];
    
    _cityLabel = [[UILabel alloc] init];
    _cityLabel.textColor = [UIColor whiteColor];
    _cityLabel.font = [UIFont systemFontOfSize:15];
    //    _cityLabel.frame = CGRectMake(100, 0, 100, 43);
    [cityButton addSubview:_cityLabel];
    [_cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(cityButton.mas_right).with.offset(-10);
        make.width.offset(100);
        make.height.equalTo(cityButton.mas_height);
    }];
    
    
    
    NSString *provinceCode = _model.province;
    NSString *cityCode = _model.city;
    NSLog(@"code=%@,code1=%@",provinceCode,cityCode);
    
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"districtMGE" ofType:@"plist"]];
    
    //        NSLog(@"%@",dic);
    NSArray *provinceArray = dic[@"Division"];
    //         NSLog(@"%@",array);
    
    for(NSDictionary *dics in provinceArray)
    {
        //             NSLog(@"%@",dic);
        if([provinceCode isEqualToString:dics[@"DivisionCode"]])
        {
            //                NSLog(@"%@",dic);
            self.provinceLabel.text = dics[@"DivisionName"];
            NSArray *cityArray = dics[@"DivisionSub"];
            for(NSDictionary *dicccc in cityArray)
            {
                NSLog(@"dicccc =%@",dicccc);
                
                if ([cityCode isEqualToString:dicccc[@"DivisionCode"]]) {
                    self.cityLabel.text = dicccc[@"DivisionName"];
                }
            }
        }
    }
    
}

//邀请码
- (void)InvitecodeData
{
    invitecodeLabel = [[UILabel alloc] init];
    invitecodeLabel.text = _model.invitecode;
    invitecodeLabel.textColor = [UIColor lightGrayColor];
    [cell addSubview:invitecodeLabel];
    [invitecodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
    }];
}

//身高
- (void)HeightData
{
    heightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    if (indexPath.row == 0) {
    [heightButton setTitle:[NSString stringWithFormat:@"%@ cm",_model.height]forState:UIControlStateNormal];
    heightButton.tag = 100;

    [heightButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
   
    [heightButton addTarget:self action:@selector(clickDataButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:heightButton];
    [heightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
        
    }];
}

//体重
- (void)WeightData
{
    weightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    if (indexPath.row == 0) {
    [weightButton setTitle:[NSString stringWithFormat:@"%@",_model.weight]forState:UIControlStateNormal];
    weightButton.tag = 101;
    //        rightButton.tag = 100;
    //    }else if (indexPath.row == 1){
    //        [stateButton setTitle:[NSString stringWithFormat:@"%@ kg",_model.weight]forState:UIControlStateNormal];
    //        stateButton.tag = 101;
    //    }
    //
    [weightButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //            stateButton.tag = 100 +indexPath.row;
    //            tagNumber = (long)stateButton.tag;
    [weightButton addTarget:self action:@selector(clickDataButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:weightButton];
    [weightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
        
    }];
}


//体脂
- (void)BodyFatData
{
    
}

//目标
- (void)TargetData
{
    goalButton = [UIButton buttonWithType:UIButtonTypeCustom];
    goalButton.tag = 103;
    goalString = [NSString stringWithFormat:@"%@",_model.goal];
    if ([goalString isEqualToString:@"fat"]) {
        [goalButton setTitle:@"减脂" forState:UIControlStateNormal];
    }else if ([goalString isEqualToString:@"body"]){
        [goalButton setTitle:@"塑身" forState:UIControlStateNormal];
    }else
    {
        [goalButton setTitle:@"增肌" forState:UIControlStateNormal];
        
    }
    [goalButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [goalButton addTarget:self action:@selector(clickDataButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:goalButton];
    [goalButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
        
    }];
 }

//活动量
- (void)ActivityData
{
    
}

//强度
- (void)IntensityData
{
    intensityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    intensityButton.tag = 105;
    goalString = [NSString stringWithFormat:@"%@",_model.basis];
    if ([goalString isEqualToString:@"1"]) {
        [intensityButton setTitle:@"初级" forState:UIControlStateNormal];
    }else if ([goalString isEqualToString:@"2"]){
        [intensityButton setTitle:@"中级" forState:UIControlStateNormal];
    }else
    {
        [intensityButton setTitle:@"高级" forState:UIControlStateNormal];
        
    }
    [intensityButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [intensityButton addTarget:self action:@selector(clickDataButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:intensityButton];
    [intensityButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.offset(0);
        make.width.offset(SCREEN_WIDTH - 100);
        make.height.offset(44);
        
    }];
    
}


//选中pickerview日期返回数据
- (void)clickBirthButton:(UIButton *)sender
{
    HLDatePicker* picker = [[HLDatePicker alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH  -40 , 300)];
    
    //配置中心，详情见KSDatePikcerApperance
    
    picker.appearance.radius = 5;
    
    //设置回调
    picker.appearance.resultCallBack = ^void(HLDatePicker* datePicker,NSDate* currentDate,KSDatePickerButtonType buttonType){
        
        if (buttonType == KSDatePickerButtonCommit) {
            
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            
            [sender setTitle:[formatter stringFromDate:currentDate] forState:UIControlStateNormal];
        }
    };
    // 显示
    [picker show];
    
}

- (void)clickCityButton:(UIButton *)sender
{
    //x,y 值无效，默认是居中的
    HLCityPicker *picker = [[HLCityPicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 40, 300)];
    picker.appearance.radius = 5;
    
    // 显示
    [picker show];
    picker.ActionDistrictViewSelectBlock = ^(NSString *desStr,NSDictionary *selectDistrictDict){
        
        self.provinceLabel.text = [selectDistrictDict objectForKey:Key_DistrictSelectProvince];
        self.cityLabel.text = [selectDistrictDict objectForKey:Key_DistrictSelectCity];
        
    };
}

- (void)clickDataButton:(UIButton *)sender
{
    //x,y 值无效，默认是居中的
    HLDatatPicker* picker = [[HLDatatPicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 40, 300)];
    
    if (sender.tag == 99) {
        picker.headerViewLabel.text = @"选择性别";
        NSArray *genderArray = @[@"男",@"女"];
        for (int i =0 ; i<genderArray.count; i ++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%@",genderArray[i]]];
            
        }
    }
    if (sender.tag ==100) {
        picker.headerViewLabel.text =@"选择身高";
        for (int i = 100; i <= 240; i++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }
    if (sender.tag == 101) {
        picker.headerViewLabel.text =@"选择体重";
        
        for (int i = 120; i<=200; i++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
    }
    if (sender.tag == 102) {
        picker.headerViewLabel.text = @"选择体脂";
        for (int i = 0; i<=50; i++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
    }
    if (sender.tag == 103) {
        picker.headerViewLabel.text = @"选择目标";
        NSArray *targetArray = @[@"减脂",@"增肌",@"塑身"];
        for (int i = 0; i<targetArray.count; i++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%@",targetArray[i]]];
            
        }
    }
    
    if (sender.tag == 104) {
        picker.headerViewLabel.text = @"选择活动量";
        for (int i = 0; i<=50; i++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
    }
    if (sender.tag == 105) {
        picker.headerViewLabel.text = @"选择活动量";
        NSArray *intensityArray = @[@"初级",@"中级",@"高级"];
        for (int i = 0; i< intensityArray.count; i++) {
            [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%@",intensityArray[i]]];
            
        }
    }
    
    //设置回调
    picker.appearance.heightCallBack = ^void(HLDatatPicker * datePicker,NSString* str,KSDatePickerButtonType buttonType){
        
        if (buttonType == KSDatePickerButtonCommit) {
            
            [sender setTitle:str forState:UIControlStateNormal];
            
        }
    };
    
    // 显示
    [picker show];
}

- (void)cancelKeyboard
{
    return;
}

//点击保存按钮
- (void)clickSave:(UIButton *)sender
{
    //    NSLog(@"token =%@",self.token);
    
    NSMutableDictionary *parmars=[NSMutableDictionary dictionary];
    parmars[@"nickname"] = nameTextField.text;
    parmars[@"birth"] = birthButton.titleLabel.text;
    
    
    NSString *provinceName = _provinceLabel.text;
    NSString *cityName = _cityLabel.text;
    
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"districtMGE" ofType:@"plist"]];
    
    //        NSLog(@"%@",dic);
    NSArray *provinceArray = dic[@"Division"];
    //         NSLog(@"%@",array);
    
    for(NSDictionary *dics in provinceArray)
    {
        //             NSLog(@"%@",dic);
        if([provinceName isEqualToString:dics[@"DivisionName"]])
        {
            //                NSLog(@"%@",dic);
            parmars[@"province"] = dics[@"DivisionCode"];
            
            NSArray *cityArray = dics[@"DivisionSub"];
            for(NSDictionary *dicccc in cityArray)
            {
                NSLog(@"dicccc =%@",dicccc);
                
                if ([cityName isEqualToString:dicccc[@"DivisionName"]]) {
                    
                    parmars[@"city"]  = dicccc[@"DivisionCode"];
                }
            }
        }
    }
    
    //    NSString *provinceStr = [[NSString alloc] init];
    
    
    
    parmars[@"height"] = heightButton.titleLabel.text;
    parmars[@"weight"] = weightButton.titleLabel.text;
    
    NSString *goalStr = [[NSString alloc] init];
    goalStr = goalButton.titleLabel.text;
    if ([goalStr isEqualToString:@"减脂"]) {
        parmars[@"goal"] = @"fat";
    }else if ([goalStr isEqualToString:@"塑身"]){
        parmars[@"goal"] = @"body";
    }else{
        parmars[@"goal"] = @"muscle";
    }
    
    NSString *intensityStr = [[NSString alloc] init];
    intensityStr = intensityButton.titleLabel.text;
    if ([intensityStr isEqualToString:@"初级"]) {
        parmars[@"basis"] = @"1";
    }else if ([intensityStr isEqualToString:@"中级"]){
        parmars[@"basis"] = @"2";
    }else{
        parmars[@"basis"] = @"3";
    }
    
    [HLAFNworkingHeader requestURL:My_ExternalAPI httpMethod:@"PUT" token:self.token params:parmars file:nil success:^(id data) {
//        NSLog(@"AA==%@",data);
        NSLog(@"AJDSHFJKAJKDFHJKA");
        
        [_model setValue:nameTextField.text forKey:@"nickname"];
        [_model setValue:birthButton.titleLabel.text forKey:@"birth"];
        [_model setValue:_provinceLabel.text forKey:@"province"];
        [_model setValue:_cityLabel.text forKey:@"city"];
        [_model setValue:heightButton.titleLabel.text forKey:@"height"];
        [_model setValue:weightButton.titleLabel.text forKey:@"weight"];
        [_model setValue:goalButton.titleLabel.text forKey:@"goal"];
        [_model setValue:intensityButton.titleLabel.text forKey:@"basis"];
        
//        HLMeTableViewController *exitVC =[[HLMeTableViewController alloc] init];
//       
//        [self.navigationController pushViewController:exitVC animated:YES];
////
        [self.navigationController popViewControllerAnimated:YES];

        
    } fail:^(NSError *error) {
        
        NSLog(@"a=错误本身:%@ a=错误原因:%@ a=错误描述%@",
              error,
              error.localizedRecoverySuggestion,
              error.localizedDescription);
    }];

}

- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
