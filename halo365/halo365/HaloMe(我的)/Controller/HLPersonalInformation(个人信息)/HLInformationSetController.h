//
//  InformationSetController.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/5.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface HLInformationSetController : UIViewController
{
    Model *_model;
}

@property(nonatomic,strong)NSString *token;

@end
