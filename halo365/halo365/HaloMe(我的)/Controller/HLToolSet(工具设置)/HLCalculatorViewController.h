//
//  HLCalculatorViewController.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/18.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//  计时器

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
@interface HLCalculatorViewController : UIViewController<AVAudioPlayerDelegate>

{
    NSString *audioString;
}

@property (nonatomic,retain)AVAudioPlayer *player;


+(id)shareUserContext;


@end
