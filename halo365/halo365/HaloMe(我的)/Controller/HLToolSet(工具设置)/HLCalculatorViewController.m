//
//  HLCalculatorViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/18.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCalculatorViewController.h"

static HLCalculatorViewController * play=nil;
#define kViewHeight self.view.bounds.size.height
#define kViewWight self.view.bounds.size.width

@interface HLCalculatorViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>

{
    
    NSMutableArray *timeArr;
    NSMutableArray *minArr;
    NSMutableArray *secArr;
    
    NSInteger time;
    NSInteger timeOld;
    
    NSTimer *timer;
    
    NSInteger min;
    NSInteger minOld;
    
    NSInteger sec;
    NSInteger secOld;
    
    NSString *minute;
    
    NSString *second;
    
    UIPickerView *selectTimer;
    
    UILabel *totalLabel;
    UIButton *start;
    
    UIButton *thirtyButton;
    UIButton *sixtyButton;
    UIButton *ninetyButton;
    //    TimerView *timerView;
    
}
@property (nonatomic) BOOL flag;
//@property (nonatomic) CGPoint beginPoint;
@property (nonatomic) CGRect firstFrame;

@end

@implementation HLCalculatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationBar];
    self.view.backgroundColor =[UIColor lightGrayColor];
    
    //选择控制器
    selectTimer=[[UIPickerView alloc]initWithFrame:CGRectMake(5, 75, kViewWight-10, 100)];
    selectTimer.delegate=self;
    selectTimer.dataSource=self;
    //设置没设置都是一个样
    selectTimer.showsSelectionIndicator=YES;
    selectTimer.layer.cornerRadius = 10;
    selectTimer.backgroundColor =[UIColor whiteColor];
    
    [self.view addSubview:selectTimer];
    
    minArr= [[NSMutableArray alloc]initWithCapacity:60];
    secArr =[[NSMutableArray alloc]initWithCapacity:60];
    for (int i=0; i<60; i++)
    {
        
        NSString *str = [NSString stringWithFormat:@"%02d",i];
        [minArr addObject:str];
        [secArr addObject:str];
        
    }
    
    timeArr =[[NSMutableArray alloc]initWithObjects:minArr,secArr, nil];
    
    //显示label
    totalLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 75, kViewWight-10, 100)];
    totalLabel.font=[UIFont fontWithName:@"Chalkboard SE" size:(kViewWight*3/4-10)/5];
    totalLabel.textColor=[UIColor colorWithRed:255/255.0 green:204/255.0 blue:102/255.0 alpha:1];
    totalLabel.textAlignment=NSTextAlignmentCenter;
    totalLabel.backgroundColor=[UIColor whiteColor];
    totalLabel.layer.cornerRadius =10;
    totalLabel.clipsToBounds=YES;
    [self.view addSubview:totalLabel];
    
    start = [UIButton buttonWithType:UIButtonTypeCustom];
    start.backgroundColor = [UIColor whiteColor];
    start.layer.cornerRadius = 20;
    start.titleLabel.font = [UIFont systemFontOfSize:30];
    start.frame = CGRectMake(3*kViewWight/8+10, 210, kViewWight/4-10, kViewWight/4-10);
    start.enabled = NO;
    totalLabel.hidden = YES;
    selectTimer.hidden = NO;
    [start setTitle:@"开始" forState:UIControlStateNormal];
    [start setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [start addTarget:self action:@selector(startTimer:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:start];
    
    //倒计时30s
    thirtyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    thirtyButton.backgroundColor = [UIColor whiteColor];
    thirtyButton.layer.cornerRadius = 10;
    thirtyButton.frame = CGRectMake(10 , 300, (kViewWight-40)/3, 40);
    [thirtyButton setTitle:@"30" forState:UIControlStateNormal];
    [thirtyButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [thirtyButton addTarget:self action:@selector(thirtyTimer:)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:thirtyButton];
    
    //倒计时60s
    sixtyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sixtyButton.backgroundColor = [UIColor whiteColor];
    sixtyButton.layer.cornerRadius = 10;
    sixtyButton.frame = CGRectMake((kViewWight-40)/3+20 , 300, (kViewWight-40)/3, 40);
    [sixtyButton setTitle:@"60" forState:UIControlStateNormal];
    [sixtyButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [sixtyButton addTarget:self action:@selector(sixtyTimer:)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sixtyButton];
    
    //倒计时90s
    ninetyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    ninetyButton.backgroundColor = [UIColor whiteColor];
    ninetyButton.layer.cornerRadius = 10;
    ninetyButton.frame = CGRectMake(10 +((kViewWight-40)/3+10)*2 , 300, (kViewWight-40)/3, 40);
    [ninetyButton setTitle:@"90" forState:UIControlStateNormal];
    [ninetyButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [ninetyButton addTarget:self action:@selector(ninetyTimer:)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ninetyButton];
}


//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"计时器";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        //        make.bottom.offset(-14);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];
    
}


//修改pickerView内部颜色和字体
-(NSAttributedString*)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *str = timeArr[component][row];
    NSMutableAttributedString *attriStr =[[NSMutableAttributedString alloc]initWithString:str];
    [attriStr addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18],NSForegroundColorAttributeName:[UIColor blueColor]} range:NSMakeRange(0, attriStr.length)];
    
    return attriStr;
}

//开始计时
-(void)startTimer:(UIButton*)btn
{
    
    if ([btn.titleLabel.text isEqualToString:@"开始"]) {
        totalLabel.hidden = NO;
        selectTimer.hidden = YES;
        thirtyButton.hidden = YES;
        sixtyButton.hidden = YES;
        ninetyButton.hidden = YES;
        
        [btn setTitle:@"取消" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        if (timer != nil) {
            //开启定时器
            timer.fireDate = [NSDate distantPast];
            
        }
    }else
    {
        totalLabel.hidden = YES;
        selectTimer.hidden = NO;
        thirtyButton.hidden = NO;
        sixtyButton.hidden = NO;
        ninetyButton.hidden = NO;
        
        [btn setTitle:@"开始" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        
        //销毁定时器
        [self deallocTimer];
        //获取老数据
        [self getOldData];
        //获取新数据
        [self getNewData];
        //显示倒计时
        [self setLableText];
        //添加定时器
        [self addTimer];
    }
    
}


- (void)thirtyTimer:(UIButton *)btn
{
    totalLabel.hidden = NO;
    selectTimer.hidden = YES;
    start.enabled = YES;
    btn.hidden = YES;
    sixtyButton.hidden = YES;
    ninetyButton.hidden =YES;
    
    [start setTitle:@"取消" forState:UIControlStateNormal];
    [start setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    second =@"30";
    sec =30;
    time = 30;
    
    
    [self deallocTimer];
    [self addTimer];
    if (timer != nil) {
        //开启定时器
        timer.fireDate = [NSDate distantPast];
    }
    
}

- (void)sixtyTimer:(UIButton *)btn
{
    totalLabel.hidden = NO;
    selectTimer.hidden = YES;
    start.enabled = YES;
    btn.hidden = YES;
    thirtyButton.hidden = YES;
    ninetyButton.hidden =YES;
    [start setTitle:@"取消" forState:UIControlStateNormal];
    [start setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    minute = @"01";
    second =@"00";
    min = 1;
    sec =0;
    time = 60;
    
    [self deallocTimer];
    [self addTimer];
    if (timer != nil) {
        //开启定时器
        timer.fireDate = [NSDate distantPast];
    }
    
}

- (void)ninetyTimer:(UIButton *)btn
{
    totalLabel.hidden = NO;
    selectTimer.hidden = YES;
    start.enabled = YES;
    btn.hidden = YES;
    thirtyButton.hidden = YES;
    sixtyButton.hidden =YES;
    
    [start setTitle:@"取消" forState:UIControlStateNormal];
    [start setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    minute = @"01";
    second =@"30";
    min = 1;
    sec =30;
    time = 90;
    
    [self deallocTimer];
    [self addTimer];
    if (timer != nil) {
        //开启定时器
        timer.fireDate = [NSDate distantPast];
    }
    
}

//销毁定时器
- (void)deallocTimer
{
    //置空定时器
    if (timer != nil) {
        [timer invalidate];
        timer = nil;
    }
}

//获取老数据
- (void)getOldData
{
    min = minOld;
    sec = secOld;
    time = timeOld;
}

//获取新数据
- (void)getNewData
{
    minute= timeArr[0][min];
    second= timeArr[1][sec];
}

//显示倒计时
- (void)setLableText
{
    totalLabel.text= [NSString stringWithFormat:@"%@:%@",minute,second];
    
}

//添加定时器
- (void)addTimer
{
    timer =[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timer:) userInfo:nil repeats:YES];
    
    //关闭定时器
    timer.fireDate= [NSDate distantFuture];
    
}

//倒计时是否为0
-(void)timer:(NSTimer*)timerr
{
    [self getNewData];
    
    if (time < 0) {
        
        [self promptVoice];
        
        //初始化提示框；
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"按钮被点击了" preferredStyle: UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [_player stop];
        }]];
        
        //弹出提示框；
        [self presentViewController:alert animated:true completion:nil];
        
        
        timer.fireDate = [NSDate distantFuture];
        [start setTitle:@"开始" forState:UIControlStateNormal];
        [start setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        
        totalLabel.hidden = YES;
        selectTimer.hidden = NO;
        self.view.hidden=NO;
        thirtyButton.hidden = NO;
        sixtyButton.hidden = NO;
        ninetyButton.hidden = NO;
        
        [self getOldData];
        [self getNewData];
        
    }
    else{
        time--;
    }
    //    hour = time/3600;
    min = time%3600/60;
    sec = time%60;
    if(sec<0)
    {
        sec = 0;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setLableText];
    });
    
}
#pragma mark - UIPickUpView 数据
#pragma mark-UIPickUpView 有多少个分组

//获取分组数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return timeArr.count;
}


#pragma mark  -每组多少行
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [timeArr[component] count];
}

//刷新所有的分组
- (void)reloadAllComponents
{
    
}
#pragma mark -UIPickerView数据代理
#pragma mark 对应组对应行的数据


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return timeArr[component][row];
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //获取对应组对应行数的数据
    NSString *timerStr =timeArr[component][row];
    switch (component) {
        case 0:
            
            minute =timerStr;
            min =row;
            
            minOld = min;
            break;
        case 1:
            
            second =timerStr;
            sec =row;
            
            secOld = sec;
            
            break;
            
        default:
            break;
    }
    
    time = min*60+sec;
    timeOld = time;
    
    if(time != 0)
    {
        start.enabled = YES;
        [start setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    }else{
        
        start.enabled = NO;
        [start setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    
    [self deallocTimer];
    [self addTimer];
}

//提示音
- (void)promptVoice
{
    audioString = @"001";
    
    NSError *error = nil;
    NSString *path = [[NSBundle mainBundle]pathForResource:audioString ofType:@"mp3"];
    
    NSURL *url = [NSURL fileURLWithPath:path];
    
    _player  = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    _player.delegate = self;
    
    [_player prepareToPlay];
    [_player play];
    
    [_player setVolume:1];
}

+(id)shareUserContext
{
    @synchronized(self)
    {
        if(play==nil)
        {
            //线程保护，如果实例为nil，则创建该实例
            play=[[[self class]alloc]init];
        }
    }
    return play;
}

//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
