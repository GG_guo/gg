//
//  HLToolSetViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLToolSetViewController.h"
#import "HLConversionViewController.h"
#import "HLCalculatorViewController.h"

@interface HLToolSetViewController ()<UITableViewDelegate,UITableViewDataSource>

{
    NSArray *toolArray;
}
@end

@implementation HLToolSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = bgVCcolor;
    toolArray = @[@"计时器",@"单位换算",@"营养元素摄入量计算",@"基础代谢率BMR计算",@"肌酸摄入量计算",@"每日所需摄入水量计算",@"计算你的瘦体重",@"就算你的最大摄氧量"];
    
    [self navigationBar];
    [self baseConfigure];
}

//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"工具";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        //        make.bottom.offset(-14);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];
    
}

- (void)baseConfigure
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT -64) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tableView];
    
    [self hideExcessLine:tableView];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return toolArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = toolArray[indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        
    }
    
    return cell;
}
//去掉多余的线
-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger cellindex = indexPath.row;
    switch (cellindex) {
        case 0:
            
            [self calculator];
            break;
        case 1:
            [self conversion];
            break;
        case 2:
            [self nutrietelement];
            break;
        case 3:
            [self metabolic];
            break;
        case 4:
            [self creatine];
            break;
        case 5:
            [self amountWater];
            break;
        case 6:
            [self amountWater];
            break;
        case 7:
            [self leanBodyMass];
            break;
        case 8:
            [self oxygenUptake];
            break;
        default:
            break;
    }
    
    NSLog(@"你点击了我");
    
}

//计时器
- (void)calculator
{
    
    HLCalculatorViewController *viewController = [HLCalculatorViewController new];
    [self.navigationController pushViewController:viewController animated:YES];

}

//单位换算
- (void)conversion
{
   HLConversionViewController *viewController = [HLConversionViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

//营养元素摄入量
- (void)nutrietelement
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@macronutrients.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

//代谢率
- (void)metabolic
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@bmr.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

//肌酸摄入量
- (void)creatine
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@creatine.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];

}

//所需摄入水量
- (void)amountWater
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@water.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

//计算瘦体重
- (void)leanBodyMass
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@lbm.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
 
}

//计算你的最大摄氧量
- (void)oxygenUptake
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@vo2max.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}


//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
