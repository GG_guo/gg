//
//  HLConversionViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/14.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLConversionViewController.h"
#import "WebVC.h"

@interface HLConversionViewController ()<UITableViewDelegate,UITableViewDataSource>

{
    NSArray *toolArray;
}
@end

@implementation HLConversionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = bgVCcolor;
    toolArray = @[@"热量单位换算",@"重量单位换算",@"长度单位换算",@"体积单位换算"];
    
    [self navigationBar];
    [self baseConfigure];
}

//添加一个导航栏
- (void)navigationBar
{
    UIView *barView = [[UIView alloc] init];
    barView.backgroundColor =  ZLColor(22, 40, 77);
    [self.view addSubview:barView];
    [barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64*Proportion_HEIGHT);
        make.right.offset(0);
    }];
    
    //导航条标题
    UILabel*titleLabel=[[UILabel alloc]init];
    titleLabel.text=@"工具";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    [barView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.centerX.equalTo(barView);
        //        make.bottom.offset(-14);
        make.width.offset(120);
        make.height.offset(18);
    }];
    
    //返回按钮
    UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(33*Proportion_HEIGHT);
        make.left.offset(15);
        make.width.offset(11);
        make.height.offset(18);
    }];
    
}


- (void)baseConfigure
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT -64) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tableView];
    
    [self hideExcessLine:tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return toolArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = toolArray[indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        
    }
    
    return cell;
}
//去掉多余的线
-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger cellindex = indexPath.row;
    switch (cellindex) {

        case 0:
            [self ThermalConversion];
            break;
        case 1:
            [self WeightConversion];
            break;
        case 2:
            [self LengthConversion];
            break;
        case 3:
            [self VolumeConversion];
            break;
        default:
            break;
    }
    
    NSLog(@"你点击了我");
    
}


//热量单位转换
- (void)ThermalConversion
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@energy.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

//重量单位转换
- (void)WeightConversion
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@weight.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

//长度单位转换
- (void)LengthConversion
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@length.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
    
}

//体积单位转换
- (void)VolumeConversion
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@volume.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

//返回按钮
- (void)clickReturn
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
