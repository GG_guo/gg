//
//  HLMeTableViewController.m
//  halo365
//
//  Created by 李鹏辉 on 16/7/21.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLBaseMeViewController.h"
#import "HLInformationSetController.h"
#import "HLToolSetViewController.h"
#import "HLSetUpViewController.h"

@interface HLBaseMeViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIView *dynamicView;
    NSArray *cellTitle;
    UIImageView *backImageView;
}

//头像·
@property (nonatomic, strong) UIImageView *avatarImage;

//显示名字
@property (nonatomic, strong) UILabel *nameLabel;

//身高
@property (nonatomic, strong) UILabel *heigthNumber;

//体重
@property (nonatomic, strong) UILabel *weightNumber;

//
@property (nonatomic, strong) UILabel *bodyLabel;

//体脂
@property (nonatomic, strong) UILabel *bodyfatNumber;



@end

@implementation HLBaseMeViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
//    [self setNavigationItem];
    [self setInfrastructure];

}

//信息提醒按钮
- (void)clickLeftItem{
    
}

//打卡功能
- (void)clickRightItem{
    
}

//内容设置
- (void)setInfrastructure
{
    //设置个人显示
    [self setPersonalShowView];
    //设置动态视图
    [self setDynamicView];
    //设置tableview
    [self setTableView];
    
}

- (void)setPersonalShowView
{
    backImageView = [[UIImageView alloc] init];
    backImageView.image = [UIImage imageNamed:@"上底图"];
    [self.view addSubview:backImageView];
    [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(194*Proportion_WIDTH);
        make.width.offset(SCREEN_WIDTH);

    }];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"navigationbar_message"] forState:UIControlStateNormal];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"navigationbar_messageHL"] forState:UIControlStateHighlighted];
    [leftButton addTarget:self action:@selector(clickLeftItem) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(29*Proportion_HEIGHT);
        make.left.offset(15*Proportion_WIDTH);
        make.width.offset(25*Proportion_WIDTH);
        make.height.offset(25*Proportion_HEIGHT);
    }];
    
    //右侧信息按钮
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"navigationbar_clock"] forState:UIControlStateNormal];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"navigationbar_clockHL"] forState:UIControlStateHighlighted];
    [rightButton addTarget:self action:@selector(clickRightItem) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightButton];
    [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(29*Proportion_HEIGHT);
        make.right.offset(-15*Proportion_WIDTH);
        make.width.offset(25*Proportion_WIDTH);
        make.height.offset(25*Proportion_HEIGHT);
    }];
    
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = [UIImage imageNamed:@"halo365"];
    iconView.frame = CGRectMake((SCREEN_WIDTH - 101)/2*Proportion_WIDTH, 37*Proportion_HEIGHT, 101*Proportion_WIDTH, 54/2*Proportion_HEIGHT);
    [self.view addSubview:iconView];
    
    //头像
    _avatarImage = [[UIImageView alloc] init];
//    _avatarImage.backgroundColor = [UIColor redColor];
    [backImageView addSubview:_avatarImage];
    [_avatarImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(80);
        make.left.offset(17);
        make.width.offset(100);
        make.height.offset(103);
    }];
    
    //姓名
    _nameLabel = [[UILabel alloc] init];
//    _nameLabel.text = @"魏龙";
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.font = [UIFont systemFontOfSize:18];
    [backImageView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_avatarImage.mas_right).offset(32);
        make.top.offset(96);
        make.width.offset(SCREEN_WIDTH -132);
        make.height.offset(38);
    }];
    
    NSInteger width = (SCREEN_WIDTH - 118)/3;
    
    NSArray *bodyLabelArray = @[@"身高",@"体重",@"体脂"];
    
    for (int i = 0; i<bodyLabelArray.count; i++) {
        _bodyLabel = [[UILabel alloc] init];
        //        heightLabel.backgroundColor = [UIColor grayColor];
        
        _bodyLabel.text = bodyLabelArray[i];
        _bodyLabel.textAlignment = NSTextAlignmentCenter;
        _bodyLabel.textColor = [UIColor whiteColor];
        _bodyLabel.font = [UIFont systemFontOfSize:12];
        [backImageView addSubview:_bodyLabel];
        [_bodyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(117+(width+1.5)*i);
            make.top.equalTo(_nameLabel.mas_bottom).offset(4);
            make.width.offset(width);
            make.height.offset(17);
        }];
        
    }
    
    //身高
    _heigthNumber = [[UILabel alloc] init];
    _heigthNumber.textColor = [UIColor whiteColor];
    _heigthNumber.font = [UIFont systemFontOfSize:14];
    _heigthNumber.textAlignment = NSTextAlignmentRight;
    [backImageView addSubview:_heigthNumber];
    [_heigthNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(117);
        make.top.equalTo(_bodyLabel.mas_bottom).offset(-2);
        make.width.offset(width/2+4);
        make.height.offset(19);
    }];
    
    NSArray *bodyUnit = @[@"CM",@"KG",@" %"];
    for (int j = 0; j<bodyUnit.count;  j++) {
        
        UILabel *heightUnit = [[UILabel alloc] init];
        heightUnit.textAlignment = NSTextAlignmentNatural;
        heightUnit.textColor = [UIColor whiteColor];
        heightUnit.text = bodyUnit[j];
        heightUnit.font = [UIFont systemFontOfSize:9];
        [backImageView addSubview:heightUnit];
        [heightUnit mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_heigthNumber.mas_right).offset(2+(width+2)*j);
            make.top.equalTo(_heigthNumber.mas_top).offset(2);
            make.width.offset(width/2-6);
            make.height.offset(17);
        }];
        
    }

    //体重
    _weightNumber = [[UILabel alloc] init];
    _weightNumber.textColor = [UIColor whiteColor];
    _weightNumber.font = [UIFont systemFontOfSize:14];
    _weightNumber.textAlignment = NSTextAlignmentRight;
    [backImageView addSubview:_weightNumber];
    [_weightNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_avatarImage.mas_right).offset(width+1);
        make.top.equalTo(_bodyLabel.mas_bottom).offset(-2);
        make.width.offset(width/2+4);
        make.height.offset(19);
    }];
    
    //体脂
    _bodyfatNumber = [[UILabel alloc] init];
    //    bodyfatNumber.backgroundColor = [UIColor yellowColor];
    
    _bodyfatNumber.text = @"0";
    _bodyfatNumber.textColor = [UIColor whiteColor];
    _bodyfatNumber.font = [UIFont systemFontOfSize:14];
    _bodyfatNumber.textAlignment = NSTextAlignmentRight;
    [backImageView addSubview:_bodyfatNumber];
    [_bodyfatNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_avatarImage.mas_right).offset(2*(width+1));
        make.top.equalTo(_bodyLabel.mas_bottom).offset(-2);
        make.width.offset(width/2+4);
        make.height.offset(19);
    }];
    
    //请求数据
    [self requestData];
}

//代理 点击头像按钮
- (void)clickAvatar
{
    NSLog(@"dianji");
}


- (void)requestData
{
    self.token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    //    NSLog(@"token ==%@",self.token);
    [HLAFNworkingHeader requestURL:My_ExternalAPI httpMethod:@"GET" token:self.token params:nil file:nil success:^(id data) {
        NSLog(@"tt==%@",data);
        
        [_avatarImage sd_setImageWithURL:[NSURL URLWithString:[data[@"msg"] objectForKey:@"avatar"]]];
        _nameLabel.text = [data[@"msg"] objectForKey:@"nickname"];
        _heigthNumber.text = [NSString stringWithFormat:@"%@",[data[@"msg"] objectForKey:@"height"]];
        _weightNumber.text = [NSString stringWithFormat:@"%@",[data[@"msg"] objectForKey:@"weight"]];
        _bodyfatNumber.text = [NSString stringWithFormat:@"%@",[data[@"msg"] objectForKey:@"bodyfat"]];
        
    } fail:^(NSError *error) {
        NSLog(@"a=错误本身:%@ a=错误原因:%@ a=错误描述%@",
              error,
              error.localizedRecoverySuggestion,
              error.localizedDescription);
    }];
}

- (void)setDynamicView
{
    NSArray *imageArray = @[@"history",@"dynamic",@"collection_ big",@"tools"];
    NSArray *imageArray_HL = @[@"history_HL",@"dynamic_HL",@"collection_ big_HL",@"tools_HL"];
    dynamicView = [[UIView alloc] init];
    dynamicView.backgroundColor = ZLColor(250, 250, 250);
    [self.view addSubview:dynamicView];
    [dynamicView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backImageView.mas_bottom).offset(5);
        make.left.offset(0);
        make.width.offset(SCREEN_WIDTH);
        make.height.offset(97*Proportion_HEIGHT);
    }];
    
    NSInteger dynamicWidth = (SCREEN_WIDTH - 25)/4;
    for (int i = 0; i <imageArray.count; i ++) {
        UIButton *dynamicButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [dynamicButton setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [dynamicButton setImage:[UIImage imageNamed:imageArray_HL[i]] forState:UIControlStateHighlighted];
        dynamicButton.tag = 10 +i;
        [dynamicButton addTarget:self action:@selector(clickDynamicButton:) forControlEvents:UIControlEventTouchUpInside];
        [dynamicView addSubview:dynamicButton];
        [dynamicButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5);
            make.left.offset((dynamicWidth +5)*i +5);
            make.width.offset(dynamicWidth);
            make.height.offset(dynamicWidth);
            
        }];
    }
    
}

- (void)clickDynamicButton:(UIButton *)sender
{
    NSInteger tg = sender.tag;
    if (tg ==10) {
        NSLog(@"1");
    }else if (tg == 11){
        NSLog(@"2");
    }else if (tg == 12){
        NSLog(@"3");
    }else if (tg == 13){
        
        HLToolSetViewController *toolVC = [HLToolSetViewController new];
        toolVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toolVC animated:YES];

        
    }
}

- (void)setTableView
{
    cellTitle=@[@"我的资料",@"照片",@"粉丝",@"关注",@"设置"];

    UITableView *tableView = [[UITableView alloc] init];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.scrollEnabled = NO;

    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dynamicView.mas_bottom);
        make.left.offset(0);
        make.width.offset(SCREEN_WIDTH);
        make.height.offset(264*Proportion_HEIGHT);
    }];
    
    [self hideExcessLine:tableView];
    
}

//去掉多余的线
-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellTitle.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44 * Proportion_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrderListCell";
    
    //自定义cell类有图片
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.textLabel.text = cellTitle[indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger cellindex = indexPath.row;
    switch (cellindex) {
        case 0:
            [self clickMySet];
            
            break;
        case 1:
            [self historyProject];
            
            break;
        case 2:
            [self healthData];
            
            break;
        case 3:
            [self collection];
            
            
            break;
        case 4:
            [self setUp];
            
            break;
            

        default:
            break;
    }
    
    NSLog(@"你点击了我");
    
}

//编辑个人资料
- (void)clickMySet
{
    HLInformationSetController *set=[HLInformationSetController new];
    set.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:set animated:YES];
    
}

//历史计划
-(void)historyProject{

}

//健康数据
-(void)healthData{
    
}

//收藏
-(void)collection
{
    
}

//设置
- (void)setUp
{
    HLSetUpViewController *setUpController = [[HLSetUpViewController alloc] init];
    setUpController.hidesBottomBarWhenPushed= YES;
    [self.navigationController pushViewController:setUpController animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
