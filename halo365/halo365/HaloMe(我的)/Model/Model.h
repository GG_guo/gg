//
//  Model.h
//  MyShow
//
//  Created by 李鹏辉 on 16/5/5.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

@property(nonatomic, copy) NSString *avatar;//头像图片地址
@property(nonatomic, copy) NSString *nickname;//名字
@property(nonatomic, copy) NSString *gender;//性别
@property(nonatomic, copy) NSString *birth;//出身日期
@property(nonatomic, copy) NSString *province;//省份
@property(nonatomic, copy) NSString *city;//市
@property(nonatomic, copy) NSString *invitecode;//邀请码
@property(nonatomic, copy) NSString *height;//身高
@property(nonatomic, copy) NSString *weight;//体重
@property(nonatomic, copy) NSString *goal;//健身目标:fat减脂，body塑身，muscle增肌
@property(nonatomic, copy) NSString *basis;// '健身基础 1初级 2中级 3高级

-(id)initWithDic:(NSDictionary*)dic;


@end
