//
//  Model.m
//  MyShow
//
//  Created by 李鹏辉 on 16/5/5.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "Model.h"

@implementation Model

-(id)initWithDic:(NSDictionary*)dic
{
    self=[super init];
    if (self) {
        
        self.avatar = dic[@"avatar"];
        self.nickname = dic[@"nickname"];
        self.gender = dic[@"gender"];
        self.birth = dic[@"birth"];
        self.province = dic[@"province"];
        self.city = dic[@"city"];
        self.invitecode = dic[@"invitecode"];
        self.height = dic[@"height"];
        self.weight = dic[@"weight"];
        self.goal = dic[@"goal"];
        self.basis = dic[@"basis"];

    }
    return self;
}
@end
