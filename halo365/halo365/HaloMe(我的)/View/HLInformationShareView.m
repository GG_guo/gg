//
//  HLInformationShareView.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//分享视图

#import "HLInformationShareView.h"
#define Button_Width  50

@interface HLInformationShareView ()

@property (nonatomic , strong)NSArray  *nameArr;
@property (nonatomic , strong)NSArray  *iconArr;
@property (nonatomic , strong)UILabel  *title;

@end

@implementation HLInformationShareView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        _nameArr = [[NSArray alloc]init];
        _iconArr = [[NSArray alloc]init];
        
        _nameArr = @[@"微信",@"朋友圈",@"QQ",@"空间"];
        
        _iconArr = @[@"logo_wechat",@"logo_wechatmoments",@"logo_qq",@"logo_qzone"];
        NSLog(@"_namearr ====== %@",_nameArr);
        
        for (int i = 0 ; i < _nameArr.count; i ++) {
            CGFloat margin   =  (SCREEN_WIDTH -Button_Width*_nameArr.count)/(_nameArr.count+1);
            
            UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [shareBtn setBackgroundImage:[UIImage imageNamed:_iconArr[i]] forState:UIControlStateNormal];
            shareBtn.tag = 6000+i;
            shareBtn.frame = CGRectMake(margin + (Button_Width+margin)*i, 40 , Button_Width, Button_Width);
            [shareBtn addTarget:self  action:@selector(didClickShareButonWithTag:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:shareBtn];
            
            UILabel *appName = [[UILabel alloc]initWithFrame:CGRectMake(margin + (Button_Width+margin)*i, 40+Button_Width+10, Button_Width, 20)];
            appName.font = [UIFont systemFontOfSize:14];
            appName.textColor =[UIColor blackColor];
            appName.textAlignment = NSTextAlignmentCenter;
            appName.text = _nameArr[i];
            
            [self addSubview:appName];
            
        }
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame = CGRectMake(0, self.frame.size.height-39, SCREEN_WIDTH, 40);
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        cancelBtn.layer.borderWidth=0.5;
        cancelBtn.layer.borderColor=[UIColor grayColor].CGColor;
        
        cancelBtn.clipsToBounds = YES;
        [cancelBtn addTarget:self action:@selector(didclickCancleBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelBtn];
        
    }
    
    
    return self;
    
    
}

- (void)didClickShareButonWithTag:(UIButton *)sender
{
    
    if ([self.delegate respondsToSelector:@selector(shareAppWithTag:)]) {
        [self.delegate shareAppWithTag:sender.tag];
    }
    
}


- (void)didclickCancleBtn
{
    if ([self.delegate respondsToSelector:@selector(didClickCancleButtonInBottomView)]) {
        [self.delegate didClickCancleButtonInBottomView];
    }
    
}


@end
