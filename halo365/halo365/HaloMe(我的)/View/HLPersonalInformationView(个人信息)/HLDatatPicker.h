//
//  HLHeightPicker.h
//  MyShow
//
//  Created by 李鹏辉 on 16/4/29.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLDataPickerAppearance.h"


@interface HLDatatPicker : UIView<UIPickerViewDataSource,UIPickerViewDelegate>


@property (nonatomic, strong, readonly)HLDataPickerAppearance* appearance;

@property (nonatomic,strong) NSMutableArray* dataSourceArray;//数据源

@property (nonatomic, strong) UILabel *headerViewLabel;

- (void)reloadAppearance;

- (void)show;
- (void)hidden;

@end
