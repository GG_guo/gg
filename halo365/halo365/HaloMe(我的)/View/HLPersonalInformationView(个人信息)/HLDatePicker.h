//
//  KSDatePicker.h
//  lianlian
//
//  Created by 李鹏辉 on 16/4/11.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLDataPickerAppearance.h"

@interface HLDatePicker : UIView

@property (nonatomic, strong, readonly)HLDataPickerAppearance* appearance;

- (void)reloadAppearance;

- (void)show;
- (void)hidden;

@end