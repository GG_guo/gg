//
//  HLHeightPicker.m
//  MyShow
//
//  Created by 李鹏辉 on 16/4/29.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLDatatPicker.h"



@implementation HLDatatPicker

{
    //    UIDatePicker*_datePicker;//时间选择器
    UILabel     *_headerViewLabel;
    UIButton    *_cancelBtn;//取消按钮
    UIButton    *_commitBtn;//确定按钮
    
    UIView      *_horizonLine;
    UIView      *_verticalLine;
    
    UIButton    *_maskViewButton;//背影按钮
    
    UIPickerView *_dataPickerView; //身高选择器
    
    //    NSArray *_heightArray;
    
}

- (instancetype)init
{
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

//数据初始化
- (void)commonInit
{
    {
        //背影按钮
        _maskViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    
    {
        
        _appearance = [HLDataPickerAppearance new];
    }
    
    {
        _dataPickerView = [[UIPickerView alloc] init];
        _dataPickerView.dataSource = self;
        _dataPickerView.delegate = self;
        [self addSubview:_dataPickerView];
    }
    
    {
        _dataSourceArray = [NSMutableArray array];
    }
    
    {
        _headerViewLabel = [[UILabel alloc] init];
        [self addSubview:_headerViewLabel];
    }
    
    {
        //取消按钮
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelBtn.tag = KSDatePickerButtonCancel;
        [self addSubview:_cancelBtn];
        [_cancelBtn addTarget:self action:@selector(footViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    {
        //确定按钮
        _commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _commitBtn.tag = KSDatePickerButtonCommit;
        [self addSubview:_commitBtn];
        [_commitBtn addTarget:self action:@selector(footViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    {
        //横线视图
        _horizonLine = [[UIView alloc] init];
        [self addSubview:_horizonLine];
        
        //垂线视图
        _verticalLine = [[UIView alloc] init];
        [self addSubview:_verticalLine];
    }
    
    [self reloadAppearance];
    
}

//加载数据
- (void)reloadAppearance
{
    {
        //整体背影按钮位置和颜色(黑色)
        _maskViewButton.frame = [UIScreen mainScreen].bounds;
        _maskViewButton.backgroundColor = _appearance.maskBackgroundColor;
    }
    
    {
        self.backgroundColor = _appearance.datePickerBackgroundColor;
        if (_appearance.radius > 0) {
            self.layer.cornerRadius = _appearance.radius;
            self.layer.masksToBounds = YES;
        }
    }
    
//    {
//        //        _datePicker.datePickerMode = _appearance.datePickerMode;
//        //        _datePicker.minimumDate = _appearance.minimumDate;
//        //        _datePicker.maximumDate = _appearance.maximumDate;
//        //        _datePicker.date = _appearance.currentDate;
//        for (int i = 100; i <= 240; i++) {
//            [_heightArray addObject:[NSString stringWithFormat:@"%d cm",i]];
//        }
//        //        _heightArray = @[@"1",@"2",@"3"];
//        //        _heightPickerView.heightArray = _appearance.dataSource;
//        //        [_heightPickerView reloadAllComponents];
//    }
    
    {
        //头部标签的显示
//        _headerViewLabel.text = @"请选择身高";
        _headerViewLabel.font = _appearance.headerTextFont;
        _headerViewLabel.textColor = _appearance.headerTextColor;
        _headerViewLabel.textAlignment = _appearance.headerTextAlignment;
        _headerViewLabel.backgroundColor = _appearance.headerBackgroundColor;
    }
    
    {
        //取消按钮的显示
        _cancelBtn.titleLabel.font = _appearance.cancelBtnFont;
        [_cancelBtn setBackgroundColor:_appearance.cancelBtnBackgroundColor];
        [_cancelBtn setTitle:_appearance.cancelBtnTitle forState:UIControlStateNormal];
        [_cancelBtn setTitleColor:_appearance.cancelBtnTitleColor forState:UIControlStateNormal];
    }
    
    {
        //确定按钮的显示
        _commitBtn.titleLabel.font = _appearance.commitBtnFont;
        [_commitBtn setBackgroundColor:_appearance.commitBtnBackgroundColor];
        [_commitBtn setTitle:_appearance.commitBtnTitle forState:UIControlStateNormal];
        [_commitBtn setTitleColor:_appearance.commitBtnTitleColor forState:UIControlStateNormal];
        
    }
    
    {
        _horizonLine.backgroundColor = _appearance.lineColor;
        
        _verticalLine.backgroundColor = _appearance.lineColor;
    }
}

//位置的设置
- (void)layoutSubviews
{
    CGFloat supWidth = self.frame.size.width;
    CGFloat supHeight = self.frame.size.height;
    
    {
        _dataPickerView.frame = CGRectMake(0, _appearance.headerHeight, supWidth, supHeight - _appearance.headerHeight - _appearance.buttonHeight);
    }
    
    {
        _headerViewLabel.frame = CGRectMake(0, 0, supWidth, _appearance.headerHeight);
    }
    
    {
        _cancelBtn.frame = CGRectMake(0 * supWidth / 2 ,supHeight - _appearance.buttonHeight , supWidth / 2, _appearance.buttonHeight);
        
        _commitBtn.frame = CGRectMake(1 * supWidth / 2, supHeight - _appearance.buttonHeight, supWidth / 2, _appearance.buttonHeight);
    }
    
    {
        _horizonLine.frame = CGRectMake(0, supHeight - _appearance.buttonHeight, supWidth, _appearance.lineWidth);
        
        _verticalLine.frame = CGRectMake(supWidth / 2., supHeight - _appearance.buttonHeight, _appearance.lineWidth, _appearance.buttonHeight);
    }
}

// 取消/确定被按钮点击
- (void)footViewButtonClick:(UIButton*)button
{
    NSString* str = [self pickerView:_dataPickerView titleForRow:[_dataPickerView selectedRowInComponent:0] forComponent:0];
    if (_appearance.heightCallBack) {
        _appearance.heightCallBack(self,str.self,button.tag);
    }
    
    [self hidden];
    
}


- (void)show
{
    [self reloadAppearance];
    
    [self animationWithView:self duration:0.3];
    _maskViewButton.alpha= 0;
    [UIView animateWithDuration:0.25 animations:^{
        _maskViewButton.alpha = 0.5;
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_maskViewButton];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.center = [UIApplication sharedApplication].keyWindow.center;
}

- (void)hidden
{   [_maskViewButton removeFromSuperview];
    [self removeFromSuperview];
}

- (void)animationWithView:(UIView *)view duration:(CFTimeInterval)duration{
    
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = duration;
    animation.removedOnCompletion = NO;
    
    animation.fillMode = kCAFillModeForwards;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    //    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 0.9)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    
    animation.values = values;
    animation.timingFunction = [CAMediaTimingFunction functionWithName: @"easeInEaseOut"];
    
    [view.layer addAnimation:animation forKey:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return _dataSourceArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [_dataSourceArray objectAtIndex:row];
    
}
@end
