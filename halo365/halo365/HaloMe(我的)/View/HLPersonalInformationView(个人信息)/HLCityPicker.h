//
//  HLCityPicker.h
//  MyShow
//
//  Created by 李鹏辉 on 16/5/10.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLDataPickerAppearance.h"

#define Key_DistrictSelectProvince          @"DistrictSelectProvince"
#define Key_DistrictSelectProvinceCode      @"DistrictSelectProvinceCode"
#define Key_DistrictSelectProvinceSubCode   @"DistrictSelectProvinceSubCode"
#define Key_DistrictSelectProvinceSub       @"DistrictSelectProvinceSub"
#define Key_DistrictSelectCityCode          @"DistrictSelectCityCode"
#define Key_DistrictSelectCity              @"DistrictSelectCity"

@interface HLCityPicker : UIView<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, copy) void(^ActionDistrictViewSelectBlock)(NSString *desStr,NSDictionary *selectDistrictDict);

@property (nonatomic, strong) UILabel *headerViewLabel;

@property (nonatomic, strong, readonly)HLDataPickerAppearance* appearance;

@property (nonatomic, strong) NSMutableArray* heightArray;//身高数据源

@property (nonatomic, strong) NSMutableArray *weightArray;//体重数据源

@property (nonatomic, strong) NSDictionary *districtDict;
//@property (nonatomic, strong) UILabel * titleLabel;


@property (nonatomic, strong) NSString *provinceStr;
@property (nonatomic, strong) NSString *cityStr;


@property (nonatomic, strong) NSArray *ProvinceArray;
@property (nonatomic, strong) NSArray *CityArray;


- (void)reloadAppearance;

- (void)show;
- (void)hidden;

@end
