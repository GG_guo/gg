//
//  HLCityPicker.m
//  MyShow
//
//  Created by 李鹏辉 on 16/5/10.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCityPicker.h"

#define Key_Division        @"Division"
#define Key_DivisionCode    @"DivisionCode"
#define Key_DivisionName    @"DivisionName"
#define Key_DivisionSub     @"DivisionSub"
#define Key_DivisionVersion @"DivisionVersion"
#define KDistrictSelectNotification     @"KDistrictSelectNotification"

#define KDistrictSelectDistrict         @"KDistrictSelectDistrict"

@implementation HLCityPicker
{
    UIButton    *_cancelBtn;//取消按钮
    UIButton    *_commitBtn;//确定按钮
    UIView      *_horizonLine;
    UIView      *_verticalLine;
    UIButton    *_maskViewButton;//背影按钮
    UIPickerView *_pickerView; //身高选择器
}

- (instancetype)init
{
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

//数据初始化
- (void)commonInit
{
    {
        //背影按钮
        _maskViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    
    {
        _appearance = [HLDataPickerAppearance new];
    }
    
    {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
        [self addSubview:_pickerView];
    }
    
    {
        _headerViewLabel = [[UILabel alloc] init];
        [self addSubview:_headerViewLabel];
    }
    
    {
        //取消按钮
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelBtn.tag = KSDatePickerButtonCancel;
        [self addSubview:_cancelBtn];
        [_cancelBtn addTarget:self action:@selector(footViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    {
        //确定按钮
        _commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _commitBtn.tag = KSDatePickerButtonCommit;
        [self addSubview:_commitBtn];
        [_commitBtn addTarget:self action:@selector(footViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    {
        //横线视图
        _horizonLine = [[UIView alloc] init];
        [self addSubview:_horizonLine];
        
        //垂线视图
        _verticalLine = [[UIView alloc] init];
        [self addSubview:_verticalLine];
    }
    
    [self reloadAppearance];
    
}

//加载数据
- (void)reloadAppearance
{
    {
        //整体背影按钮位置和颜色(黑色)
        _maskViewButton.frame = [UIScreen mainScreen].bounds;
        _maskViewButton.backgroundColor = _appearance.maskBackgroundColor;
    }
    
    {
        self.backgroundColor = _appearance.datePickerBackgroundColor;
        if (_appearance.radius > 0) {
            self.layer.cornerRadius = _appearance.radius;
            self.layer.masksToBounds = YES;
        }
    }
    
    {
        //头部显示
        _headerViewLabel.font = _appearance.headerTextFont;
        _headerViewLabel.textColor = _appearance.headerTextColor;
        _headerViewLabel.textAlignment = _appearance.headerTextAlignment;
        _headerViewLabel.backgroundColor = _appearance.headerBackgroundColor;
    }
    
    {
        //取消按钮的显示
        _cancelBtn.titleLabel.font = _appearance.cancelBtnFont;
        [_cancelBtn setBackgroundColor:_appearance.cancelBtnBackgroundColor];
        [_cancelBtn setTitle:_appearance.cancelBtnTitle forState:UIControlStateNormal];
        [_cancelBtn setTitleColor:_appearance.cancelBtnTitleColor forState:UIControlStateNormal];
    }
    
    {
        //确定按钮的显示
        _commitBtn.titleLabel.font = _appearance.commitBtnFont;
        [_commitBtn setBackgroundColor:_appearance.commitBtnBackgroundColor];
        [_commitBtn setTitle:_appearance.commitBtnTitle forState:UIControlStateNormal];
        [_commitBtn setTitleColor:_appearance.commitBtnTitleColor forState:UIControlStateNormal];
        
    }
    
    {
        self.districtDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"districtMGE" ofType:@"plist"]];
        self.ProvinceArray = self.districtDict[Key_Division];
    }
    
    {
        _horizonLine.backgroundColor = _appearance.lineColor;
        
        _verticalLine.backgroundColor = _appearance.lineColor;
    }
}

//位置的设置
- (void)layoutSubviews
{
    CGFloat supWidth = self.frame.size.width;
    CGFloat supHeight = self.frame.size.height;
    
    {
        _pickerView.frame = CGRectMake(0, _appearance.headerHeight, supWidth, supHeight - _appearance.headerHeight - _appearance.buttonHeight);
    }
    
    {
        _headerViewLabel.frame = CGRectMake(0, 0, supWidth, _appearance.headerHeight);
    }
    
    {
        _cancelBtn.frame = CGRectMake(0 * supWidth / 2 ,supHeight - _appearance.buttonHeight , supWidth / 2, _appearance.buttonHeight);
        
        _commitBtn.frame = CGRectMake(1 * supWidth / 2, supHeight - _appearance.buttonHeight, supWidth / 2, _appearance.buttonHeight);
    }
    
    {
        _horizonLine.frame = CGRectMake(0, supHeight - _appearance.buttonHeight, supWidth, _appearance.lineWidth);
        
        _verticalLine.frame = CGRectMake(supWidth / 2., supHeight - _appearance.buttonHeight, _appearance.lineWidth, _appearance.buttonHeight);
    }
}

// 取消/确定被按钮点击
- (void)footViewButtonClick:(UIButton*)button
{
    NSString * provinceCode = [self.ProvinceArray[[_pickerView selectedRowInComponent:0]] objectForKey:Key_DivisionCode];
    NSString * cityCode = [self.CityArray[[_pickerView selectedRowInComponent:1]] objectForKey:Key_DivisionCode];
    
    NSString *provinceName = [self.ProvinceArray[[_pickerView selectedRowInComponent:0]] objectForKey:Key_DivisionName];
    NSString *cityName = [self.CityArray[[_pickerView selectedRowInComponent:1]] objectForKey:Key_DivisionName];
    
    NSString *areaTitle = [NSString stringWithFormat:@"%@",provinceName];
    if (self.ActionDistrictViewSelectBlock) {
        self.ActionDistrictViewSelectBlock(areaTitle,@{
                                                       Key_DistrictSelectProvince:provinceName,
                                                       Key_DistrictSelectProvinceCode:provinceCode,
                                                       Key_DistrictSelectCity:cityName,
                                                       Key_DistrictSelectCityCode:cityCode,
                                                       
                                                       });
    }
    
    
    [self hidden];
    
}

//显示
- (void)show
{
    [self reloadAppearance];
    
    [self animationWithView:self duration:0.3];
    _maskViewButton.alpha= 0;
    [UIView animateWithDuration:0.25 animations:^{
        _maskViewButton.alpha = 0.5;
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_maskViewButton];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.center = [UIApplication sharedApplication].keyWindow.center;
}

//隐藏
- (void)hidden
{   [_maskViewButton removeFromSuperview];
    [self removeFromSuperview];
}

- (void)animationWithView:(UIView *)view duration:(CFTimeInterval)duration{
    
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = duration;
    animation.removedOnCompletion = NO;
    
    animation.fillMode = kCAFillModeForwards;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    //    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 0.9)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    
    animation.values = values;
    animation.timingFunction = [CAMediaTimingFunction functionWithName: @"easeInEaseOut"];
    
    [view.layer addAnimation:animation forKey:nil];
}


- (void)getSelectDistrictName
{
    NSDictionary *ProvinceDict = self.ProvinceArray[[_pickerView selectedRowInComponent:0]];
    self.provinceStr = [ProvinceDict objectForKey:Key_DivisionName];
    
    //*****
    
    NSArray *array = [ProvinceDict objectForKey:Key_DivisionSub];
    if ([_pickerView selectedRowInComponent:1] > array.count - 1) {
        return;
    }
    NSDictionary *CityDict = [[ProvinceDict objectForKey:Key_DivisionSub] objectAtIndex:[_pickerView selectedRowInComponent:1]];
    self.cityStr = [CityDict objectForKey:Key_DivisionName];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==0)
    {
        return self.ProvinceArray.count;
    }
    else
    {
        if ([pickerView selectedRowInComponent:0]==-1)
        {
            return 0;
        }
        else
        {
            NSDictionary *ProvinceDict = self.ProvinceArray[[_pickerView selectedRowInComponent:0]];
            self.CityArray = [ProvinceDict objectForKey:Key_DivisionSub];
            
            return [self.CityArray count];
        }
    }
}

#pragma mark Picker Delegate Methods

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = nil;
    if (component==0)
    {
        NSDictionary *ProvinceDict = self.ProvinceArray[row];
        title = [ProvinceDict objectForKey:Key_DivisionName];
        
    }
    else if(component ==1)
    {
        NSDictionary *CityDict = self.CityArray[row];
        title = [CityDict objectForKey:Key_DivisionName];
    }
    return title ? title : @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component==0)
    {
        [pickerView reloadComponent:1];
        
        [self getSelectDistrictName];
    }
    else if(component==1)
    {
        [self getSelectDistrictName];
    }
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 70.0;
}

@end
