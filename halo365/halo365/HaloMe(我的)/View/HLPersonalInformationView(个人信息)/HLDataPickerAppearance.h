//
//  KSDatePickerAppearance.h
//  lianlian
//
//  Created by 李鹏辉 on 16/4/11.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class HLDatePicker,HLDatatPicker;
typedef NS_ENUM(NSUInteger, KSDatePickerButtonType) {
    KSDatePickerButtonCancel = 9999,
    KSDatePickerButtonCommit,
};

typedef void (^KSDatePickerResult)(HLDatePicker* datePicker,NSDate* currentDate,KSDatePickerButtonType buttonType);

typedef void (^KSHeightPickerResult)(HLDatatPicker* datePicker,NSString* str,KSDatePickerButtonType buttonType);

@interface HLDataPickerAppearance : NSObject

//datePicker
@property (nonatomic, strong) UIColor           *datePickerBackgroundColor;
@property (nonatomic, assign) CGFloat           radius;
@property (nonatomic, strong) UIColor           *maskBackgroundColor;
@property (nonatomic, strong) NSDate            *minimumDate;
@property (nonatomic, strong) NSDate            *maximumDate;
@property (nonatomic, strong, readwrite) NSDate *currentDate;
@property (nonatomic, assign) UIDatePicker      *datePicker;

@property (nonatomic, assign) UIDatePickerMode  datePickerMode;

//headerView
@property (nonatomic, copy) NSString            *headerText;
@property (nonatomic, strong) UIFont            *headerTextFont;
@property (nonatomic, strong) UIColor           *headerTextColor;
@property (nonatomic, assign) NSTextAlignment   headerTextAlignment;
@property (nonatomic, strong) UIColor           *headerBackgroundColor;
@property (nonatomic, assign) CGFloat           headerHeight;

//cancelButton
@property (nonatomic, assign) CGFloat           buttonHeight;  //cancelBtn and commitBtn

@property (nonatomic, copy) NSString            *cancelBtnTitle;
@property (nonatomic, strong) UIFont            *cancelBtnFont;
@property (nonatomic, strong) UIColor           *cancelBtnTitleColor;
@property (nonatomic, strong) UIColor           *cancelBtnBackgroundColor;

//commitButton
@property (nonatomic, copy) NSString            *commitBtnTitle;
@property (nonatomic, strong) UIFont            *commitBtnFont;
@property (nonatomic, strong) UIColor           *commitBtnTitleColor;
@property (nonatomic, strong) UIColor           *commitBtnBackgroundColor;

//line
@property (nonatomic, strong) UIColor           *lineColor;
@property (nonatomic, assign) CGFloat           lineWidth;

@property (nonatomic, copy) KSDatePickerResult  resultCallBack;
@property (nonatomic, copy) KSHeightPickerResult  heightCallBack;

@end
