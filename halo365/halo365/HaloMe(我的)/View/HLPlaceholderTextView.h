//
//  HLPlaceholderTextView.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/19.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLPlaceholderTextView : UITextView

@property (nonatomic, strong) UILabel * placeHolderLabel;

@property (nonatomic, copy) NSString * placeholder;

@property (nonatomic, strong) UIColor * placeholderColor;

- (void)textChanged:(NSNotification * )notification;

@end
