//
//  HLPersonalShowView.m
//  halo365
//
//  Created by 李鹏辉 on 16/7/26.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLPersonalShowView.h"

@implementation HLPersonalShowView

+(instancetype)shareTimerWithFrame:(CGRect)frame
{
    static HLPersonalShowView *editProfileView=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        editProfileView=[[HLPersonalShowView alloc]initWithFrame:frame];
    });
    return editProfileView;
}

-(void)back{
    
    self.frame=self.firstFrame;
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.firstFrame =frame;
        
        [self initSubViews];
        
    }
    return self;
}

//
- (void)initSubViews
{
    _avatarImage = [[UIImageView alloc] init];
    [self addSubview:_avatarImage];
    [_avatarImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15*Proportion_WIDTH);
        make.top.offset(0);
        make.height.offset(100*Proportion_HEIGHT);
        make.width.offset(103*Proportion_WIDTH);
    }];
    
    _imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _imageButton.backgroundColor = [UIColor lightGrayColor];
    //    [_imageButton setImage:[UIImage imageNamed:@"图标"] forState:UIControlStateNormal];
    [_imageButton addTarget:self action:@selector(clickImageButton) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_imageButton];
    [_imageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15*Proportion_WIDTH);
        make.top.offset(0);
        make.height.offset(86*Proportion_HEIGHT);
        make.width.offset(86*Proportion_WIDTH);

    }];
    
//    //显示名字
//    _nameLable=[[UILabel alloc]init];
//    //    Namelable.textColor=[UIColor whiteColor];
//    //    _Namelable.text = @"华波";
//    [self addSubview:_nameLable];
//    [_nameLable mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.equalTo(_imageButton.mas_right).with.offset(15*Proportion_WIDTH);
//        make.top.equalTo(_imageButton.mas_top).with.offset(5*Proportion_HEIGHT);
//        make.height.offset(22*Proportion_HEIGHT);
//        make.width.offset(100*Proportion_WIDTH);
//        
//    }];
//    
    
    
    
//    //竖定义分割线
//    NSInteger dividerWidth=self.frame.size.width-100;
//    //    NSInteger width = self.frame.size.width;
//    for (NSInteger i=0; i<2; i++) {
//        UIImageView *verticalLine=[[UIImageView alloc]init];
//        verticalLine.backgroundColor=[UIColor redColor];
//        //        verticalLine.frame=CGRectMake(dividerWidth/3 * (i +1) +100, 40, 1, 30);
//        [self addSubview:verticalLine];
//        [verticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_imageButton.mas_right).with.offset((dividerWidth-2)/3 * (i +1));
//            make.top.equalTo(_imageButton.mas_top).with.offset(25*Proportion_HEIGHT);
//            make.width.offset(1);
//            make.height.offset(30*Proportion_HEIGHT);
//        }];
//    }
//    UIImageView *vertiaclLine1 = [[UIImageView alloc] init];
//    vertiaclLine1.backgroundColor = [UIColor redColor];
//    //    vertiaclLine1.frame =CGRectMake(dividerWidth/2 +100, 70, 1, 30);
//    [self addSubview:vertiaclLine1];
//    [vertiaclLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_imageButton.mas_right).with.offset(dividerWidth/2);
//        make.top.equalTo(_imageButton.mas_top).with.offset(55*Proportion_HEIGHT);
//        make.width.offset(1);
//        make.height.offset(30*Proportion_HEIGHT);
//    }];
//    //横定义分割线
//    for (NSInteger i = 0; i<3; i++) {
//        UIImageView *crossLine=[[UIImageView alloc]init];
//        crossLine.backgroundColor=[UIColor redColor];
//        //        NSInteger dividerWidth=(self.frame.size.width-100)/3;
//        //        crossLine.frame=CGRectMake(100 ,40+30*i, dividerWidth, 1);
//        //        crossLine.frame = CGRectMake(0, 200+40*i + 0.5, self.frame.size.width, 1);
//        [self addSubview:crossLine];
//        [crossLine mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_imageButton.mas_right).with.offset(0);
//            make.top.equalTo(_imageButton.mas_top).with.offset(21+30*i*Proportion_HEIGHT);
//            make.width.offset(dividerWidth);
//            make.height.offset(1);
//        }];
//    }
//    
    
//    _heightLabel = [[UILabel alloc]init];
//    _heightLabel.textAlignment = NSTextAlignmentCenter;
//    _heightLabel.font = [UIFont systemFontOfSize:13];
//    [self addSubview:_heightLabel];
//    [_heightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_imageButton.mas_right).with.offset(0);
//        make.top.equalTo(_imageButton.mas_top).with.offset(25*Proportion_HEIGHT);
//        make.width.offset((dividerWidth-2)/3);
//        make.height.offset(30*Proportion_HEIGHT);
//    }];
//    
//    _weightLabel = [[UILabel alloc] init];
//    _weightLabel.textAlignment = NSTextAlignmentCenter;
//    _weightLabel.font = [UIFont systemFontOfSize:13];
//    [self addSubview:_weightLabel];
//    [_weightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_heightLabel.mas_right).with.offset(1);
//        make.top.equalTo(_imageButton.mas_top).with.offset(25*Proportion_HEIGHT);
//        make.width.offset((dividerWidth-2)/3);
//        make.height.offset(30*Proportion_HEIGHT);
//    }];
//    
//    _bodyLabel = [[UILabel alloc] init];
//    _bodyLabel.textAlignment = NSTextAlignmentCenter;
//    _bodyLabel.font = [UIFont systemFontOfSize:12];
//    _bodyLabel.textColor = [UIColor whiteColor];
//    [self addSubview:_bodyLabel];
//    [_bodyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_imageButton.mas_right).with.offset(0);
//        make.top.equalTo(_imageButton.mas_top).with.offset(55*Proportion_HEIGHT);
//        make.width.offset((dividerWidth-1)/2);
//        make.height.offset(30*Proportion_HEIGHT);
//    }];
//    
//    UILabel *weightLabel =[[UILabel alloc] init];
//    weightLabel.text = @"体重变化:";
//    weightLabel.textAlignment = NSTextAlignmentRight;
//    weightLabel.textColor = [UIColor whiteColor];
//    weightLabel.font = [UIFont systemFontOfSize:14];
//    [self addSubview:weightLabel];
//    [weightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_imageButton.mas_right).with.offset(dividerWidth/2+5);
//        make.top.equalTo(_imageButton.mas_top).with.offset(55*Proportion_HEIGHT);
//        make.width.offset(dividerWidth/2 - 70);
//        make.height.offset(30*Proportion_HEIGHT);
//    }];
    
}


- (void)clickImageButton
{
    if ([self.delegate respondsToSelector:@selector(didClickImageButtonInBottomView)]) {
        [self.delegate didClickImageButtonInBottomView];
        
    }
}

@end
