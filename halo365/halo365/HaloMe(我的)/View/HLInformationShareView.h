//
//  HLInformationShareView.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HLInformationShareViewDelegate <NSObject>

- (void)shareAppWithTag:(NSInteger)senderTag;
- (void)didClickCancleButtonInBottomView;

@end

@interface HLInformationShareView : UIView

@property (nonatomic , weak) id<HLInformationShareViewDelegate> delegate;

@end
