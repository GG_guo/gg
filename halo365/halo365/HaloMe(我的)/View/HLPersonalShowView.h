//
//  HLPersonalShowView.h
//  halo365
//
//  Created by 李鹏辉 on 16/7/26.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HLPersonalShowViewDelegate <NSObject>

- (void)didClickImageButtonInBottomView;

@end

@interface HLPersonalShowView : UIView

@property (nonatomic, strong) UIImageView *avatarImage;

@property (nonatomic, strong) UIButton *imageButton;

//显示名字
@property (nonatomic, strong) UILabel *nameLable;

@property (nonatomic, strong) UILabel *heightLabel;

@property (nonatomic, strong) UILabel *weightLabel;

@property (nonatomic, strong) UILabel *bodyLabel;

@property (nonatomic) CGRect firstFrame;

@property (nonatomic , weak) id<HLPersonalShowViewDelegate> delegate;

+(instancetype)shareTimerWithFrame:(CGRect)frame;

-(void)back;



@end
