//
//  HLPlanCell.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLPlanModel.h"

@interface HLPlanCell : UITableViewCell

//-(void)setCellModel:(id)obj;

@property(nonatomic,strong)HLPlanModel *model;

@end
