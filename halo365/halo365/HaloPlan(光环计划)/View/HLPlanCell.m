//
//  HLPlanCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLPlanCell.h"

@interface HLPlanCell()

@property (nonatomic, strong)  UIImageView *thumb;
@property (nonatomic, strong)  UILabel *nickLabel;
//@property (weak, nonatomic)  UIImageView *avatar;
@property (nonatomic, strong)  UILabel *titleLabel;
@property (nonatomic, strong) UILabel *parameterLabel;

@end

@implementation HLPlanCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self createCellView];
    }
    return self;
}


- (void)createCellView{
    self.backgroundColor = [UIColor whiteColor];
    //图片
    _thumb = [[UIImageView alloc] init];
    //    imgView.image = [UIImage imageNamed:model.thumb];
    
    _thumb.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:_thumb];
    [_thumb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.left.offset(5);
        make.bottom.offset(-5);
        //            make.right.offset(-5);
        make.width.offset(SCREEN_WIDTH-10);
//        make.height.offset((SCREEN_HEIGHT-10)/2-20);
    }];
    
    //主题
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:20];
    _titleLabel.textColor = [UIColor whiteColor];
    [_thumb addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_thumb.mas_top).offset(150);
        make.left.equalTo(_thumb.mas_left).offset((SCREEN_WIDTH-10)/2-70);
        make.height.offset(20);
    }];
//
    //难度系数
    _nickLabel = [[UILabel alloc] init];
    
    _nickLabel.font = [UIFont systemFontOfSize:15];
    _nickLabel.textColor = [UIColor whiteColor];
    _nickLabel.numberOfLines = 0;
    [self.contentView addSubview:_nickLabel];
    [_nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_titleLabel.mas_bottom).offset(0);
//        make.left.equalTo(_thumb.mas_right).offset(5);
//        make.right.offset(-10);
        make.bottom.offset(-20);
        make.left.offset(20);
        make.height.offset(40);
    }];
    
    
    //难度系数的参数
    
    _parameterLabel = [[UILabel alloc] init];
    _parameterLabel.font = [UIFont systemFontOfSize:15];
    _parameterLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_parameterLabel];
    [_parameterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_nickLabel.mas_right);
        make.bottom.equalTo(_nickLabel.mas_bottom);
        make.height.offset(40);
    }];
    
    
}

//- (void)setCellModel:(HLPlanModel *)obj
-(void)setModel:(HLPlanModel *)obj
{
    _thumb.image = [UIImage imageNamed:obj.thumb];
//    [self.thumb sd_setImageWithURL:[NSURL URLWithString:obj.thumb]];

    
    _titleLabel.text = obj.title;
    
    _nickLabel.text = obj.nick;
    
    _parameterLabel.text = obj.parameter;
}

@end
