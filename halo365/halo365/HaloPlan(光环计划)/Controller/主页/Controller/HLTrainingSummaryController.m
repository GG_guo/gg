//
//  HLTrainingSummaryController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLTrainingSummaryController.h"
#import "HLCoachVideoCell.h"
#import "HLCoachVideoModel.h"
#import "HLCoachOverviewCell.h"
#import "HLCoachOverviewModel.h"

@interface HLTrainingSummaryController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,strong) NSMutableArray *trainingArray;
@property (nonatomic ,strong) NSMutableArray *overviewArray;
@end

@implementation HLTrainingSummaryController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"训练概述";
    
    [self reloadCollectionView];
    
    [self registerCell];
    
    [self httpRequest];
    [self hideExcessLine:_tableView];
}

- (void)reloadCollectionView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    
    [self.view addSubview:_tableView];
    
}

- (void)registerCell{
    
    [_tableView registerClass:[HLCoachVideoCell class] forCellReuseIdentifier:@"HLCoachVideoCell"];
    [_tableView registerClass:[HLCoachOverviewCell class] forCellReuseIdentifier:@"HLCoachOverviewCell"];
    
}

- (void)httpRequest{
    
    _trainingArray = [NSMutableArray array];
    NSArray *arraynutrition = @[@"video-play"];
    NSArray *arrayVideo = @[@"华波2.jpg"];
    for (int j = 0; j <arraynutrition.count; j++) {
        HLCoachVideoModel *videoModel = [[HLCoachVideoModel alloc] init];
        videoModel.imageView =arrayVideo[j];
        videoModel.videoImage = arraynutrition[j];
        [_trainingArray addObject:videoModel];
    }
    
    _overviewArray = [NSMutableArray array];
    NSArray *arrayOverview = @[@"健康是重要的财富，在这里，华波会和你分享他追求健康的过程和计划，你将学会如何健身，如何饮食，准备好开启不一样的人生吧！                                                                在这里你将看到队长华波的12周训练计划，你可以学习这个计划并结合自己的生活制定出一套适合于你的自己健身方案，从此打开你的健身之路。"];
    for (int i =0; i<arrayOverview.count; i ++) {
        HLCoachOverviewModel *overviewModel = [[HLCoachOverviewModel alloc] init];
        overviewModel.overviewStr = arrayOverview[i];
        [_overviewArray addObject:overviewModel];
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0) {
        return _trainingArray.count;
        
    }
    return _overviewArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section== 0) {
        return (SCREEN_HEIGHT-10)/4-20;
        
    }
    
    return ((SCREEN_HEIGHT-20)/3-10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        HLCoachVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachVideoCell"];
        cell.model = _trainingArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    HLCoachOverviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachOverviewCell"];
    cell.model = _overviewArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
