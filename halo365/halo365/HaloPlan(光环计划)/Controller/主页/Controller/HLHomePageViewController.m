//
//  HLHomePageViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLHomePageViewController.h"
#import "HLCoachImageModel.h"
#import "HLCoachImageCell.h"
#import "HLCoachVideoCell.h"
#import "HLCoachVideoModel.h"
#import "HLCoachOverviewCell.h"
#import "HLCoachOverviewModel.h"
#import "HLCoachOverviewListModel.h"
#import "HLCoachOverviewListCell.h"
#import "HLCoachUserModel.h"
#import "HLCoachUserCell.h"
//#import "HLPlanTableViewController.h"
#import "HLNutritionSummaryController.h"
#import "HLTrainingSummaryController.h"

#import "HLCoachListModel.h"
#import "HLCoachListCell.h"
#import "HLTrainingViewController.h"

@interface HLHomePageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic , strong) UITableView *tableView;

@property(nonatomic , strong) NSMutableArray *imageArray;

@property(nonatomic , strong) NSMutableArray *videoArray;

@property(nonatomic , strong) NSMutableArray *overviewArray;

@property(nonatomic ,strong) NSMutableArray *listArray;

//@property(nonatomic, strong) NSMutableArray *wordArray;

@property(nonatomic, strong)NSMutableArray *userArray;

@end

@implementation HLHomePageViewController

#pragma mark 懒加载存放数据的数组


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    
    [self reloadCollectionView];
    
    [self registerCell];
    
    [self httpRequest];
    
}

- (void)reloadCollectionView{
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"返回" highIcon:@"返回" target:self action:@selector(clickSearch)];
    
    _tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //    _tableView.scrollEnabled = NO;
    [self.view addSubview:_tableView];
    
}
//注册Cell
- (void)registerCell
{
    //    [_tableView registerClass:[HLCoachImageCell class] forCellReuseIdentifier:@"HLCoachImageCell"];
    
    [_tableView registerClass:[HLCoachListCell class] forCellReuseIdentifier:@"HLCoachListCell"];
    
}

- (void)httpRequest{
    
    UIButton *footButton = [UIButton buttonWithType:UIButtonTypeCustom];
    footButton.frame = CGRectMake(0, 0, 0, 60);
    [footButton setTitle:@"跟我一起训练吧" forState:UIControlStateNormal];
    [footButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [footButton addTarget:self action:@selector(clickFootButton) forControlEvents:UIControlEventTouchUpInside];
    _tableView.tableFooterView = footButton;
    
    //    _imageArray = [NSMutableArray array];
    //
    //    NSArray *arrayImage = @[@"华波7.jpg"];
    //
    //    for (int i = 0; i <arrayImage.count; i++) {
    //        HLCoachImageModel *imageModel = [[HLCoachImageModel alloc] init];
    //        imageModel.imageThumb = arrayImage[i];
    //        [_imageArray addObject:imageModel];
    //
    //    }
    _imageArray = [NSMutableArray array];
    
    NSArray *arrayImage = @[@"华波7.jpg"];
    for (int i = 0 ; i < arrayImage.count; i++) {
        HLCoachListModel *model = [[HLCoachListModel alloc] init];
        model.imageView = arrayImage[i];
        [_imageArray addObject:model];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    NSArray *imageArray = @[@"华波6.jpg"];
    NSArray *videoArray = @[@"video-play"];
    NSArray *videoStr = @[@"健康是重要的财富，在这里，华波会和你分享他追求健康的过程和计划，你将学会如何健身，如何饮食，准备好开启不一样的人生吧！                                                在这里你将看到队长华波的12周训练计划，你可以学习这个计划并结合自己的生活制定出一套适合于你的自己健身方案，从此打开你的健身之路。"];
    
    
    for (int i = 0 ; i < imageArray.count; i ++) {
        HLCoachListModel *model = [[HLCoachListModel alloc] init];
        model.videoImage = videoArray[i];
        model.imageView = imageArray[i];
        model.vidoStr = videoStr[i];
        [array addObject:model];
    }
    
    NSMutableArray *array1 = [NSMutableArray array];
    NSArray *imageArray1 = @[@"华波3.jpg"];
    NSArray *videoArray1 = @[@"video-play"];
    NSArray *videoStr1 =  @[@"健康是重要的财富，在这里，华波会和你分享他追求健康的过程和计划，你将学会如何健身，如何饮食，准备好开启不一样的人生吧！                                                                在这里你将看到队长华波的12周训练计划，你可以学习这个计划并结合自己的生活制定出一套适合于你的自己健身方案，从此打开你的健身之路。"];
    
    for (int i = 0; i < imageArray1.count; i ++) {
        HLCoachListModel *model = [[HLCoachListModel alloc] init];
        model.videoImage = videoArray1[i];
        model.imageView = imageArray1[i];
        model.vidoStr = videoStr1[i];
        [array1 addObject:model];
    }
    
    NSMutableArray *array2 = [NSMutableArray array];
    NSArray *imageArray2 = @[@"11.jpg"];
    NSArray *videoArray2 = @[@"video-play"];
    NSArray *videoStr2 = @[@"营养元素是能提供生长发育维持生命和进行生产的各种正常生理活动所需要的元素或化合物。                                                                                                  人体需要很多的营养和微量元素，一般我们可以从食物中合理的获取，我们需要的营养包括碳水化合物、脂肪、蛋白质、维生素、矿物质和水六大类，但是不是每一种食物都是具备这些营养元素的，所以我们要合理的进行食物搭配食用。                                这些营养成分可以分为构成物质、能源物质和调节物质三部分。蛋白质、 矿物质和水是构成物质，碳水化合物和脂肪是能源物质，维生素是调节物质。"];
    
    for (int i = 0; i < imageArray2.count; i ++) {
        HLCoachListModel *model = [[HLCoachListModel alloc] init];
        model.videoImage = videoArray2[i];
        model.imageView = imageArray2[i];
        model.vidoStr = videoStr2[i];
        [array2 addObject:model];
    }
    
    //    NSMutableArray *array3 = [NSMutableArray array];
    //    NSArray *videoStr3 = @[@"跟随此计划"];
    //    NSArray *imageArray3 = @[@""];
    //    NSArray *videoArray3 = @[@""];
    //    for (int i = 0; i < videoStr3.count; i ++) {
    //        HLCoachListModel *model = [[HLCoachListModel alloc] init];
    //        model.videoImage = videoArray3[i];
    //        model.imageView = imageArray3[i];
    //        model.vidoStr = videoStr3[i];
    //        [array2 addObject:model];
    //    }
    
    _listArray = [[NSMutableArray alloc] initWithObjects:_imageArray,array,array1,array2,nil];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  _listArray.count  ;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    NSLog(@"aa==%ld",(long)section);
    
    return [[_listArray objectAtIndex:section ] count];
    
}

//设置标题高度
- (CGFloat)tableView:(UITableView *)tableView heightForHederInSection:(NSInteger)section
{
    if (section <= 1 ) {
        return 0;
    }
    return 20;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return @"训练概述";
    } else if (section == 3){
        return @"营养概述";
    }
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return (SCREEN_HEIGHT-10)/4-25;
        
    }
    //    else if (indexPath.section == _listArray.count -1){
    //        return 120;
    //
    //    }
    return ((SCREEN_HEIGHT-10)/4-30 +170);
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (indexPath.section == 0) {
    //        HLCoachImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachImageCell"];
    //        cell.model = _imageArray[indexPath.row];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //
    //        return cell;
    //    }
    //    else if (indexPath.section == 1){
    //        HLCoachVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachVideoCell"];
    //        cell.model = _videoArray[indexPath.row];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //
    //        return cell;
    //    }else if (indexPath.section == 2){
    //        HLCoachOverviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachOverviewCell"];
    //        cell.model = _overviewArray[indexPath.row];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //
    //        return cell;
    //    }
    //    else if (indexPath.section == 2 +_listArray.count){
    //        HLCoachUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachUserCell"];
    //        cell.model = _userArray[indexPath.row];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        return cell;
    //
    //    }
    
    HLCoachListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachListCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.model = [[_listArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)clickFootButton
{
    NSLog(@"点击响应");
    HLTrainingViewController *trainingVC = [[HLTrainingViewController alloc] init];
    trainingVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:trainingVC animated:NO];
    
}

- (void)clickSearch{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
