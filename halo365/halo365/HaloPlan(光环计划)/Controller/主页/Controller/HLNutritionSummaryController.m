//
//  HLNutritionSummaryController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLNutritionSummaryController.h"
#import "HLCoachVideoCell.h"
#import "HLCoachVideoModel.h"
#import "HLCoachOverviewCell.h"
#import "HLCoachOverviewModel.h"
#import "HLCoachToolModel.h"
#import "HLCoachToolCell.h"
#import "HLToolSetViewController.h"

@interface HLNutritionSummaryController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *nutritionArray;
@property (nonatomic ,strong) NSMutableArray *overviewArray;
@property (nonatomic ,strong) NSMutableArray *toolArray;

@end

@implementation HLNutritionSummaryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadCollectionView];
    
    [self registerCell];
    
    [self httpRequest];
    [self hideExcessLine:_tableView];
}

- (void)reloadCollectionView
{
    
    self.title = @"营养概述";
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    
    [self.view addSubview:_tableView];
    
}

- (void)registerCell{
    
    [_tableView registerClass:[HLCoachVideoCell class] forCellReuseIdentifier:@"HLCoachVideoCell"];
    [_tableView registerClass:[HLCoachOverviewCell class] forCellReuseIdentifier:@"HLCoachOverviewCell"];
    [_tableView registerClass:[HLCoachToolCell class] forCellReuseIdentifier:@"HLCoachToolCell"];
    
}

- (void)httpRequest{
    
    _nutritionArray = [NSMutableArray array];
    NSArray *arraynutrition = @[@"video-play"];
    NSArray *arrayVideo = @[@"11.jpg"];
    for (int j = 0; j <arraynutrition.count; j++) {
        HLCoachVideoModel *videoModel = [[HLCoachVideoModel alloc] init];
        videoModel.imageView =arrayVideo[j];
        videoModel.videoImage = arraynutrition[j];
        [_nutritionArray addObject:videoModel];
    }
    
    _overviewArray = [NSMutableArray array];
    NSArray *arrayOverview = @[@"营养元素是能提供生长发育维持生命和进行生产的各种正常生理活动所需要的元素或化合物。                                                                                                  人体需要很多的营养和微量元素，一般我们可以从食物中合理的获取，我们需要的营养包括碳水化合物、脂肪、蛋白质、维生素、矿物质和水六大类，但是不是每一种食物都是具备这些营养元素的，所以我们要合理的进行食物搭配食用。                                这些营养成分可以分为构成物质、能源物质和调节物质三部分。蛋白质、 矿物质和水是构成物质，碳水化合物和脂肪是能源物质，维生素是调节物质。"];
    for (int i =0; i<arrayOverview.count; i ++) {
        HLCoachOverviewModel *overviewModel = [[HLCoachOverviewModel alloc] init];
        overviewModel.overviewStr = arrayOverview[i];
        [_overviewArray addObject:overviewModel];
    }
    
    _toolArray = [NSMutableArray array];
    NSArray *toolArray = @[@"工具功能"];
    
    for (int i = 0; i <toolArray.count; i++) {
        HLCoachToolModel *overviewModel = [[HLCoachToolModel alloc] init];
        overviewModel.toolStr = toolArray[i];
        [_toolArray addObject:overviewModel];
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0) {
        return _nutritionArray.count;
        
    }if (section == 1) {
        return _overviewArray.count;
        
    }
    return _toolArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section== 0) {
        return (SCREEN_HEIGHT-10)/4-20;
        
    }if (indexPath.section == 2) {
        return 60;
        
    }
    
    return ((SCREEN_HEIGHT-20)/3-10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        HLCoachVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachVideoCell"];
        cell.model = _nutritionArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }if (indexPath.section == 1) {
        HLCoachOverviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachOverviewCell"];
        cell.model = _overviewArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    HLCoachToolCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachToolCell"];
    cell.model = _toolArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        //        HLToolSetViewController *toolSetVC = [[HLToolSetViewController alloc] init];
        //        toolSetVC.hidesBottomBarWhenPushed = YES;
        //        [self.navigationController pushViewController:toolSetVC animated:YES];
        
        [self overviewTool];
    }
}

- (void)overviewTool
{
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@macronutrients.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
    
}

-(void)hideExcessLine:(UITableView *)tableView{
    
    UIView *view=[[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    [tableView setTableFooterView:view];
}

- (void)clickSearch{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
