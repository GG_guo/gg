//
//  HLHomeScroll.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLCoachImageModel : NSObject

@property (nonatomic , copy)NSString *imageThumb;

@end
