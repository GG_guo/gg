//
//  HLCoachUserModel.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLCoachUserModel : NSObject

@property (nonatomic , copy)NSString *userImage;
@property (nonatomic , copy)NSString *userStr;

@end
