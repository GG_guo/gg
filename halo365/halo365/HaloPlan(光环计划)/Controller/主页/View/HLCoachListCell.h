//
//  HLCoachListCell.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/20.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLCoachListModel.h"

@interface HLCoachListCell : UITableViewCell

@property (nonatomic, strong) HLCoachListModel *model;

@end
