//
//  HLCoachUserCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachUserCell.h"
@interface HLCoachUserCell()

@property (nonatomic, strong) UIImageView *userImage;

@property (nonatomic, strong) UILabel *userLabel;

@end

@implementation HLCoachUserCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self createCellView];
    }
    return self;
}


- (void)createCellView
{
    //图片
    _userImage = [[UIImageView alloc] init];
    _userImage.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:_userImage];
    [_userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.left.offset(5);
        make.width.offset(SCREEN_WIDTH-10);
        make.height.offset((SCREEN_HEIGHT-20)/3-10);
    }];
    
    _userLabel = [[UILabel alloc] init];
    _userLabel.font = [UIFont systemFontOfSize:20];
    _userLabel.textAlignment = NSTextAlignmentCenter;
    _userLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_userLabel];
    [_userLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userImage.mas_top).with.offset(((SCREEN_HEIGHT-10)/3-50)/2);
        make.left.equalTo(_userImage.mas_left);
        make.width.equalTo(_userImage.mas_width);
        make.height.offset(30);
    }];
    
}

- (void)setModel:(HLCoachUserModel *)model
{
    _userImage.image = [UIImage imageNamed:model.userImage];
    
    _userLabel.text = model.userStr;
}


@end
