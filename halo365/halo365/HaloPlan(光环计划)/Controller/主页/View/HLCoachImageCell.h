//
//  HLHomeScrollCell.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLCoachImageModel.h"
@interface HLCoachImageCell : UITableViewCell

@property(nonatomic, strong) HLCoachImageModel *model;

@end
