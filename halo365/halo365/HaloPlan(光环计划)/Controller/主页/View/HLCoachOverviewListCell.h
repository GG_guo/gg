//
//  HLOverviewListCell.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLCoachOverviewListModel.h"

@interface HLCoachOverviewListCell : UITableViewCell

@property (nonatomic ,strong) HLCoachOverviewListModel *model;

@end
