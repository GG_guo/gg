//
//  HLCoachToolCell.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLCoachToolModel.h"

@interface HLCoachToolCell : UITableViewCell

@property(nonatomic, strong) HLCoachToolModel *model;

@end
