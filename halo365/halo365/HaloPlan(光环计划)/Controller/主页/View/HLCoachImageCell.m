//
//  HLHomeScrollCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachImageCell.h"

@interface HLCoachImageCell()

@property (nonatomic, strong)  UIImageView *thumbImage;


@end


@implementation HLCoachImageCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self createCellView];
    }
    return self;
}


- (void)createCellView
{
    //图片
    _thumbImage = [[UIImageView alloc] init];    
    _thumbImage.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:_thumbImage];
    [_thumbImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.left.offset(5);
        make.bottom.offset(-5);
        make.width.offset(SCREEN_WIDTH-10);
    }];

}

- (void)setModel:(HLCoachImageModel *)model
{
    _thumbImage.image = [UIImage imageNamed:model.imageThumb];

}

@end

