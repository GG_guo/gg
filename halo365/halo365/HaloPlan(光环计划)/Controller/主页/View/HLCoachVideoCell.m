//
//  HLCoachVideoCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachVideoCell.h"

@interface HLCoachVideoCell ()

@property (nonatomic, strong) UIImageView *imageGround;

@property (nonatomic, strong) UIImageView *videoImage;

@end


@implementation HLCoachVideoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self createCellView];
    }
    return self;
}

- (void)createCellView
{
    _imageGround = [[UIImageView alloc] init];
    _imageGround.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_imageGround];
    [_imageGround mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.left.offset(5);
        make.height.offset((SCREEN_HEIGHT-10)/4-30);
        make.width.offset(SCREEN_WIDTH - 10);
    }];
    
    _videoImage = [[UIImageView alloc] init];
//    _videoImage.backgroundColor = []
    [self.contentView addSubview:_videoImage];
    [_videoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imageGround.mas_top).with.offset(((SCREEN_HEIGHT-10)/4-70)/2);
        make.left.equalTo(_imageGround.mas_left).with.offset((SCREEN_WIDTH - 60)/2);
        make.width.offset(50);
        make.height.offset(50);
    }];

}

- (void)setModel:(HLCoachVideoModel *)model
{
    
    _imageGround.image = [UIImage imageNamed:model.imageView];
    _videoImage.image = [UIImage imageNamed:model.videoImage];
    
}


@end
