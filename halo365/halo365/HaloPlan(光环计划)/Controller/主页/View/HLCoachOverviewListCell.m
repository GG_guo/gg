//
//  HLOverviewListCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachOverviewListCell.h"

@interface HLCoachOverviewListCell ()

@property (nonatomic , strong) UILabel *listLabel;

@end

@implementation HLCoachOverviewListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createCellView];
    }
    return self;
}

- (void)createCellView
{
    
    _listLabel = [[UILabel alloc] init];
    _listLabel.font = [UIFont systemFontOfSize:20];
    _listLabel.textColor = [UIColor blackColor];
    _listLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_listLabel];
    [_listLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(40);
        make.width.offset(SCREEN_WIDTH);
    }];
}

- (void)setModel:(HLCoachOverviewListModel *)model
{
    _listLabel.text = model.listStr;
}

@end
