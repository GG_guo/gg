//
//  HLCoachOverviewCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachOverviewCell.h"

@interface HLCoachOverviewCell()

@property (nonatomic , strong) UILabel *overviewTitlLabel;

@end

@implementation HLCoachOverviewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self createCellView];
    }
    return self;
}


- (void)createCellView{
//    self.backgroundColor = [UIColor redColor];
    _overviewTitlLabel = [[UILabel alloc] init];
    _overviewTitlLabel.font = [UIFont systemFontOfSize:16];
    _overviewTitlLabel.textColor = [UIColor blackColor];
    _overviewTitlLabel.numberOfLines = 0;
    [self.contentView addSubview:_overviewTitlLabel];

    [_overviewTitlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.left.offset(5);
        make.bottom.offset(-5);
        make.width.offset(SCREEN_WIDTH - 10);
    }];

}

- (void)setModel:(HLCoachOverviewModel *)model
{
    _overviewTitlLabel.text = model.overviewStr;
}

@end
