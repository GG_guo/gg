
//
//  HLCoachToolCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachToolCell.h"

@interface HLCoachToolCell()

@property (nonatomic , strong) UILabel *toolTitlLabel;

@end

@implementation HLCoachToolCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self createCellView];
    }
    return self;
}


- (void)createCellView{
    //    self.backgroundColor = [UIColor redColor];
    _toolTitlLabel = [[UILabel alloc] init];
    _toolTitlLabel.font = [UIFont systemFontOfSize:20];
    _toolTitlLabel.textColor = [UIColor blackColor];
//    _toolTitlLabel.numberOfLines = 0;
    _toolTitlLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_toolTitlLabel];
    
    [_toolTitlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(5);
        make.height.offset(60);
        make.width.offset(SCREEN_WIDTH - 10);
    }];
    
}

- (void)setModel:(HLCoachToolModel *)model
{
    _toolTitlLabel.text = model.toolStr;
}


@end
