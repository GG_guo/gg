//
//  HLCoachPlanViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/2.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCoachPlanViewController.h"
#import "HLHomePageViewController.h"
#import "HLTrainingViewController.h"
#import "HLDietViewController.h"
#import "HLBaseNavigationController.h"

#define kClassKey   @"rootVCClassString"
#define kTitleKey   @"title"
#define kImgKey     @"imageName"
#define kSelImgKey  @"selectedImageName"

#define Global_tintColor [UIColor colorWithRed:0 green:(190 / 255.0) blue:(12 / 255.0) alpha:1]
@interface HLCoachPlanViewController ()

@end

@implementation HLCoachPlanViewController

//- (void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBarHidden= YES;
//}
//
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self reloadData];
    
}

- (void)reloadData
{
    NSArray *childItemsArray = @[
                                 //                                 @{kClassKey  : @"HLHomeTabelViewController",
                                 //                                   kTitleKey  : @"首页",
                                 //                                   kImgKey    : @"tabbar_mainframe",
                                 //                                   kSelImgKey : @"tabbar_mainframeHL"},
                                 
                                 @{kClassKey  : @"HLHomePageViewController",
                                   kTitleKey  : @"主页",
                                   kImgKey    : @"homePage_default@2x",
                                   kSelImgKey : @"homePage_selected@2x"},
                                 
                                 //                                 @{kClassKey  : @"HLOngoingPlanViewController",
                                 //                                   kTitleKey  : @"正在进行",
                                 //                                   kImgKey    : @"tabbar_discover",
                                 //                                   kSelImgKey : @"tabbar_discoverHL"},
                                 
                                 @{kClassKey  : @"HLTrainingViewController",
                                   kTitleKey  : @"训练",
                                   kImgKey    : @"category_default@2x",
                                   kSelImgKey : @"category_selected@2x"},
                                 
                                 @{kClassKey  : @"HLDietViewController",
                                   kTitleKey  : @"饮食",
                                   kImgKey    : @"shopCart_default@2x",
                                   kSelImgKey : @"shopCart_selected@2x"} ];
    
    
    
    
    [childItemsArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        UIViewController *vc = [NSClassFromString(dict[kClassKey]) new];
        vc.title = dict[kTitleKey];
        
        HLBaseNavigationController *nav = [[HLBaseNavigationController alloc] initWithRootViewController:vc];
        //        nav.navigationItem.leftBarButtonItem =  [UIBarButtonItem itemWithIcon:@"nav_back_normal" highIcon:@"nav_back_select" target:self action:@selector(clickSearch)];
        
        UITabBarItem *item = nav.tabBarItem;
        item.title = dict[kTitleKey];
        
        item.image = [UIImage imageNamed:dict[kImgKey]];
        item.selectedImage = [[UIImage imageNamed:dict[kSelImgKey]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [item setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor orangeColor]} forState:UIControlStateSelected];
        [self addChildViewController:nav];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
