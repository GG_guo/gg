//
//  HLPlanViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/12.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLPlanViewController.h"
#import "HLPlanModel.h"
#import "HLPlanCell.h"
//#import "HLBaseCell.h"
#import "HLCoachPlanViewController.h"
#import "HLCustomCurriculumController.h"

@interface HLPlanViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *planTableView;

@property(nonatomic , strong) NSMutableArray  *formArray;

@property (nonatomic, strong)NSArray *scrollImage1;

@end

@implementation HLPlanViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    //改变 状态栏的颜色为白色
    return UIStatusBarStyleLightContent;
    //改变状态栏的颜色为黑色
    //    return UIStatusBarStyleDefault;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    // 右边按钮
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor purpleColor];
    [self.view addSubview:view];
//    [self.view bringSubviewToFront:navcBar];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.height.offset(64);
        make.right.offset(0);
    }];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"50x50小图查看"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"50x50小图查看选中"] forState:UIControlStateHighlighted];
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(28);
        make.right.offset(-15);
        make.width.offset(30);
        make.height.offset(28);
    }];
    
    
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"50x50小图查看" highIcon:@"50x50小图查看选中" target:self action:@selector(clickClock)];
    
    _planTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT -64) style:UITableViewStylePlain];
    _planTableView.dataSource = self;
    _planTableView.delegate = self;
    [self.view addSubview:_planTableView];
    
    
    [self configureRefresh];
    
    [self registerCell];
    
    [self httpRequest];

    
    
}
//打卡功能
- (void)clickClock
{
    
}

- (void)registerCell
{
    [_planTableView registerClass:[HLPlanCell class] forCellReuseIdentifier:@"HLPlanCell"];
    
}

- (void)httpRequest{
    
    _scrollImage1 = @[@"build1.jpg",@"build1.jpg",@"build2.jpg",@"build3.jpg",@"build4.jpg"];
    NSArray *scrollName1 = @[@"华波的健身计划",@"华波的健身计划",@"凌霄的健身计划",@"魏龙的健身计划",@"袁洋的健身计划"];
    NSArray *scrollNick = @[@"",@"训练难度:",@"训练难度:",@"训练难度:",@"训练难度:"];
    NSArray *scorllParameter = @[@"",@"8",@"6",@"6",@"6"];
    //    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithObjects:_scrollImage1,scrollName1,scrollNick,nil];
    
    _formArray = [NSMutableArray array];
    for (int i = 0; i < _scrollImage1.count; i++) {
        HLPlanModel *classMD = [[HLPlanModel alloc] init];
        classMD.thumb = _scrollImage1[i];
        classMD.title = scrollName1[i];
        classMD.nick = scrollNick[i];
        classMD.parameter = scorllParameter[i];
        [_formArray addObject:classMD];
        
    }
    
}

- (void)configureRefresh {
    
    //设置下拉刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        //此处数据接口请求 列表刷新判断 建议封装的afn方法 保持代码的简洁性
        
        [self.planTableView.header beginRefreshing];
        [self.planTableView.header endRefreshing];
        
        
    }];
    self.planTableView.header = header;
    //设置上拉加载更多
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        if (self.formArray.count == 0) return;
        
        // 此处数据接口请求 建议封装的afn方法 保持代码的简洁性
        [self.planTableView.footer endRefreshingWithNoMoreData];
        
        
    }];
    self.planTableView.footer = footer;
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _formArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == 0) {
//        return ((SCREEN_HEIGHT-20)/4-10);
//        
//    }
    return ((SCREEN_HEIGHT-20)/2-10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    static NSString *cellIdentifier = @"HLPlanCell";
    
    //    HLPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //    if (cell == nil) {
    //        cell = [[HLPlanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    //
    //        HLPlanModel *model = _formArray[indexPath.row];
    //        [cell setCellModel: model];
    //    }
    
    HLPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLPlanCell"];
    cell.model = _formArray[indexPath.row];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == 0) {
//        HLCustomCurriculumController *custom = [[HLCustomCurriculumController alloc] init];
//        custom.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:custom animated:YES];
//    }
    
    if (indexPath.row == 1 || indexPath.row == 0) {
        
        HLCoachPlanViewController  *coachVC = [[HLCoachPlanViewController alloc] init];
        coachVC.hidesBottomBarWhenPushed =YES;
        //        [self.navigationController pushViewController:coachVC animated:YES];
        coachVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self presentViewController:coachVC animated:YES completion:nil];
        
    }
}

@end
