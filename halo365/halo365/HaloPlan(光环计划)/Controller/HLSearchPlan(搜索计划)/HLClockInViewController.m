//
//  ViewController.m
//  打卡
//
//  Created by 李鹏辉 on 16/6/22.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLClockInViewController.h"
#import "HLPlaceholderTextView.h"
#import "PhotoCollectionViewCell.h"
#import "Masonry.h"

#define kTextBorderColor     RGBCOLOR(227,224,216)
#undef  RGBCOLOR
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define ZLColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

@interface HLClockInViewController ()<UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>

@property (nonatomic, strong) HLPlaceholderTextView * textView;

@property (nonatomic, strong) UICollectionView *collectionVC;

//发送按钮
@property (nonatomic, strong) UIButton * sendButton;

//文本视图
@property (nonatomic, strong) UIView *contentView;

//字数限制
@property (nonatomic, strong) UILabel *wordCountLabel;

//上传图片的个数
@property (nonatomic, strong) NSMutableArray *photoArrayM;

//上传图片的button
@property (nonatomic, strong) UIButton *photoBtn;

//回收键盘
@property (nonatomic, strong) UITextField *textField;

//分割线
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIView *lineView1;
@property (nonatomic, strong) UIView *lineView2;

//定位
@property (nonatomic, strong) UIButton *positionButton;
@property (nonatomic, strong) UIImageView *positionImage;

//提醒
@property (nonatomic, strong) UIButton *remindButton;
@property (nonatomic, strong) UILabel *remindLabel;

//公开、私密
@property (nonatomic, strong) UIButton *publicButton;
@property (nonatomic, strong) UIImageView *publicImage;
@property (nonatomic, strong) UILabel *publicLabel;

@end

@implementation HLClockInViewController

//懒加载数组
- (NSMutableArray *)photoArrayM{
    if (!_photoArrayM) {
        _photoArrayM = [NSMutableArray arrayWithCapacity:0];
    }
    return _photoArrayM;
}

//button的frame
-(void)viewWillAppear:(BOOL)animated{
    if (self.photoArrayM.count < 5) {
        
        [self.collectionVC reloadData];
        _contentView.frame = CGRectMake(0, 64, SCREEN_WIDTH ,SCREEN_HEIGHT - 64);

        self.photoBtn.frame = CGRectMake(10 * (self.photoArrayM.count + 1) + (self.contentView.frame.size.width - 60) / 5 * self.photoArrayM.count, 139, (self.contentView.frame.size.width - 60) / 5, (self.contentView.frame.size.width - 60) / 5 + 5);
        
    }else{
        [self.collectionVC reloadData];
        self.photoBtn.frame = CGRectMake(0, 0, 0, 0);
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"打卡";
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"打卡" highIcon:nil target:self action:@selector(clickSender)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"return" highIcon:nil target:self action:@selector(clickClock)];
    
    self.view.backgroundColor = [UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0f];
    self.contentView = [[UIView alloc]init];
    _contentView.backgroundColor = [UIColor whiteColor];
    _contentView.frame = CGRectMake(0, 64, self.view.frame.size.width, SCREEN_HEIGHT - 64);
    [self.view addSubview:_contentView];
//    self.navigationItem.title = @"意见反馈";
    
    //限制字数显示的
    self.wordCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.textView.frame.origin.x ,  self.textView.frame.size.height + 64 - 1, [UIScreen mainScreen].bounds.size.width , 20)];
    _wordCountLabel.font = [UIFont systemFontOfSize:14.f];
    _wordCountLabel.textColor = [UIColor lightGrayColor];
    self.wordCountLabel.text = @"0/300";
    self.wordCountLabel.backgroundColor = [UIColor whiteColor];
    self.wordCountLabel.textAlignment = NSTextAlignmentRight;
    
    //分割线
    _lineView = [[UIView alloc]initWithFrame:CGRectMake(self.textView.frame.origin.x,  self.textView.frame.size.height + 64 - 1 + 23, [UIScreen mainScreen].bounds.size.width , 1)];
    _lineView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:_lineView];
    
    [self.view addSubview:_wordCountLabel];
    
    [_contentView addSubview:self.textView];
    
    self.automaticallyAdjustsScrollViewInsets = NO;//写文本的时候，是否可以来回滑动功能

    //创建collectionView进行上传图片
    [self addCollectionViewPicture];
    
//    //提交信息的button
//    [self.view addSubview:self.sendButton];
    
    //上传图片的button
    self.photoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.photoBtn.frame = CGRectMake(10 , 106, (self.contentView.frame.size.width- 60) / 5, (self.contentView.frame.size.width- 60) / 5);
    [_photoBtn setImage:[UIImage imageNamed:@"2.4意见反馈_03(1)"] forState:UIControlStateNormal];
    //[_photoBtn setBackgroundColor:[UIColor redColor]];
    
    [_photoBtn addTarget:self action:@selector(picureUpload:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_photoBtn];

    [self.contentView addSubview:self.lineView1];
    [self.contentView addSubview:self.lineView2];
    
    [self setPosition];
    [self setRemind];
    [self setPublic];
    
    [self setCompleteTraining];
    [self setFinishEating];
    
}

//发送按钮
- (void)clickSender
{
    if (self.textView.text.length == 0) {
        UIAlertController *alertLength = [UIAlertController alertControllerWithTitle:@"提示" message:@"你输入的信息为空，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *suer = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alertLength addAction:suer];
        [self presentViewController:alertLength animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];

    }
//    HLPlanTableViewController *planVC = [[HLPlanTableViewController alloc] init];
//    planVC
}

- (void)clickClock
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"确定不发动态了" preferredStyle: UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //点击按钮的响应事件；
    }]];
    [self presentViewController:alert animated:true completion:nil];
}


//定位
- (void)setPosition
{

//    positionButton.backgroundColor = [UIColor blackColor];
//    [positionButton setImage:[UIImage imageNamed:@"未选中定位"] forState:UIControlStateNormal];
//    [positionButton setImage:[UIImage imageNamed:@"选中定位"] forState:UIControlStateHighlighted];
    _positionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_positionButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    _positionButton.tag = 200;
    [self.contentView addSubview:_positionButton];
    [_positionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_photoBtn.mas_bottom).offset(21);
        make.left.offset(0);
        make.width.offset((SCREEN_WIDTH - 2)/3);
        make.height.offset(40);

    }];
    
    _positionImage = [[UIImageView alloc] init];
    _positionImage.image = [UIImage imageNamed:@"未选中定位"];
    _positionImage.backgroundColor = [UIColor blackColor];
    [_positionButton addSubview:_positionImage];
    [_positionImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(11);
        make.left.offset(20);
        make.width.offset(18);
        make.height.offset(18);
    }];
    
    UILabel *positonLabel = [[UILabel alloc] init];
    positonLabel.text = @"位置";
    positonLabel.font = [UIFont systemFontOfSize:15];
    [_positionButton addSubview:positonLabel];
    [positonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_positionImage.mas_top).offset(1.5);
        make.left.equalTo(_positionImage.mas_right).offset(5);
        make.width.offset(36);
        make.height.offset(15);
    }];
}

//提醒谁看
- (void)setRemind
{
    _remindButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_remindButton];
    [_remindButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_positionButton.mas_top);
        make.left.equalTo(_positionButton.mas_right).offset(1);
        make.width.offset((SCREEN_WIDTH - 2)/3);
        make.height.offset(40);
        
    }];
    
    _remindLabel = [[UILabel alloc] init];
    _remindLabel.text = @"@";
    _remindLabel.font = [UIFont systemFontOfSize:18];
    _remindLabel.textColor = [UIColor grayColor];
    [_remindButton addSubview:_remindLabel];
    [_remindLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(11);
        make.left.offset(20);
        make.width.offset(18);
        make.height.offset(18);

    }];
    
    UILabel *remindLabel = [[UILabel alloc] init];
    remindLabel.font = [UIFont systemFontOfSize:15];
    remindLabel.text = @"提醒";
    [_remindButton addSubview:remindLabel];
    [remindLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_remindLabel.mas_top).offset(1.5);
        make.left.equalTo(_remindLabel.mas_right).offset(5);
        make.width.offset(36);
        make.height.offset(15);

    }];

}

//公开/私密
- (void)setPublic
{
    _publicButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [_publicButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    _publicButton.tag = 202;
    [self.contentView addSubview:_publicButton];
    [_publicButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_positionButton.mas_top);
        make.left.offset(((SCREEN_WIDTH - 2)/3+1) *2);
        make.width.offset((SCREEN_WIDTH - 2)/3);
        make.height.offset(40);
        
    }];
    
    _publicImage = [[UIImageView alloc] init];
    _publicImage.image = [UIImage imageNamed:@"公开"];
    [_publicButton addSubview:_publicImage];
    [_publicImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(11);
        make.left.offset(20);
        make.width.offset(18);
        make.height.offset(18);
    }];

    
    _publicLabel = [[UILabel alloc] init];
    _publicLabel.text = @"公开";
    _publicLabel.font = [UIFont systemFontOfSize:15];
    [_publicButton addSubview:_publicLabel];
    [_publicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_publicImage.mas_top).offset(1.5);
        make.left.equalTo(_publicImage.mas_right).offset(5);
        make.width.offset(54);
        make.height.offset(15);
    }];
}

- (void)clickButton:(UIButton *)sender
{
    if (sender.tag == 200) {
       
        if (sender.selected == NO) {
            sender.selected = YES;
            _positionImage.image = [UIImage imageNamed:@"选中定位"];
            
        }else
        {
            sender.selected = NO;
            _positionImage.image = [UIImage imageNamed:@"未选中定位"];
            
        }

    }else if (sender.tag == 202){
        
        if (sender.selected == NO) {
            sender.selected = YES;
            _publicImage.image = [UIImage imageNamed:@"私密"];
            _publicLabel.text  =@"不公开";
        }else
        {
            sender.selected = NO;
            _publicImage.image = [UIImage imageNamed:@"公开"];
            _publicLabel.text = @"公开";
            
        }
    }
    
    
}


//完成当天训练的功能显示
- (void)setCompleteTraining
{
    
    UIButton *trainingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [trainingButton setImage:[UIImage imageNamed:@"35x35无勾选框.png"] forState:UIControlStateNormal];
    [trainingButton setImage:[UIImage imageNamed:@"35x35有勾选框.png"] forState:UIControlStateSelected];
    [trainingButton addTarget:self action:@selector(clickSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:trainingButton];
    [trainingButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_photoBtn.mas_bottom).offset(72);
        make.left.offset(20);
        make.width.offset(18);
        make.height.offset(18);
    }];
    
    UILabel *completeLabel = [[UILabel alloc] init];
    completeLabel.text = @"已完成";
    completeLabel.font = [UIFont systemFontOfSize:18];
    [self.contentView addSubview:completeLabel];
    [completeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(trainingButton.mas_top);
        make.left.equalTo(trainingButton.mas_right).offset(5);
        make.width.offset(60);
        make.height.offset(18);
    }];
    
    UILabel *trainingLabel = [[UILabel alloc] init];
    trainingLabel.text = @"某教练，第某天，某部位训练";
    trainingLabel.font = [UIFont systemFontOfSize:18];
    [self.contentView addSubview:trainingLabel];
    [trainingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(completeLabel.mas_top);
        make.left.equalTo(completeLabel.mas_right).offset(5);
        make.width.offset(SCREEN_WIDTH - 108);
        make.height.offset(18);
    }];
    
    
}

//完成饮食计划的功能显示
- (void)setFinishEating
{
    UIButton *eatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [eatButton setImage:[UIImage imageNamed:@"35x35无勾选框.png"] forState:UIControlStateNormal];
    [eatButton setImage:[UIImage imageNamed:@"35x35有勾选框.png"] forState:UIControlStateSelected];
    [eatButton addTarget:self action:@selector(clickSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:eatButton];
    [eatButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_photoBtn.mas_bottom).offset(113);
        make.left.offset(20);
        make.width.offset(18);
        make.height.offset(18);
    }];
    
    UILabel *completeLabel = [[UILabel alloc] init];
    completeLabel.text = @"已完成";
    completeLabel.font = [UIFont systemFontOfSize:18];
    [self.contentView addSubview:completeLabel];
    [completeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(eatButton.mas_top);
        make.left.equalTo(eatButton.mas_right).offset(5);
        make.width.offset(60);
        make.height.offset(18);
    }];
    
    UILabel *eatingLabel = [[UILabel alloc] init];
    eatingLabel.text = @"某教练的饮食计划";
    eatingLabel.font = [UIFont systemFontOfSize:18];
    [self.contentView addSubview:eatingLabel];
    [eatingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(completeLabel.mas_top);
        make.left.equalTo(completeLabel.mas_right).offset(5);
        make.width.offset(SCREEN_WIDTH - 108);
        make.height.offset(18);
    }];

}

- (void)clickSelected:(UIButton *)sender
{
    if (sender.selected == NO) {
        sender.selected = YES;
    }else
    {
        sender.selected = NO;
    }
}

- (UIView *)lineView1
{
    
    if (!_lineView1) {
       
        for (int i = 0 ; i <4; i ++) {
            _lineView1 = [[UIView alloc]init];
            _lineView1.backgroundColor = [UIColor lightGrayColor];
            [self.view addSubview:_lineView1];
            [_lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_photoBtn.mas_bottom).offset(20 + 41*i);
                make.left.offset(0);
                make.width.offset(SCREEN_WIDTH);
                make.height.offset(1);
                
            }];
        }
        
    }
    return _lineView1;
}

- (UIView *)lineView2
{
    
    if (!_lineView2) {
        
        for (int i = 0 ; i <2; i ++) {
            _lineView2 = [[UIView alloc] init];
            _lineView2 = [[UIView alloc]init];
            _lineView2.backgroundColor = [UIColor lightGrayColor];
            [self.view addSubview:_lineView2];
            [_lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_photoBtn.mas_bottom).offset(20);
                make.left.offset(((SCREEN_WIDTH - 2)/3+1)*(i+1));
                make.width.offset(1);
                make.height.offset(40);
                
            }];
        }
        
    }
    return _lineView1;
}


///图片上传
-(void)picureUpload:(UIButton *)sender{
    
    UIImagePickerController *picker=[[UIImagePickerController alloc]init];
    picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    picker.allowsEditing=YES;
    picker.delegate=self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

//上传图片的协议与代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
    //    [self.btn setImage:image forState:UIControlStateNormal];
    [self.photoArrayM addObject:image];
    //选取完图片之后关闭视图
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)addCollectionViewPicture{
    //创建一种布局
    UICollectionViewFlowLayout *flowL = [[UICollectionViewFlowLayout alloc]init];
    //设置每一个item的大小
    flowL.itemSize = CGSizeMake((self.contentView.frame.size.width - 60) / 5 , (self.contentView.frame.size.width - 60) / 5 );
    flowL.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    //列
    flowL.minimumInteritemSpacing = 10;
    //行
    flowL.minimumLineSpacing = 10;
    //创建集合视图         _contentView.frame = CGRectMake(0, 20, self.view.frame.size.width, 300);

    self.collectionVC = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 145, self.contentView.frame.size.width, ([UIScreen mainScreen].bounds.size.width - 60) / 5 + 10) collectionViewLayout:flowL];
    _collectionVC.backgroundColor = [UIColor whiteColor];
    // NSLog(@"-----%f",([UIScreen mainScreen].bounds.size.width - 60) / 5);
    _collectionVC.delegate = self;
    _collectionVC.dataSource = self;
    //添加集合视图
    [self.contentView addSubview:_collectionVC];
    
    //注册对应的cell
    [_collectionVC registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
}

-(HLPlaceholderTextView *)textView{
    
    if (!_textView) {
        _textView = [[HLPlaceholderTextView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , 100)];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.delegate = self;
        _textView.font = [UIFont systemFontOfSize:14.f];
        _textView.textColor = [UIColor blackColor];
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.editable = YES;
        _textView.layer.cornerRadius = 4.0f;
        _textView.layer.borderColor = kTextBorderColor.CGColor;
        _textView.layer.borderWidth = 0.5;
        _textView.placeholderColor = RGBCOLOR(0x89, 0x89, 0x89);
        _textView.placeholder = @"说说你今天运动的感受吧~~";
        
        
    }
    
    return _textView;
}

////打卡按钮提交初始化
//- (UIButton *)sendButton{
//    
//    if (!_sendButton) {
//        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _sendButton.layer.cornerRadius = 2.0f;
//        _sendButton.frame = CGRectMake(20, 400, self.view.frame.size.width - 40, 40);
//        _sendButton.backgroundColor = [self colorWithRGBHex:0x60cdf8];
//        [_sendButton setTitle:@"提交" forState:UIControlStateNormal];
//        [_sendButton addTarget:self action:@selector(sendFeedBack) forControlEvents:UIControlEventTouchUpInside];
//    }
//    
//    return _sendButton;
//    
//}

- (UIColor *)colorWithRGBHex:(UInt32)hex
{
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0f];
}


- (void)sendFeedBack
{
    
}

//把回车键当做退出键盘的响应键  textView退出键盘的操作
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if ([@"\n" isEqualToString:text] == YES)
    {
        [textView resignFirstResponder];
        
        
        return NO;
    }
    
    return YES;
}

#pragma mark CollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_photoArrayM.count == 0) {
        return 0;
    }
    else{
        return _photoArrayM.count;
    }
}

//返回每一个cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.photoV.image = self.photoArrayM[indexPath.item];
    return cell;
}


//在这个地方计算输入的字数
- (void)textViewDidChange:(UITextView *)textView
{
    NSInteger wordCount = textView.text.length;
    self.wordCountLabel.text = [NSString stringWithFormat:@"%ld/300",(long)wordCount];
    [self wordLimit:textView];
}

#pragma mark 超过300字不能输入
-(BOOL)wordLimit:(UITextView *)text{
    if (text.text.length < 300) {
        NSLog(@"%ld",text.text.length);
        self.textView.editable = YES;
        
    }
    else{
        self.textView.editable = NO;
        
    }
    return nil;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_textView resignFirstResponder];
    [_textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
