//
//  HLCustomCurriculumController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/12.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCustomCurriculumController.h"
#import "HLCustomAgeController.h"

@interface HLCustomCurriculumController ()

@property (nonatomic, strong) UIButton *maleButton;
@property (nonatomic, strong) UIButton *femaleButton;

@end

@implementation HLCustomCurriculumController

//static NSInteger const hexRedColor = 3dc466;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"选择性别";
    
    [self reloadCollectionView];
    [self reloadNextButton];
}

- (void)reloadCollectionView
{
    
    _maleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _maleButton.backgroundColor = [UIColor redColor];
    [_maleButton setTitle:@"男" forState:UIControlStateNormal];
    [_maleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_maleButton addTarget:self action:@selector(clickmaleButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_maleButton];
    [_maleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset (100);
        make.left.offset(15);
        make.right.offset(-15);
        make.height.offset (0.096*SCREEN_HEIGHT);
        
    }];
    
    _femaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _femaleButton.backgroundColor =  ZLColor(61, 76, 102);
    [_femaleButton setTitle:@"女" forState:UIControlStateNormal];
    [_femaleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_femaleButton addTarget:self action:@selector(clickfemaleButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_femaleButton];
    [_femaleButton mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.equalTo(_maleButton.mas_bottom).offset(50);
        make.left.equalTo(_maleButton.mas_left);
        make.right.equalTo(_maleButton.mas_right);
        make.height.equalTo(_maleButton.mas_height);
        
    }];
}

- (void)clickmaleButton:(UIButton *)sender
{
    _maleButton.backgroundColor = [UIColor redColor];
    
    _femaleButton.backgroundColor = ZLColor(61, 76, 102);
   }

- (void)clickfemaleButton
{
    _maleButton.backgroundColor = ZLColor(61, 76, 102);
    
    _femaleButton.backgroundColor = [UIColor redColor];
}

- (void)reloadNextButton
{
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.backgroundColor = [UIColor redColor];
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    [nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(64);
    }];
}

- (void)clickNextButton
{
    HLCustomAgeController *ageVC = [[HLCustomAgeController alloc] init];
//    ageVC.hidesBottomBarWhenPushed
    [self.navigationController pushViewController:ageVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
