//
//  HLCustomAgeController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/12.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCustomAgeController.h"
#import "HLCustomHeightViewController.h"
#import "HLDatatPicker.h"

@interface HLCustomAgeController ()

@end

@implementation HLCustomAgeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    self.title = @"选择年龄";
    [self reloadCollectionView];
    [self reloadNextButton];
}

- (void)reloadCollectionView
{
    UIButton *ageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    ageButton.backgroundColor = ZLColor(61, 76, 102);
    [ageButton setTitle:@"23岁" forState:UIControlStateNormal];
    [ageButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ageButton addTarget:self action:@selector(clickAgeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:ageButton];
    [ageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset (100);
        make.left.offset(15);
        make.right.offset(-15);
        make.height.offset (0.096*SCREEN_HEIGHT);
        
    }];
    
}

- (void)reloadNextButton
{
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.backgroundColor = [UIColor redColor];
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    [nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(64);
    }];
}

- (void)clickAgeButton:(UIButton *)sender
{
    NSLog(@"hhh");
    HLDatatPicker * picker = [[HLDatatPicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 40, 300)];
    picker.headerViewLabel.text =@"选择年龄";
    for (int i = 10; i <= 100; i++) {
        [picker.dataSourceArray addObject:[NSString stringWithFormat:@"%d 岁",i]];
    }

    //设置回调
    picker.appearance.heightCallBack = ^void(HLDatatPicker * datePicker,NSString* str,KSDatePickerButtonType buttonType){
        
        if (buttonType == KSDatePickerButtonCommit) {
            
            [sender setTitle:str forState:UIControlStateNormal];
            
        }
    };
    
    // 显示
    [picker show];
}

- (void)clickNextButton
{
    HLCustomHeightViewController *heightVC = [[HLCustomHeightViewController alloc] init];
    [self.navigationController pushViewController:heightVC animated:YES];
}

@end
