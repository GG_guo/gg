//
//  HLHeightViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/12.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLTargetViewController.h"
#import "HLCustomIntensityViewController.h"

@interface HLTargetViewController ()

@property (nonatomic, strong) UIButton *fatButton;//减脂

@property (nonatomic, strong) UIButton *muscleButton;//增肌

@property (nonatomic, strong) UIButton *bodyButton;//转型

@end

@implementation HLTargetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"选择目标";
    
    [self reloadCollectionView];
    [self reloadNextButton];
}

- (void)reloadCollectionView
{
    
    _fatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _fatButton.backgroundColor = [UIColor redColor];
    [_fatButton setTitle:@"减脂" forState:UIControlStateNormal];
    [_fatButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_fatButton addTarget:self action:@selector(clickFat) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_fatButton];
    [_fatButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset (100);
        make.left.offset(15);
        make.right.offset(-15);
        make.height.offset (0.096*SCREEN_HEIGHT);
        
    }];
    
    _muscleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _muscleButton.backgroundColor = ZLColor(61, 76, 102);
    [_muscleButton setTitle:@"增肌" forState:UIControlStateNormal];
    [_muscleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_muscleButton addTarget:self action:@selector(clickMuscle) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_muscleButton];
    [_muscleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_fatButton.mas_bottom).offset(50);
        make.left.equalTo(_fatButton.mas_left);
        make.right.equalTo(_fatButton.mas_right);
        make.height.equalTo(_fatButton.mas_height);
        
    }];
    
    _bodyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _bodyButton.backgroundColor = ZLColor(61, 76, 102);
    [_bodyButton setTitle:@"转型" forState:UIControlStateNormal];
    [_bodyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_bodyButton addTarget:self action:@selector(clickBody) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_bodyButton];
    [_bodyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_muscleButton.mas_bottom).offset(50);
        make.left.equalTo(_muscleButton.mas_left);
        make.right.equalTo(_muscleButton.mas_right);
        make.height.equalTo(_muscleButton.mas_height);
        
    }];
    
}

- (void)clickFat
{
    _fatButton.backgroundColor = [UIColor redColor];
    _muscleButton.backgroundColor = ZLColor(61, 76, 102);
    _bodyButton.backgroundColor = ZLColor(61, 76, 102);
    
}

- (void)clickMuscle
{
    _fatButton.backgroundColor = ZLColor(61, 76, 102);
    _muscleButton.backgroundColor = [UIColor redColor];
    _bodyButton.backgroundColor = ZLColor(61, 76, 102);

}

- (void)clickBody
{
    _fatButton.backgroundColor = ZLColor(61, 76, 102);
    _muscleButton.backgroundColor = ZLColor(61, 76, 102);
    _bodyButton.backgroundColor = [UIColor redColor];
}

- (void)reloadNextButton
{
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.backgroundColor = [UIColor redColor];
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    [nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(64);
    }];
}

- (void)clickNextButton
{
    HLCustomIntensityViewController *intensityVC = [[HLCustomIntensityViewController alloc] init];

    [self.navigationController pushViewController:intensityVC animated:YES];
}

@end
