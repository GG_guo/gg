//
//  HLCustomIntensityViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/12.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLCustomIntensityViewController.h"
#import "HLPlanViewController.h"

@interface HLCustomIntensityViewController ()

@property (nonatomic, strong) UIButton *primaryButton;

@property (nonatomic, strong) UIButton *intermediateButton;

@property (nonatomic, strong) UIButton *seniorButton;

@end

@implementation HLCustomIntensityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"选择目标";
    
    [self reloadCollectionView];
    [self reloadNextButton];
}

- (void)reloadCollectionView
{
    
    _primaryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _primaryButton.backgroundColor = [UIColor redColor];
    [_primaryButton setTitle:@"初级" forState:UIControlStateNormal];
    [_primaryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_primaryButton addTarget:self action:@selector(clickPrimary) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_primaryButton];
    [_primaryButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset (100);
        make.left.offset(15);
        make.right.offset(-15);
        make.height.offset (0.096*SCREEN_HEIGHT);
        
    }];
    
    _intermediateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _intermediateButton.backgroundColor = ZLColor(61, 76, 102);
    [_intermediateButton setTitle:@"中级" forState:UIControlStateNormal];
    [_intermediateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_intermediateButton addTarget:self action:@selector(clickIntermediate) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_intermediateButton];
    [_intermediateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_primaryButton.mas_bottom).offset(50);
        make.left.equalTo(_primaryButton.mas_left);
        make.right.equalTo(_primaryButton.mas_right);
        make.height.equalTo(_primaryButton.mas_height);
        
    }];
    
    _seniorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _seniorButton.backgroundColor = ZLColor(61, 76, 102);
    [_seniorButton setTitle:@"高级" forState:UIControlStateNormal];
    [_seniorButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_seniorButton addTarget:self action:@selector(clickSenior) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_seniorButton];
    [_seniorButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_intermediateButton.mas_bottom).offset(50);
        make.left.equalTo(_intermediateButton.mas_left);
        make.right.equalTo(_intermediateButton.mas_right);
        make.height.equalTo(_intermediateButton.mas_height);
        
    }];
}

- (void)clickPrimary
{
    _primaryButton.backgroundColor = [UIColor redColor];
    _intermediateButton.backgroundColor = ZLColor(61, 76, 102);
    _seniorButton.backgroundColor = ZLColor(61, 76, 102);
}

- (void)clickIntermediate
{
    _primaryButton.backgroundColor = ZLColor(61, 76, 102);
    _intermediateButton.backgroundColor = [UIColor redColor];
    _seniorButton.backgroundColor = ZLColor(61, 76, 102);
}

- (void)clickSenior
{
    _primaryButton.backgroundColor = ZLColor(61, 76, 102);
    _intermediateButton.backgroundColor = ZLColor(61, 76, 102);
    _seniorButton.backgroundColor = [UIColor redColor];
}

- (void)reloadNextButton
{
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.backgroundColor = [UIColor redColor];
    [nextButton setTitle:@"完成" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    [nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(64);
    }];
}

- (void)clickNextButton
{
//    HLPlanViewController *planVC = [[HLPlanViewController alloc] init];
//    planVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//
//    [self presentViewController:planVC animated:YES completion:nil];
//    [self.navigationController pushViewController:planVC animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
