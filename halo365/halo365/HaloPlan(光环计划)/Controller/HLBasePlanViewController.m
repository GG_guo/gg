//
//  HLPlanTableViewController.m
//  halo365
//
//  Created by 李鹏辉 on 16/7/21.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLBasePlanViewController.h"
#import "HLPlanModel.h"
#import "HLPlanCell.h"
#import "HLCoachPlanViewController.h"
#import "HLCustomCurriculumController.h"
#import "RAYNewFunctionGuideVC.h"
#import "HLClockInViewController.h"
@interface HLBasePlanViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *planTableView;

@property(nonatomic , strong) NSMutableArray  *formArray;

@property (nonatomic, strong) NSArray *scrollImage1;

@end

@implementation HLBasePlanViewController
//显示引导按钮
//- (void)viewDidAppear:(BOOL)animated{
//    [self makeFunctionGuide];
//
//}
- (void)makeFunctionGuide{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *firstComeInTeacherDetail = @"isFirstEnterHere";
    [user setBool:NO forKey:firstComeInTeacherDetail];
    
    if (![user boolForKey:firstComeInTeacherDetail]) {
        [user setBool:YES forKey:firstComeInTeacherDetail];
        [user synchronize];
        [self makeGuideView];
    }
    
}

- (void)makeGuideView{
    RAYNewFunctionGuideVC *vc = [[RAYNewFunctionGuideVC alloc]init];
    vc.titles = @[@"找到更合适于你的计划",@"将你的成果分享给朋友",];
    vc.frames = @[@"{{5,  20},{50,40}}",
                  @"{{300,20},{70,40}}",
                  ];
    
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNavigationItem];
    [self setInfrastructure];
   
}

- (void)setNavigationItem
{
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"navigationbar_addplan" highIcon:@"navigationbar_addplanHL" target:self action:@selector(clickLeftItem)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"navigationbar_clock" highIcon:@"navigationbar_clockHL" target:self action:@selector(clickRightItem)];
    
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = [UIImage imageNamed:@"halo365"];
    iconView.frame = CGRectMake((SCREEN_WIDTH - 101)/2, 9, 101, 53/2);
    [self.navigationController.navigationBar addSubview:iconView];
    
}

//查询计划
- (void)clickLeftItem{
    
}

//打卡功能

- (void)clickRightItem{
    
}

//基础设置
- (void)setInfrastructure
{
    _planTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _planTableView.dataSource = self;
    _planTableView.delegate = self;
    [self.view addSubview:_planTableView];
    
    //刷新数据
    [self configureRefresh];
    
    [self registerCell];
    
    //加载数据
    [self httpRequest];
}

- (void)configureRefresh {
    
    //设置下拉刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        //此处数据接口请求 列表刷新判断 建议封装的afn方法 保持代码的简洁性
        
        [self.planTableView.header beginRefreshing];
        [self.planTableView.header endRefreshing];
        
        
    }];
    self.planTableView.header = header;
    //设置上拉加载更多
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        if (self.formArray.count == 0) return;
        
        // 此处数据接口请求 建议封装的afn方法 保持代码的简洁性
        [self.planTableView.footer endRefreshingWithNoMoreData];
        
        
    }];
    self.planTableView.footer = footer;
}

- (void)registerCell
{
    [_planTableView registerClass:[HLPlanCell class] forCellReuseIdentifier:@"HLPlanCell"];
    
}

- (void)httpRequest{
    
    _scrollImage1 = @[@"华波1.jpg",@"build2.jpg",@"build3.jpg",@"build4.jpg"];
    NSArray *scrollName1 = @[@"华波的健身计划",@"凌霄的健身计划",@"魏龙的健身计划",@"袁洋的健身计划"];
    NSArray *scrollNick = @[@"训练难度:",@"训练难度:",@"训练难度:",@"训练难度:"];
    NSArray *scorllParameter = @[@"8",@"6",@"6",@"6"];
    //    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithObjects:_scrollImage1,scrollName1,scrollNick,nil];
    
    _formArray = [NSMutableArray array];
    for (int i = 0; i < _scrollImage1.count; i++) {
        HLPlanModel *classMD = [[HLPlanModel alloc] init];
        classMD.thumb = _scrollImage1[i];
        classMD.title = scrollName1[i];
        classMD.nick = scrollNick[i];
        classMD.parameter = scorllParameter[i];
        [_formArray addObject:classMD];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _formArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return ((SCREEN_HEIGHT-20)/2-10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HLPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLPlanCell"];
    cell.model = _formArray[indexPath.row];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        HLCoachPlanViewController  *coachVC = [[HLCoachPlanViewController alloc] init];
        coachVC.hidesBottomBarWhenPushed =YES;
        //        [self.navigationController pushViewController:coachVC animated:YES];
        coachVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self presentViewController:coachVC animated:YES completion:nil];
        
    }
}


@end
