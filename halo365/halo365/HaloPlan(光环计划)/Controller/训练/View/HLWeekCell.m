
//
//  HLWeekCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLWeekCell.h"

@interface HLWeekCell ()

@property (nonatomic, strong) UIImageView *weekImageView;

@property (nonatomic, strong) UILabel *weekTitleLabel;

@property (nonatomic, strong) UILabel *weekDetailLabel;


@end

@implementation HLWeekCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
        [self createCellView];

    }
    return self;
}

- (void)createCellView
{
    _weekImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:_weekImageView];
    [_weekImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.bottom.offset(-5);
        make.left.offset(5);
        make.width.offset(80);
    }];
    
    _weekTitleLabel = [[UILabel alloc] init];
    _weekTitleLabel.font = [UIFont systemFontOfSize:20];
    [self.contentView addSubview:_weekTitleLabel];
    [_weekTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_weekImageView.mas_top);
        make.left.equalTo(_weekImageView.mas_right).offset(5);
        make.width.offset (100);
        make.height.offset(20);
        
    }];
    
    
    _weekDetailLabel = [[UILabel alloc] init];
    _weekDetailLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_weekDetailLabel];
    [_weekDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo (_weekTitleLabel.mas_bottom).offset (5);
        make.left.equalTo(_weekTitleLabel.mas_left);
        make.width.offset(200);
        make.height.offset(20);
    }];
    
}

- (void)setModel:(HLWeekModel *)model
{
    _weekImageView.image = [UIImage imageNamed:model.weekImageStr];
    
    _weekTitleLabel.text = model.weekTitleStr;
    
    _weekDetailLabel.text = model.weekDetialStr;
}

@end
