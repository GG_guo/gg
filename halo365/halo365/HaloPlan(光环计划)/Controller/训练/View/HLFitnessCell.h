//
//  HLFitnessCell.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLFitnessModel.h"

@interface HLFitnessCell : UITableViewCell

@property (nonatomic, strong) HLFitnessModel *model;

@end
