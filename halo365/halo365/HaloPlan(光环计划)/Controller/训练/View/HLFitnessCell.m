//
//  HLFitnessCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLFitnessCell.h"

@interface HLFitnessCell()

@property (nonatomic, strong) UIImageView *numberImageView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *actionLabel;

@property (nonatomic, strong) UIImageView *videoImageView;

@property (nonatomic, strong) UIImageView *movementView;

@end

@implementation HLFitnessCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createCellView];
        
    }
    return self;
}

- (void)createCellView
{
    _numberImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:_numberImageView];
    [_numberImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(10);
//        make.bottom.offset(-5);
        make.left.offset(5);
        make.width.offset(30);
        make.height.offset(30);
    }];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:25];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_numberImageView.mas_top);
        make.left.equalTo(_numberImageView.mas_right).offset(10);
        make.width.offset (300);
        make.height.offset(25);
        
    }];
    
    
    _actionLabel = [[UILabel alloc] init];
    _actionLabel.font = [UIFont systemFontOfSize:15];
    _actionLabel.numberOfLines = 0;
//    _actionLabel.textAlignment = 
    [self.contentView addSubview:_actionLabel];
    [_actionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo (_titleLabel.mas_bottom).offset (5);
        make.left.equalTo(_titleLabel.mas_left);
        make.width.offset(200);
        make.height.offset(60);
    }];
    
    _movementView = [[UIImageView alloc] init];
    [self.contentView addSubview:_movementView];
    [_movementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.right.offset(-10);
        make.width.offset(90);
        make.bottom.offset(-5);
        
    }];
    
    
    _videoImageView = [[UIImageView alloc] init];
    _videoImageView.image = [UIImage imageNamed:@"video-play"];
    [self.contentView addSubview:_videoImageView];
    [_videoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_movementView.mas_top).offset(20);
        make.right.equalTo(_movementView.mas_right).offset(-20);
        make.width.offset(50);
        make.height.offset(50);
    }];
    
}

- (void)setModel:(HLFitnessModel *)model
{
    _numberImageView.image = [UIImage imageNamed:model.numberImageStr];
    
    _titleLabel.text = model.titleStr;
    
    _actionLabel.text = model.actionStr;
    _movementView.image = [UIImage imageNamed:model.movementStr];
}

@end
