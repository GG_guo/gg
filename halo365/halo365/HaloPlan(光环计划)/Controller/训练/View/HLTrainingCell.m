//
//  HLTrainingCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/6.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//



#import "HLTrainingCell.h"

@interface HLTrainingCell ()

@property (nonatomic, strong) UILabel *planLabel;

@end

@implementation HLTrainingCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createCellView];
    }
    return self;
}

- (void)createCellView
{
    _planLabel = [[UILabel alloc] init];
    _planLabel.font = [UIFont systemFontOfSize:20];
    _planLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_planLabel];
    [_planLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.width.offset(SCREEN_WIDTH);
        make.height.offset(44);
    }];
    
}

- (void)setModel:(HLTrainingModel *)model
{
    _planLabel.text = model.planStr;
}


@end
