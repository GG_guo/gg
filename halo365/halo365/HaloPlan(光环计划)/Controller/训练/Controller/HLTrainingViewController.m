//
//  HLTrainingViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLTrainingViewController.h"
#import "HLTrainingModel.h"
#import "HLTrainingCell.h"
#import "HLTrainingSummaryController.h"
#import "HLFirstWeekViewController.h"

@interface HLTrainingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *planArray;

@end

@implementation HLTrainingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor greenColor];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"return" highIcon:@"return" target:self action:@selector(clickSearch)];

    [self reloadTableView];
    
    [self registerCell];
    
    [self httpRequest];
}

- (void)reloadTableView{
    

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview: _tableView];
    
}

- (void)registerCell{
    [_tableView registerClass:[HLTrainingCell class] forCellReuseIdentifier:@"HLTrainingCell"];
}

- (void)httpRequest{
    
    _planArray = [NSMutableArray array];
    
    NSArray *array = @[@"训练概述",@"第一周",@"第二周",@"第三周",@"第四周",@"第五周",@"第六周",@"第七周",@"第八周",@"第九周",@"第十周",@"第十一周",@"第十二周"];
    for (int i =0;  i <array.count; i++) {
        HLTrainingModel *model = [[HLTrainingModel alloc] init];
        model.planStr = array[i];
        [_planArray addObject:model];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _planArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLTrainingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLTrainingCell"];
    cell.model = _planArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        HLTrainingSummaryController *traningVC = [[HLTrainingSummaryController alloc] init];
        traningVC.hidesBottomBarWhenPushed =YES;
        [self.navigationController pushViewController:traningVC animated:YES];
    }
    
    if (indexPath.row ==1) {
        HLFirstWeekViewController *firstVC = [[HLFirstWeekViewController alloc] init];
       firstVC.hidesBottomBarWhenPushed =YES;
        [self.navigationController pushViewController:firstVC animated:YES];
        
    }
    
    
    
}

- (void)clickSearch{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
