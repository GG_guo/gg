//
//  HLFirstWeekViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLFirstWeekViewController.h"
#import "HLWeekCell.h"
#import "HLWeekModel.h"
#import "HLFirstDayController.h"

@interface HLFirstWeekViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *weekArray;

@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *detailArray;

@end

@implementation HLFirstWeekViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"第一周";
    
    [self reloadCollectionView];
    
    [self registerCell];
    
    [self httpRequest];
    

}


- (void)reloadCollectionView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
}


- (void)registerCell
{
    [_tableView registerClass:[HLWeekCell class] forCellReuseIdentifier:@"HLWeekCell"];
}

- (void)httpRequest
{
    _weekArray = [NSMutableArray array];
    
    _imageArray = @[@"build1.jpg",@"build2.jpg",@"build3.jpg",@"build4.jpg",@"build5.jpg",@"build6.jpg",@"build3.jpg"];
    _titleArray = @[@"第一天",@"第二天",@"第三天",@"第四天",@"第五天",@"第六天",@"第七天"];
    
    _detailArray = @[@"胸，三头，小腿",@"背，二头，腹部",@"休息",@"肩，小腿",@"腿部，腹部",@"休息",@"休息"];
    
    for (int i = 0;  i <_imageArray.count; i++) {
        HLWeekModel *model = [[HLWeekModel alloc] init];
        model.weekImageStr = _imageArray[i];
        model.weekTitleStr = _titleArray[i];
        model.weekDetialStr = _detailArray[i];
        [_weekArray addObject:model];
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _weekArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    HLWeekCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLWeekCell"];
    
    cell.model = _weekArray[indexPath.row];
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLFirstDayController *firstDayVC =[[HLFirstDayController alloc] init];
   firstDayVC.hidesBottomBarWhenPushed =YES;
    [self.navigationController pushViewController:firstDayVC animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
