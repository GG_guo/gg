//
//  HLFirstDayController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLFirstDayController.h"
#import "HLCoachVideoCell.h"
#import "HLCoachVideoModel.h"
#import "HLCoachOverviewCell.h"
#import "HLCoachOverviewModel.h"
#import "HLFitnessCell.h"
#import "HLFitnessModel.h"
#import "HLCalculatorViewController.h"
#import "HLClockInViewController.h"

@interface HLFirstDayController ()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *expandStates;
@property (nonatomic, strong) UIImageView *coachImage;
@property (nonatomic, strong) UIImageView *trainingImage;
@property (nonatomic, strong) UIButton *trainingTitleButton;
@property (nonatomic, strong) UIImageView *trainTitleImage;

@end

@implementation HLFirstDayController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.pagingEnabled = NO;
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor lightGrayColor];
    
    _coachImage = [[UIImageView alloc] init];
    _coachImage.image = [UIImage imageNamed:@"build2.jpg"];
    [self.scrollView addSubview:_coachImage];
    [_coachImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.width.offset(SCREEN_WIDTH);
        make.height.offset((SCREEN_HEIGHT-10)/4-30);
    }];
    
    UIButton *videoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [videoButton setImage:[UIImage imageNamed:@"video-play"] forState:UIControlStateNormal];
    [videoButton addTarget:self action:@selector(clickVideo) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:videoButton];
    [videoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset((SCREEN_WIDTH - 50)/2);
        make.top.offset(((SCREEN_HEIGHT-10)/4-30 - 50)/2);
        make.width.offset(50);
        make.height.offset(50);
    }];
    
    
    _trainingTitleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _trainingTitleButton.backgroundColor= [UIColor redColor];
    [self.scrollView addSubview:_trainingTitleButton];
    [_trainingTitleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_coachImage.mas_bottom);
        make.left.mas_equalTo(_coachImage.mas_left);
        make.width.mas_equalTo(_coachImage.mas_width);
        make.height.mas_equalTo(28);
    }];
    
    UILabel *trainTitleLabel = [[UILabel alloc] init];
    trainTitleLabel.text = @"第一天/胸部，三头肌和小腿";
    trainTitleLabel.textColor = [UIColor whiteColor];
    trainTitleLabel.font = [UIFont systemFontOfSize:20];
    [_trainingTitleButton addSubview:trainTitleLabel];
    [trainTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(4);
        make.left.offset(15);
        make.width.offset(300);
        make.height.offset(20);
    }];
    
    _trainTitleImage = [[UIImageView alloc] init];
    _trainTitleImage.image = [UIImage imageNamed:@"48x48上拉按钮"];
    [_trainingTitleButton addSubview:_trainTitleImage];
    [_trainTitleImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15);
        make.top.offset(2);
        make.width.offset(24);
        make.height.offset(24);
    }];
    
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UILabel *lastLabel = nil;
    //  for (NSUInteger i = 0; i < 10; ++i) {
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.text = [self randomText];
    label.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [label addGestureRecognizer:tap];
    
    //自动换行
    label.preferredMaxLayoutWidth = screenWidth - 30;
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = [UIColor whiteColor];
    [self.scrollView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(self.view).offset(-15);
        
        if (lastLabel) {
            make.top.mas_equalTo(lastLabel.mas_bottom).offset(0);
        } else {
            make.top.mas_equalTo(_trainingTitleButton.mas_bottom).offset(0);
        }
        
        make.height.mas_equalTo(80);
    }];
    
    lastLabel = label;
    
    [self.expandStates addObject:[@[label, @(NO)] mutableCopy]];
    
    //    NSArray *numberArray = @[@"蓝标1",@"蓝标2",@"蓝标3",@"蓝标4",@"蓝标5"];
    NSArray *titleArray = @[@"慢跑",@"斜坡哑铃卧推",@"斜坡哑铃飞鸟",@"哑铃卧推",@"双杠臂屈伸"];
    NSArray *actionArray = @[@"5分钟",@"热身组:8-12。3组8-12个，60-90秒的休息时间，放弃组21个",@"3组8-12个，60-90秒的休息时间",@"4组6-10个，60-90秒的休息时间",@"3组做到力竭"];
    NSArray *imageArray = @[@"华波1.jpg",@"华波2.jpg",@"华波3.jpg",@"华波5.jpg",@"华波6.jpg"];
    
    
    for (int i = 0; i < imageArray.count; i ++ ) {
        
        _trainingImage = [[UIImageView alloc] init];
        _trainingImage.image = [UIImage imageNamed:imageArray[i]];
        //        _trainingImage.frame = CGRectMake(10, 300 + 50 *i, 50, 50);
        [self.scrollView addSubview:_trainingImage];
        [_trainingImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lastLabel.mas_bottom).offset(10 +105*i);
            make.left.offset(10);
            make.width.offset(100);
            make.height.offset(100);
        }];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = titleArray[i];
        titleLabel.font = [UIFont systemFontOfSize:20];
        titleLabel.textColor = [UIColor whiteColor];
        [self.scrollView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_trainingImage.mas_top);
            make.left.mas_equalTo(_trainingImage.mas_right).offset(10);
            make.width.offset(SCREEN_WIDTH - 130);
            make.height.offset(20);
        }];
        
        UILabel *actionLabel = [[UILabel alloc] init];
        actionLabel.text = actionArray[i];
        actionLabel.font = [UIFont systemFontOfSize:15];
        actionLabel.numberOfLines = 0;
        actionLabel.textColor = [UIColor whiteColor];
        [self.scrollView addSubview:actionLabel];
        [actionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabel.mas_bottom).offset(5);
            make.left.mas_equalTo(titleLabel.mas_left);
            make.width.mas_equalTo(titleLabel.mas_width);
            
        }];
        
    }
    
    UIButton *completeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    completeButton.backgroundColor = [UIColor blueColor];
    [completeButton setTitle:@"已完成今天的训练" forState:UIControlStateNormal];
    [self.scrollView addSubview:completeButton];
    [completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_trainingImage.mas_bottom).offset(10);
        make.left.offset(5);
        make.height.offset(44);
        make.width.offset(SCREEN_WIDTH -10);
    }];
    
    
    UILabel *orLabel = [[UILabel alloc] init];
    orLabel.text = @"或";
    [self.scrollView addSubview:orLabel];
    [orLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(completeButton.mas_bottom).offset(5);
        make.left.mas_offset((SCREEN_WIDTH - 20)/2);
        make.width.mas_offset(20);
        make.height.mas_offset(20);
    }];
    
    
    UIButton *pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pushButton setTitle:@"跳过今天的训练" forState:UIControlStateNormal];
    [pushButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.scrollView addSubview:pushButton];
    [pushButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(orLabel.mas_bottom).offset(5);
        make.left.mas_offset(5);
        make.width.mas_equalTo(completeButton.mas_width);
        make.height.mas_offset(20);
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
        
        // 让scrollview的contentSize随着内容的增多而变化
        make.bottom.mas_equalTo(pushButton.mas_bottom).offset(20);
    }];
}

- (void)clickVideo
{
    
}

- (NSMutableArray *)expandStates {
    if (_expandStates == nil) {
        _expandStates = [[NSMutableArray alloc] init];
    }
    
    return _expandStates;
}



- (NSString *)randomText {
    CGFloat length = 100 ;
    
    NSMutableString *str = [[NSMutableString alloc] init];
    for (NSUInteger i = 0; i < length; ++i) {
        [str appendString:@"测试数据很长，"];
    }
    
    return str;
}

- (void)onTap:(UITapGestureRecognizer *)sender {
    _trainTitleImage.image = [UIImage imageNamed:@"48x48下拉按钮"];
    
    UIView *tapView = sender.view;
    
    NSUInteger index = 0;
    for (NSMutableArray *array in self.expandStates) {
        UILabel *view = [array firstObject];
        
        if (view == tapView) {
            NSNumber *state = [array lastObject];
            
            if ([state boolValue] == YES) {
                _trainTitleImage.image = [UIImage imageNamed:@"48x48上拉按钮"];
                
                [view mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(80);
                }];
            } else {
                UIView *preView = nil;
                UIView *nextView = nil;
                
                if (index - 1 < self.expandStates.count && index >= 1) {
                    preView = [[self.expandStates objectAtIndex:index - 1] firstObject];
                }
                
                if (index + 1 < self.expandStates.count) {
                    nextView = [[self.expandStates objectAtIndex:index + 1] firstObject];
                }
                
                [view mas_remakeConstraints:^(MASConstraintMaker *make) {
                    if (preView) {
                        make.top.mas_equalTo(preView.mas_bottom).offset(-100);
                    } else {
                        make.top.mas_equalTo(_trainingTitleButton.mas_bottom).offset(0);
                    }
                    
                    make.left.mas_equalTo(15);
                    make.right.mas_equalTo(self.view).offset(-15);
                }];
                
                if (nextView) {
                    [nextView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(view.mas_bottom).offset(-10);
                    }];
                }
            }
            
            [array replaceObjectAtIndex:1 withObject:@(!state.boolValue)];
            
            [self.view setNeedsUpdateConstraints];
            [self.view updateConstraintsIfNeeded];
            
            [UIView animateWithDuration:0.35 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
            break;
        }
        
        index++;
    }
    
}

@end