//
//  HLTrainingModel.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLTrainingModel : NSObject

@property (nonatomic, copy) NSString *planStr;

@end
