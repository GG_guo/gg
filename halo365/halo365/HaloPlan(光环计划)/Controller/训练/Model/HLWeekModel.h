//
//  HLWeekModel.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLWeekModel : NSObject

@property (nonatomic, copy) NSString *weekImageStr;
@property (nonatomic, copy) NSString *weekTitleStr;
@property (nonatomic, copy) NSString *weekDetialStr;

@end
