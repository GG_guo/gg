//
//  HLFitnessModel.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/8.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLFitnessModel : NSObject

@property (nonatomic, copy) NSString *numberImageStr;
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *actionStr;
@property (nonatomic, copy) NSString *movementStr;

@end
