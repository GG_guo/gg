//
//  HLDietPlanCell.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLDietPlanCell.h"

@interface HLDietPlanCell ()
//Calories, fat, protein, carbohydrates

@property (nonatomic, strong) UIImageView *foodImage;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *caloriesLabel;
@property (nonatomic, strong) UILabel *fatLabel;
@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbohydratesLabel;

//@property (nonatomic, strong) UILabel *nameStr;
@property (nonatomic, strong) UILabel *caloriesStr;
@property (nonatomic, strong) UILabel *fatStr;
@property (nonatomic, strong) UILabel *proteinStr;
@property (nonatomic, strong) UILabel *carbohydratesStr;


@end

@implementation HLDietPlanCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createCellView];
    }
    return self;
}

- (void)createCellView
{
    
    _foodImage = [[UIImageView alloc] init];
    _foodImage.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:_foodImage];
    [_foodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.left.offset(5);
        make.width.offset(90);
        make.height.offset(90);
    }];
    
    _nameLabel = [[UILabel alloc] init];
    _nameLabel.font = [UIFont systemFontOfSize:15];
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_foodImage.mas_top);
        make.left.equalTo(_foodImage.mas_right).offset(5);
        make.width.offset(180);
        make.height.offset(15);
        
    }];
    
    _caloriesLabel = [[UILabel alloc] init];
    _caloriesLabel.text = @"热量:";
    _caloriesLabel.font = [UIFont systemFontOfSize:13];
    _caloriesLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_caloriesLabel];
    [_caloriesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameLabel.mas_bottom);
        make.left.equalTo(_foodImage.mas_right).offset(5);
        make.width.offset(80);
        make.height.offset(15);
        
    }];
    
    _caloriesStr = [[UILabel alloc] init];
//    _caloriesStr.text = @"热量:";
    _caloriesStr.font = [UIFont systemFontOfSize:13];
    _caloriesStr.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_caloriesStr];
    [_caloriesStr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_caloriesLabel.mas_top);
        make.left.equalTo(_caloriesLabel.mas_right).offset(5);
        make.width.offset(80);
        make.height.offset(15);
        
    }];
    
    
    _fatLabel = [[UILabel alloc] init];
    _fatLabel.text = @"脂肪:";
    _fatLabel.font = [UIFont systemFontOfSize:13];
    _fatLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_fatLabel];
    [_fatLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_caloriesLabel.mas_bottom);
        make.left.equalTo(_foodImage.mas_right).offset(5);
        make.width.equalTo(_caloriesLabel.mas_width);
        make.height.offset(15);
    }];
    
    _fatStr = [[UILabel alloc] init];
//    _fatStr.text = @"脂肪:";
    _fatStr.font = [UIFont systemFontOfSize:13];
    _fatStr.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_fatStr];
    [_fatStr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_fatLabel.mas_top);
        make.left.equalTo(_fatLabel.mas_right).offset(5);
        make.width.equalTo(_caloriesLabel.mas_width);
        make.height.offset(15);
    }];
    
    
    _proteinLabel = [[UILabel alloc] init];
    _proteinLabel.text = @"蛋白质:";
    _proteinLabel.font = [UIFont systemFontOfSize:13];
    _proteinLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_proteinLabel];
    [_proteinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_fatLabel.mas_bottom);
        make.left.equalTo(_foodImage.mas_right).offset(5);
        make.width.equalTo(_caloriesLabel.mas_width);
        make.height.offset(15);
        
    }];
    
    _proteinStr = [[UILabel alloc] init];
//    _proteinStr.text = @"蛋白质:";
    _proteinStr.font = [UIFont systemFontOfSize:13];
    _proteinStr.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_proteinStr];
    [_proteinStr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_proteinLabel.mas_top);
        make.left.equalTo(_proteinLabel.mas_right).offset(5);
        make.width.equalTo(_caloriesLabel.mas_width);
        make.height.offset(15);
        
    }];

    _carbohydratesLabel = [[UILabel alloc] init];
    _carbohydratesLabel.text = @"碳水化合物:";
//    _carbohydratesLabel.backgroundColor = [UIColor redColor];
    _carbohydratesLabel.font = [UIFont systemFontOfSize:13];
    _carbohydratesLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_carbohydratesLabel];
    [_carbohydratesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_proteinLabel.mas_bottom);
        make.left.equalTo(_foodImage.mas_right).offset(5);
        make.width.equalTo(_caloriesLabel.mas_width);
        make.height.offset(15);
        
    }];
    
    _carbohydratesStr = [[UILabel alloc] init];
//    _carbohydratesStr.text = @"碳水化合物:";
    //    _carbohydratesLabel.backgroundColor = [UIColor redColor];
    _carbohydratesStr.font = [UIFont systemFontOfSize:13];
    _carbohydratesStr.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_carbohydratesStr];
    [_carbohydratesStr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_carbohydratesLabel.mas_top);
        make.left.equalTo(_carbohydratesLabel.mas_right).offset(5);
        make.width.equalTo(_caloriesLabel.mas_width);
        make.height.offset(15);
        
    }];
    
}

- (void)setModel:(HLDietPlanModel *)model
{
    _foodImage.image = [UIImage imageNamed:model.dietImageStr];
    _nameLabel.text = model.dietNameStr;
    _caloriesStr.text = model.dietCaloriesStr;
    _fatStr.text = model.dietFatStr;
    _proteinStr.text = model.dietProteinStr;
    _carbohydratesStr.text = model.dietCarbohydratesStr;
    
}


@end
