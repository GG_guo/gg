//
//  HLDietPlanModel.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLDietPlanModel : NSObject

@property (nonatomic, copy) NSString *dietImageStr;
@property (nonatomic, copy) NSString *dietNameStr;
@property (nonatomic, copy) NSString *dietCaloriesStr;//热量
@property (nonatomic, copy) NSString *dietFatStr;//脂肪
@property (nonatomic, copy) NSString *dietProteinStr;//蛋白质
@property (nonatomic, copy) NSString *dietCarbohydratesStr;//碳水化合物


@end
