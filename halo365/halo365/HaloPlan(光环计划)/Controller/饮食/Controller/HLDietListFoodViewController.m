//
//  HLDietListFoodViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLDietListFoodViewController.h"
#import "HLDietPlanCell.h"
#import "HLDietPlanModel.h"

@interface HLDietListFoodViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listCountArray;

@property (nonatomic, strong) NSArray *headerArray;

@end


@implementation HLDietListFoodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self reloadTableView];
    
    [self registerCell];
    
    [self httpRequest];


}

- (void)reloadTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];

}

- (void)registerCell{
    
    [_tableView registerClass:[HLDietPlanCell class] forCellReuseIdentifier:@"HLDietPlanCell"];
    
}

- (void)httpRequest{
    
    _headerArray = @[@"碳水化合物",@"蛋白质",@"脂肪"];
    
    NSMutableArray *array1 = [NSMutableArray array];
    NSArray *foodImageArray1 = @[@"food1.jpg",@"food2.jpg",@"food3.jpg"];
    NSArray *nameArray1 = @[@"蛋白粉",@"鸡蛋",@"鸡肉"];
    NSArray *caloriesArray1 = @[@"150KJ",@"230KJ",@"100KJ"];
    NSArray *fatArray1 = @[@"150g",@"400g",@"600g"];
    NSArray *proteinArray1 = @[@"329g",@"131g",@"322g"];
    NSArray *carbohydratesArray1 = @[@"321g",@"493g",@"234g"];
    
    
    for (int i = 0; i <foodImageArray1.count; i++) {
        HLDietPlanModel *model = [[HLDietPlanModel alloc] init];
        model.dietImageStr = foodImageArray1[i];
        model.dietNameStr = nameArray1[i];
        model.dietCaloriesStr = caloriesArray1[i];
        model.dietFatStr = fatArray1[i];
        model.dietProteinStr = proteinArray1[i];
        model.dietCarbohydratesStr = carbohydratesArray1[i];
        [array1 addObject:model];
    }
    
    NSMutableArray *array2 = [NSMutableArray array];
    NSArray *foodImageArray2 = @[@"food4.jpg",@"food1.jpg",@"food3.jpg",@"food5.jpg"];
    NSArray *nameArray2 = @[@"蛋白粉",@"鸡蛋",@"鸡肉",@"蔬菜沙拉"];
    NSArray *caloriesArray2 = @[@"150KJ",@"230KJ",@"100KJ",@"80KJ"];
    NSArray *fatArray2 = @[@"150g",@"400g",@"600g",@"32g"];
    NSArray *proteinArray2 = @[@"329g",@"131g",@"322g",@"83"];
    NSArray *carbohydratesArray2 = @[@"321g",@"493g",@"234g",@"93g"];
    
    
    for (int i = 0; i <foodImageArray2.count; i++) {
        HLDietPlanModel *model = [[HLDietPlanModel alloc] init];
        model.dietImageStr = foodImageArray2[i];
        model.dietNameStr = nameArray2[i];
        model.dietCaloriesStr = caloriesArray2[i];
        model.dietFatStr = fatArray2[i];
        model.dietProteinStr = proteinArray2[i];
        model.dietCarbohydratesStr = carbohydratesArray2[i];
        [array2 addObject:model];
    }

    NSMutableArray *array3 = [NSMutableArray array];
    NSArray *foodImageArray3 = @[@"food1.jpg",@"food1.jpg"];
    NSArray *nameArray3 = @[@"蛋白粉",@"鸡蛋",@"鸡肉",@"蔬菜沙拉"];
    NSArray *caloriesArray3 = @[@"150KJ",@"230KJ",@"100KJ",@"80KJ"];
    NSArray *fatArray3 = @[@"150g",@"400g",@"600g",@"32g"];
    NSArray *proteinArray3 = @[@"329g",@"131g",@"322g",@"83"];
    NSArray *carbohydratesArray3 = @[@"321g",@"493g",@"234g",@"93g"];
    
    
    for (int i = 0; i <foodImageArray3.count; i++) {
        HLDietPlanModel *model = [[HLDietPlanModel alloc] init];
        model.dietImageStr = foodImageArray3[i];
        model.dietNameStr = nameArray3[i];
        model.dietCaloriesStr = caloriesArray3[i];
        model.dietFatStr = fatArray3[i];
        model.dietProteinStr = proteinArray3[i];
        model.dietCarbohydratesStr = carbohydratesArray3[i];
        [array3 addObject:model];
    }

    _listCountArray = [[NSMutableArray alloc] initWithObjects:array1,array2,array3, nil];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _listCountArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_listCountArray objectAtIndex:section] count];
}

//设置标题高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
//    return [_headerArray[section] title];
    if (section ==0) {
        return @"主要成分蛋白质";
    }else if (section ==1){
        return @"主要成分脂肪";
    }else
        return @"主要成分碳水化合物";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLDietPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLDietPlanCell"];
    cell.model = [[_listCountArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
