//
//  HLDietSummaryViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLDietPlanViewController.h"
#import "HLCoachImageCell.h"
#import "HLCoachImageModel.h"
#import "HLCoachOverviewCell.h"
#import "HLCoachOverviewModel.h"
#import "HLDietPlanCell.h"
#import "HLDietPlanModel.h"

@interface HLDietPlanViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dietImageArray;
@property (nonatomic, strong) NSMutableArray *dietTitleArray;
@property (nonatomic, strong) NSMutableArray *dietListFoodArray;
@property (nonatomic, strong) NSMutableArray *foodImageArray;


@end

@implementation HLDietPlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self reloadTableView];
    
    [self registerCell];
    
    [self httpRequest];

}

- (void)reloadTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
}

- (void)registerCell
{
    [_tableView registerClass:[HLCoachImageCell class] forCellReuseIdentifier:@"HLCoachImageCell"];

    [_tableView registerClass:[HLCoachOverviewCell class] forCellReuseIdentifier:@"HLCoachOverviewCell"];
    [_tableView registerClass:[HLDietPlanCell class] forCellReuseIdentifier:@"HLDietPlanCell"];
    
}

- (void)httpRequest
{
    _dietImageArray = [NSMutableArray array];
    NSArray *dietImageArray = @[@"tools001.jpg"];
    for (int i = 0; i <dietImageArray.count; i++) {
        HLCoachImageModel *model = [[HLCoachImageModel alloc] init];
        model.imageThumb = dietImageArray[i];
        [_dietImageArray addObject:model];
    }
    
    _dietTitleArray = [NSMutableArray array];
    NSArray *dietTitleArray = @[@"食物是人体能量和营养素的主要来源。能量用以支持人体活动，而在修复组织损伤、促进人类成长和保持健康方面，营养素则更为重要。                                                                          我们把吃进去的食物叫「营养物质」,那么构成这些食物的基本成分叫做「营养素」。"];
    for (int i = 0; i <dietTitleArray.count; i++) {
        HLCoachOverviewModel *model = [[HLCoachOverviewModel alloc] init];
        model.overviewStr = dietTitleArray[i];
        [_dietTitleArray addObject:model];
    }

    _foodImageArray = [NSMutableArray array];
    NSArray *foodImageArray = @[@"food1.jpg",@"food2.jpg",@"food3.jpg",@"food4.jpg",@"food5.jpg"];
    NSArray *nameArray = @[@"蛋白粉",@"鸡蛋",@"鸡肉",@"蔬菜沙拉",@"香蕉"];
    NSArray *caloriesArray = @[@"150KJ",@"230KJ",@"100KJ",@"80KJ",@"50KJ"];
    NSArray *fatArray = @[@"150g",@"400g",@"600g",@"70g",@"60g"];
    NSArray *proteinArray = @[@"329g",@"131g",@"322g",@"234g",@"53g"];
    NSArray *carbohydratesArray = @[@"321g",@"493g",@"234g",@"123g",@"64g"];
    
    
    for (int i = 0; i <foodImageArray.count; i++) {
        HLDietPlanModel *model = [[HLDietPlanModel alloc] init];
        model.dietImageStr = foodImageArray[i];
        model.dietNameStr = nameArray[i];
        model.dietCaloriesStr = caloriesArray[i];
        model.dietFatStr = fatArray[i];
        model.dietProteinStr = proteinArray[i];
        model.dietCarbohydratesStr = carbohydratesArray[i];
        [_foodImageArray addObject:model];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return _dietImageArray.count;

    }if (section == 1) {
        return _dietTitleArray.count;

    }
    return _foodImageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        HLCoachImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachImageCell"];
        cell.model = _dietImageArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }if (indexPath.section == 1) {
        HLCoachOverviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLCoachOverviewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.model = _dietTitleArray[indexPath.row];
        return cell;
    }
    HLDietPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLDietPlanCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.model = _foodImageArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return ((SCREEN_HEIGHT-20)/3-10);
  
    }if (indexPath.section == 1) {
        return ((SCREEN_HEIGHT-20)/4-10);

    }
    return 100;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
