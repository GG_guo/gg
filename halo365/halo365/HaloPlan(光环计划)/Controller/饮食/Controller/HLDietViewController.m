//
//  HLDietViewController.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/3.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLDietViewController.h"
#import "HLTrainingModel.h"
#import "HLTrainingCell.h"
#import "HLNutritionSummaryController.h"
#import "HLDietPlanViewController.h"
#import "HLDietListFoodViewController.h"
#import "HLOverviewToolController.h"

@interface HLDietViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dietArray;

@end

@implementation HLDietViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor yellowColor];
    [self reloadTableView];
    
    [self registerCell];
    
    [self httpRequest];
    
}

- (void)reloadTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview: _tableView];
    
}

- (void)registerCell
{
    [_tableView registerClass:[HLTrainingCell class] forCellReuseIdentifier:@"HLTrainingCell"];
}

- (void)httpRequest{
    
    _dietArray = [NSMutableArray array];
    
    NSArray *array = @[@"营养概述",@"饮食计划",@"食物清单",@"营养元素摄入量"];
    for (int i =0;  i <array.count; i++) {
        HLTrainingModel *model = [[HLTrainingModel alloc] init];
        model.planStr = array[i];
        [_dietArray addObject:model];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dietArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLTrainingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLTrainingCell"];
    cell.model = _dietArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (indexPath.row) {
        case 0:
            [self nutritionView];
            break;
        case 1:
            [self nutritionPlan];
            break;
        case 2:
            [self listFood];
            break;
        case 3:
            [self overviewTool];
            break;
        default:
            break;
    }
      
}

- (void)nutritionView{
    
    HLNutritionSummaryController *dietSummaryVC = [[HLNutritionSummaryController alloc] init];
    [self.navigationController pushViewController:dietSummaryVC animated:YES];
}

- (void)nutritionPlan{
    
    HLDietPlanViewController *dietPlanVC = [[HLDietPlanViewController alloc] init];
    [self.navigationController pushViewController:dietPlanVC animated:YES];

}

- (void)listFood{
    
    HLDietListFoodViewController *dietFoodVC = [[HLDietListFoodViewController alloc] init];
    [self.navigationController pushViewController:dietFoodVC animated:YES];

}

//营养元素的计算工具
- (void)overviewTool{
//    HLOverviewToolController *overviewToolVC = [[HLOverviewToolController alloc] init];
//    [self.navigationController pushViewController:overviewToolVC animated:YES];
//
    
    
    WebVC *helpview=[[WebVC alloc]init];
    helpview.hidesBottomBarWhenPushed=YES;
    NSString*url=[NSString stringWithFormat:@"%@macronutrients.html",BESTTOOL];
    
    helpview.urlString=url;
    [self.navigationController pushViewController:helpview animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
