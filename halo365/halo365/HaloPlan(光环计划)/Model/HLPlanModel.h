//
//  HLPlanModel.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLPlanModel : NSObject

@property (nonatomic , strong)NSString *thumb;
@property (nonatomic , strong)NSString *title;
@property (nonatomic , strong)NSString *nick;
@property (nonatomic , strong)NSString *parameter;
@end
