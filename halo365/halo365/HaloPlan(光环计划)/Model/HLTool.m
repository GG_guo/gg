//
//  HLTool.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/6/1.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLTool.h"

@implementation HLTool

+ (void)dealTableViewSeparator:(id)sender {
    
    if ([sender respondsToSelector:@selector(setSeparatorInset:)]) {
        [sender setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([sender respondsToSelector:@selector(setLayoutMargins:)]) {
        [sender setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
