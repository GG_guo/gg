//
//  AppDelegate.h
//  halo365
//
//  Created by 李鹏辉 on 16/7/21.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CoreData/CoreData.h>
//
#import "HLBaseAPPTabBarController.h"
//#define KAppkey @"1965302827"
//#define KRedirectURL @"http://www.sina.com"

@interface HLAppDelegate : UIResponder <UIApplicationDelegate>
//{
//    NSString* wbtoken;
//    NSString* wbCurrentUserID;
//}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic , strong)HLBaseAPPTabBarController * tabBarController;

- (UIViewController *)getNowViewController;

//@property (strong, nonatomic) NSString *wbtoken;
//@property (strong, nonatomic) NSString *wbRefreshToken;
//@property (strong, nonatomic) NSString *wbCurrentUserID;

@end

