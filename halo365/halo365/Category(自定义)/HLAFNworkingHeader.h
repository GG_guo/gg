//
//  HLRequestData.h
//  光环tableview
//
//  Created by 李鹏辉 on 16/3/17.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLAFNworkingHeader : NSObject

/**
 *  数据请求
 *
 *  @param urlstring URL
 *  @param method    get or Post
 *  @param token     返回的token
 *  @param parmas    请求参数
 *  @param files     请求文件(图片)
 *  @param success   请求成功的block
 *  @param fail      请求失败的block
 */


+ (void)requestURL:(NSString *)requestURL
        httpMethod:(NSString *)method
             token:(NSString *)token
            params:(NSMutableDictionary *)parmas
              file:(NSDictionary *)files
           success:(void (^)(id data))success
              fail:(void (^)(NSError *error))fail;

@end
