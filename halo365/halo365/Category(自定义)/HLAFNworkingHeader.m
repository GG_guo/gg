//
//  HLRequestData.m
//  光环tableview
//
//  Created by 李鹏辉 on 16/3/17.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLAFNworkingHeader.h"
#import "AFNetworking.h"

@implementation HLAFNworkingHeader

+ (void)requestURL:(NSString *)requestURL
        httpMethod:(NSString *)method
             token:(NSString *)token
            params:(NSMutableDictionary *)parmas
              file:(NSDictionary *)files
           success:(void (^)(id data))success
              fail:(void (^)(NSError *error))fail
{
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"ios32D44@&&ddks" forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"v0.1" forHTTPHeaderField:@"X-Request-Version"];
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"X-Request-Token"];
    [manager.requestSerializer setValue:@"$uuid" forHTTPHeaderField:@"X-Request-Id"];
    [manager.requestSerializer setValue:@"Halo365-Team" forHTTPHeaderField:@"X-Powered-By"];
    
    
    //判断网络状况
    AFNetworkReachabilityManager *netManager = [AFNetworkReachabilityManager sharedManager];
    [netManager startMonitoring];  //开始监听
    [netManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        
        
        
        if (status == AFNetworkReachabilityStatusNotReachable)
        {
            
            //showAlert
//            [[[UIAlertView alloc]initWithTitle:@"提示" message:@"网络链接错误,请检查网络链接" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil, nil]show];
            [UIAlertController alertControllerWithTitle:@"提示" message:@"网络链接错误,请检查网络链接"preferredStyle:NO];
            //NSLog(@"没有网络");
            
            return;
            
        }else if (status == AFNetworkReachabilityStatusUnknown){
            
            //NSLog(@"未知网络");
            
            
        }else if (status == AFNetworkReachabilityStatusReachableViaWWAN){
            
            //NSLog(@"WiFi");
            
        }else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
            
            //NSLog(@"手机网络");
        }
        
        
        
    }];
    
    
    // 4.get请求
    if ([[method uppercaseString] isEqualToString:@"GET"]) {
        
        
        [manager GET:requestURL
          parameters:parmas
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 
                 if (success != nil)
                 {
                     success(responseObject);
                 }
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 
                 
                 
                 if (fail != nil) {
                     fail(error);
                 }
             }];
        
        
        // 5.post请求不带文件 和post带文件
    }else if ([[method uppercaseString] isEqualToString:@"POST"]) {
        
        
        if (files == nil) {
            
            
            [manager POST:requestURL
               parameters:parmas
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      
                      
                      if (success) {
                          success(responseObject);
                      }
                      
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      
                      if (fail) {
                          fail(error);
                      }
                      
                  }];
            
        } else {
            
            [manager POST:requestURL
               parameters:parmas constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                   
                   
                   for (id key in files) {
                       
                       id value = files[key];
                       
                       
                       
                       [formData appendPartWithFileData:value
                                                   name:key
                                               fileName:@"header.png"
                                               mimeType:@"image/png"];
                   }
                   
               } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   
                   if (success) {
                       success(responseObject);
                   }
                   
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   
                   if (fail) {
                       fail(error);
                   }
                   
               }];
        }
        
    }else if ([[method uppercaseString] isEqualToString:@"PUT"]){
        
        [manager PUT:requestURL
          parameters:parmas
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 
                 if (success != nil)
                 {
                     success(responseObject);
                 }
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 
                 
                 if (fail != nil) {
                     fail(error);
                 }
             }];
        
        
        
    }
    
    
}

@end
