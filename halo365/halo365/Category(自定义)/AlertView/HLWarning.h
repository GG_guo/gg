//
//  HLWarning.h
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import <Foundation/Foundation.h>

static CGFloat hudAnimationInterval = 1.5f;

@interface HLWarning : NSObject

/**
 *  显示文本 hud
 *
 *  @param message <#message description#>
 */
+ (void)showMessage:(NSString *)message;

/**
 *  显示指示器 hud
 *
 *  @param text <#text description#>
 */
+ (void)showIndicationWithText:(NSString *)text;
+ (void)hideAll;

@end
