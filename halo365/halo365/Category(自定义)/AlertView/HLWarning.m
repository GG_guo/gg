//
//  HLWarning.m
//  halo_demo
//
//  Created by 李鹏辉 on 16/4/7.
//  Copyright © 2016年 李鹏辉. All rights reserved.
//

#import "HLWarning.h"

@implementation HLWarning

+ (void)showMessage:(NSString *)message {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:HLSharedAppDelegate.window animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    
    [hud hide:YES afterDelay:hudAnimationInterval];
}

+ (void)showIndicationWithText:(NSString *)text {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:HLSharedAppDelegate.window animated:YES];
    if (text) hud.labelText = text;
    hud.removeFromSuperViewOnHide = YES;
    [hud show:YES];
}

+ (void)hideAll {
    
    [MBProgressHUD hideAllHUDsForView:HLSharedAppDelegate.window animated:YES];
}

@end
